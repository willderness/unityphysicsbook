﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientDll
{
    public class Messages
    {
        public const string Begin = "Hello";
        public const string End = "Bye";
        public const string ACK = "YES";

        public const string InkStroke = "Stroke";
        public const string ClearRecognizer = "Clear";
        public const string Results = "Results";
        public const string ClearStroke = "ClearStroke";
    }
}
