﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace ClientDll
{
    public class Client
    {
        private Socket ClientSocket;
        private byte[] ReceiveBuffer = new byte[4096];
        public bool Initialized { get; protected set; }

        public Client()
        {
            Initialized = false;
        }


        public void Connect()
        {
            try
            {
                ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
                IPAddress serverAddr = IPAddress.Parse("127.0.0.1");
                IPEndPoint serverMachine = new IPEndPoint(serverAddr, 8881);

                ClientSocket.Connect(serverMachine);

                Send(Messages.Begin);
                string response = Receive();
                if (response == Messages.ACK)
                    Initialized = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Initialized = false;
            }
        }

        public void Send(string msg)
        {
            try
            {
                msg += "$";
                //Encode a message
                byte[] sendBuffer = Encoding.ASCII.GetBytes(msg);
                ClientSocket.Send(sendBuffer);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }

        public string Receive()
        {
            string message = "";
            try
            {
                ClientSocket.Receive(ReceiveBuffer);
                message = Encoding.Default.GetString(ReceiveBuffer);
                //Remove the tailing '\0's after the '\n' token, caused by the buffer size
                int ixEnd = message.IndexOf('$');
                message = message.Remove(ixEnd);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }

            return message;
        }

        public void Disconnect()
        {
            Send(Messages.End);
            string response = Receive();
            if (response == Messages.ACK)
            {
                //Wait for server to close this connection
                System.Threading.Thread.Sleep(500);
            }
            else
            {
                Console.WriteLine("Client failed to disconnect.\n");
            }
        }


    }
}
