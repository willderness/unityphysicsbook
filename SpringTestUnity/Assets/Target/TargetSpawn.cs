using UnityEngine;
using System.Collections;

public class TargetSpawn : MonoBehaviour {
	public float TargetDistanceZStart = -58;
	public float TargetDistanceZRange = -70;
	public float angle;
	public float targetScale = 5;
	public float distance;
	public Transform targetPrefab;
	// Use this for initialization
	void Start () {
		angle = (Random.value * (Mathf.PI/2))-(Mathf.PI/4);
		distance = (Random.value * TargetDistanceZRange )  + TargetDistanceZStart;
		float height = 0.1f;
		transform.position = new Vector3( distance*Mathf.Sin(angle), height, distance*Mathf.Cos(angle));
		transform.localScale = new Vector3(targetScale, 1, targetScale);
		//transform.rotation =  Quaternion.LookRotation (GameObject.FindGameObjectWithTag("Player").transform.position - transform.position);
		//transform.Rotate( new Vector3( 0,0,0) );
			
	}
	
    /*void OnCollisionEnter(Collision collision) {
		if( collision.gameObject.name == "Player" )
		{
			GameObject.Instantiate( targetPrefab );
			Destroy(gameObject);
		}
	}*/
}
