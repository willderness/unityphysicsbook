//////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2007-2012 Infinite Z, Inc.  All Rights Reserved.
//
//  File:       ZSStylusDragger.cs
//  Content:    Allows the user to drag a rigid body with the stylus.
//  SVN Info:   $Id$
//
//////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ZSStylusShape))]
/// <summary>
/// Stylus tool for dragging objects with or without physics.
/// </summary>
public class ZSStylusMenus : MonoBehaviour
{
  /// <summary>
  /// Chooses between physical or non-physical dragging.
  /// During non-physical drag, the dragged object's transform is directly updated.
  /// During physical drag, the dragged object is physically constrained to the stylus.
  /// </summary>
  public bool m_isPhysical = false;
  
  /// <summary>
  /// The ID of the stylus button that will be used for dragging.
  /// </summary>
  public int m_dragButton = 0;

  List<GameObject> m_focusObjects = new List<GameObject>();
  Vector3 m_contactPoint;
  Dictionary<GameObject, Vector3> m_focusOffsets = new Dictionary<GameObject, Vector3>();
  Dictionary<GameObject, Quaternion> m_focusRotations = new Dictionary<GameObject, Quaternion>();
  Dictionary<GameObject, ConfigurableJoint> m_joints = new Dictionary<GameObject, ConfigurableJoint>();
  ZSStylusSelector m_stylusSelector;

  void Awake()
  {
    m_stylusSelector = GameObject.Find("ZSStylus").GetComponent<ZSStylusSelector>();
  }


  void Update()
  {
    if (m_stylusSelector.GetButtonDown(m_dragButton) && m_stylusSelector.hoveredObject != null)
    {
      foreach (GameObject selectedObject in m_stylusSelector.selectedObjects)
      {
        m_focusObjects.Add(selectedObject);
                
        selectedObject.SendMessage("OnDragBegin", SendMessageOptions.DontRequireReceiver);
      }
    }

    if (m_stylusSelector.GetButtonUp(m_dragButton))
    {
      foreach (GameObject focusObject in m_focusObjects)
        focusObject.SendMessage("OnDragEnd", SendMessageOptions.DontRequireReceiver);

      m_focusObjects.Clear();
      m_focusOffsets.Clear();
      m_focusRotations.Clear();
    }

  }



}
