
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ZSStylusShape))]
/// <summary>
/// Stylus tool for dragging objects with or without physics.
/// </summary>
public class ZSStylusCameraDrag : MonoBehaviour
{

  	/// <summary>
  /// The ID of the stylus button that will be used for dragging.
  /// </summary>
  	public int m_dragButton = 1;
	
	public int m_translateButton = 2;
		
	List<GameObject> m_focusObjects = new List<GameObject>();
	Vector3 m_contactPoint;
	Dictionary<GameObject, Vector3> m_focusOffsets = new Dictionary<GameObject, Vector3>();
	Dictionary<GameObject, Quaternion> m_focusRotations = new Dictionary<GameObject, Quaternion>();
	ZSStylusSelector m_stylusSelector;
	ZSCore m_zsCore;
	
	GameObject centerCube;
	GameObject originCube;
	
	private bool yRotation, xRotation;
	
  	private bool isDraggable = true;
	Vector3 m_stylusStartLoc;	
	Vector3 m_stylusLastPos;
	void Awake()
	{
		m_stylusSelector = GameObject.Find("ZSStylus").GetComponent<ZSStylusSelector>();
		m_zsCore = GameObject.Find("ZSCore").GetComponent<ZSCore>();
		centerCube = GameObject.Find("CenterCube");
		yRotation = false;
		xRotation = false;
	}
	
	enum ButtonState { ONE, TWO, NONE };
	ButtonState buttonState = ButtonState.NONE;

	void Update()
	{
		if (m_stylusSelector.GetButtonDown(m_dragButton) )
		{
			m_contactPoint = m_stylusSelector.hoverPoint;
		
			//m_focusObjects.Add(Camera.main.gameObject);
			m_focusObjects.Add(centerCube);
			
			// Save the relative transform from the stylus to the focus object.*/			
			Quaternion invRotation = Quaternion.Inverse(transform.rotation);
			//m_focusOffsets[Camera.main.gameObject] = /*invRotation **/ (Camera.main.transform.position);
			//m_focusRotations[Camera.main.gameObject] = invRotation * Camera.main.transform.rotation;
			m_focusOffsets[centerCube] = /*invRotation **/ (centerCube.transform.position);
			m_focusRotations[centerCube] = invRotation * centerCube.transform.rotation;
			m_stylusStartLoc = transform.position;
			//m_stylusLastPos = transform.position;
			m_stylusLastPos = Input.mousePosition;
			
			// Initiate drag.        
			//Camera.main.SendMessage("OnDragBegin", SendMessageOptions.DontRequireReceiver);     
			centerCube.SendMessage("OnDragBegin", SendMessageOptions.DontRequireReceiver); 
			buttonState = ButtonState.ONE;
			yRotation = false;
			xRotation = false;
		}
		else if (m_stylusSelector.GetButtonDown(m_translateButton) )
		{
			m_contactPoint = m_stylusSelector.hoverPoint;
			
			//m_focusObjects.Add(Camera.main.gameObject);
			m_focusObjects.Add(centerCube);
			
			// Save the relative transform from the stylus to the focus object.*/
			//m_focusOffsets[Camera.main.gameObject] = /*invRotation **/ (Camera.main.transform.position);
			m_focusOffsets[centerCube] = /*invRotation **/ (centerCube.transform.position);
			m_stylusStartLoc = transform.position;
			m_stylusLastPos = transform.position;
			
			// Initiate drag.        
			//Camera.main.SendMessage("OnDragBegin", SendMessageOptions.DontRequireReceiver);      
			centerCube.SendMessage("OnDragBegin", SendMessageOptions.DontRequireReceiver); 
			buttonState = ButtonState.TWO;
		}
		
		if (m_stylusSelector.GetButtonUp(m_dragButton) || m_stylusSelector.GetButtonUp(m_translateButton))
		{
			foreach (GameObject focusObject in m_focusObjects)
				focusObject.SendMessage("OnDragEnd", SendMessageOptions.DontRequireReceiver);
			
			m_stylusSelector.stylus.OnDragEnd(m_focusObjects.ToArray());
			
			m_focusObjects.Clear();
			m_focusOffsets.Clear();
			m_focusRotations.Clear();
			buttonState = ButtonState.NONE;
			yRotation = false;
		xRotation = false;
		}
		
		// For non-physical dragging, update the dragged object transform by applyging the saved relative transform to the new stylus transform.
		foreach (GameObject focusObject in m_focusObjects)
		{
			
			if (buttonState == ButtonState.TWO) 
			{
#if OLDWAY_TRANS
				Vector3 offset = ( (transform.position - focusObject.transform.position) - (m_stylusStartLoc - m_focusOffsets[centerCube]) );
				offset /= 10f;
				
				focusObject.transform.position = focusObject.transform.position + offset; //focusObject.transform.Translate(offset);
#endif
				Vector3 offset = ( m_stylusLastPos - transform.position);
				focusObject.transform.position = focusObject.transform.position - (offset); //focusObject.transform.Translate(offset);
				m_stylusLastPos = transform.position;
			}
			else
			{
#if OLDOLDWAY
				// This rotates in any direction
				//focusObject.transform.rotation = transform.rotation * m_focusRotations[focusObject];
#endif
#if OLDWAY		
				// Trying to restrict the rotation to 1 plane at a time.
				// However this is jumping back to center every time... it needs to work as a relative change to the button down position.

				
				Vector3 eulerAngles = transform.rotation.eulerAngles;
				eulerAngles = new Vector3(0, eulerAngles.y, 0);
				print("eulerAngles.y = " + eulerAngles.y);
				print("position = " + transform.position);
				focusObject.transform.rotation = Quaternion.Euler(eulerAngles);
				Camera.main.transform.LookAt( Vector3.zero );
#endif
				Vector3 stylusOffset = (m_stylusLastPos - Input.mousePosition)/500.0f;
				print( Time.deltaTime + " offsET: "+ stylusOffset );
				if( Mathf.Abs(stylusOffset.x) > 0.1 )
				{
					xRotation = true;
					yRotation = false;	
				}
				if( Mathf.Abs(stylusOffset.y) > 0.1 )
				{
					xRotation = false;
					yRotation = true;	
				}
				stylusOffset = new Vector3(
					xRotation ? stylusOffset.x : 0,
					yRotation ? stylusOffset.y : 0,
					0 );
				//stylusOffset *= 10;
				//focusObject.transform.RotateAround( Vector3.up, stylusOffset.x );
				
				focusObject.transform.RotateAround(Camera.main.transform.right,-  stylusOffset.y );
				
				focusObject.transform.RotateAround(Vector3.up, stylusOffset.x );
				/*Vector3 eulerAngles = focusObject.transform.rotation.eulerAngles;
				eulerAngles = new Vector3(0, eulerAngles.y + stylusOffset.x, 0);							
				focusObject.transform.rotation = Quaternion.Euler(eulerAngles);
				*/
				m_stylusLastPos = Input.mousePosition;
			}
		}
	}
}
