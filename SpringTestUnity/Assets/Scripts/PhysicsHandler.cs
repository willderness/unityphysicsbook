using UnityEngine;
using System.Collections;


public class PhysicsHandler : MonoBehaviour {
	public enum HINT_LEVEL { NO_HINT, HINT_SOME, HINT_ALL };
	
	public bool isPlaying;
	private float velocity_x;
	private float velocity_y_0;
	private float velocity_z;
	private Vector3 gravity;
	private float simStartTime;
	private Vector3 initialPosition;
	private InkBoxBaseball inkbox;
	
	//public bool bRenderTrajectory;
	public int nTrajectoryVertexCount = 15;
	public Material trajectoryMaterial;
	private LineRenderer trajectory;
	private bool bSimComplete;
	public HINT_LEVEL hintLevel = HINT_LEVEL.NO_HINT;
	public float fAirTime = 0;
	
	void Awake() {
		isPlaying = false;
		//bRenderTrajectory = true;
	}
	// Use this for initialization
	void Start () {
		bSimComplete = false;
		velocity_x = 0;
		velocity_y_0 = 5;
		velocity_z = 0;
		gravity = new Vector3(0,-9.8f,0);
		isPlaying = false;
		initialPosition = transform.position;
//		if( bRenderTrajectory )
		{
			trajectory = gameObject.AddComponent<LineRenderer>();
			trajectory.material = trajectoryMaterial;
			trajectory.SetVertexCount( nTrajectoryVertexCount );
			trajectory.SetWidth(0.05f,0.01f);
			for( int i=0; i<nTrajectoryVertexCount; i++ )
				trajectory.SetPosition(i, new Vector3(-1000,-1000,-1000));
			//trajectory.SetWidth(0.1,0.01);
		}
		
		inkbox = gameObject.GetComponent<InkBoxBaseball>();
	}
	public float finalPositionX;
	public float finalPositionZ;
	public Vector3 getFinalPosition()
	{
		return new Vector3(finalPositionX, 0, finalPositionZ );	
	}
	
	
	void Update()
	{
		if( isPlaying )
		{
			if( trajectory != null )
			{
				/*trajectory.SetVertexCount(0);
				Destroy(trajectory);
				trajectory =null;*/
			}
			
			//float velocity_y = velocity_y_0 + 0.5f*gravity.y*Mathf.Pow(Time.time-simStartTime,2);
			float velocity_y = velocity_y_0 + gravity.y*(Time.time-simStartTime);
			rigidbody.velocity =  new Vector3( velocity_x, velocity_y, velocity_z );
			/*transform.position = new Vector3( transform.position.x + (Time.deltaTime*velocity_x),
				transform.position.y + (Time.deltaTime*velocity_y),
				transform.position.z + (Time.deltaTime*velocity_z));*/
			//rigidbody.AddForce( ib.gravity );	
		}
		//else
		{
			
			UpdateValues();
			
			float fStartAirTime = ( -1*velocity_y_0 / gravity.y );
			float s = (velocity_y_0/2.0f) * fStartAirTime;
			float fEndAirTime = Mathf.Sqrt((initialPosition.y+s)/(-0.5f*gravity.y));
			fAirTime = fStartAirTime + fEndAirTime;
			//print( "Airtime: " + fAirTime );
			
				finalPositionX = fAirTime * velocity_x;
				finalPositionZ = fAirTime * velocity_z;		
			if( trajectory != null && hintLevel != HINT_LEVEL.NO_HINT )
				//bRenderTrajectory )
			{
				/*trajectory = gameObject.AddComponent<LineRenderer>();
			trajectory.material = trajectoryMaterial;
			trajectory.SetVertexCount( nTrajectoryVertexCount );
			trajectory.SetWidth(0.1f,0.1f);*/
				
				//trajectory.SetVertexCount( nTrajectoryVertexCount );
				float fTrajectoryMultiplier = 0f;
				if( hintLevel == HINT_LEVEL.HINT_ALL )
					fTrajectoryMultiplier = fAirTime/nTrajectoryVertexCount;
				else if( hintLevel == HINT_LEVEL.HINT_SOME )
					fTrajectoryMultiplier = (0.05f*fAirTime)/(nTrajectoryVertexCount);
					
				trajectory.SetPosition(0, initialPosition );
				trajectory.enabled = true;
				for( int i = 1; i < nTrajectoryVertexCount; i++ )
				{	
					float time_i = i * fTrajectoryMultiplier;
					float v_i = velocity_y_0 + (gravity.y * time_i);
					float v_avg = (v_i + velocity_y_0) * 0.5f;
					Vector3 displacement = new Vector3( 
						time_i* velocity_x, 
						time_i* v_avg, 
						time_i* velocity_z );
					trajectory.SetPosition(i, initialPosition + displacement );
				}			
			}
		}
		
		// Check for ending simulation - this is basically the collision detection now
//		if( transform.position.y < 0.034f )
//		{
//			rigidbody.velocity = Vector3.zero;
//			transform.position = new Vector3( transform.position.x,0.034f, transform.position.z );
//			bSimComplete = true;
//			
//			
//		}
	}	
	void OnCollisionEnter(Collision collision)
	{
//		print("Entering Collision, Physics Handler " + collision.gameObject.name);
//		if( collision.gameObject.name == "stadium" )
//		{
//			
//			rigidbody.velocity = Vector3.zero;
//			//transform.position = new Vector3( transform.position.x,0.034f, transform.position.z );
//			bSimComplete = true;
//		}
	}
	public void Reset()
	{
			transform.position = initialPosition;
			rigidbody.velocity = Vector3.zero;
	}
	public void SetPlaying( bool nowPlaying )
	{
		if( isPlaying == false && nowPlaying == true )
		{
			simStartTime = Time.time;
			SaveValues();
			UpdateValues();			
			bSimComplete = false;
		}
		isPlaying = nowPlaying; 
		if( ! isPlaying )
		{
			rigidbody.velocity = Vector3.zero;
			bSimComplete = true;
		}
	}
	private void UpdateValues()
	{			
		Quaternion tempRotation = Quaternion.identity;
		tempRotation.eulerAngles = new Vector3( 180-inkbox.rho, (360-inkbox.theta)-135, 0 );
		transform.rotation = tempRotation;
	
		rigidbody.mass = inkbox.mass;
		float impulseForce = inkbox.force / inkbox.mass;
		float thetaRads = ((360-inkbox.theta)-135)* Mathf.Deg2Rad;
		float rhoRads = (180-inkbox.rho)* Mathf.Deg2Rad;
		velocity_x = impulseForce * Mathf.Sin(thetaRads);
		velocity_z = impulseForce * Mathf.Cos(thetaRads);
		velocity_y_0 = impulseForce * Mathf.Sin(rhoRads);
		gravity = inkbox.gravity;
	}
	
	private float rho_prev = 0.0f;
	private float theta_prev = 0.0f;
	private float mass_prev = 0.0f;
	private float force_prev = 0.0f;
	private Vector3 gravity_prev = Vector3.zero;
	
	private void SaveValues()
	{
		rho_prev   = inkbox.rho;
		theta_prev = inkbox.theta;
		mass_prev  = inkbox.mass;
		force_prev = inkbox.force;
		gravity_prev = inkbox.gravity;
	}
	
	public void InstantReplay(bool nowPlaying )
	{
		if( isPlaying == false && nowPlaying == true )
		{
			simStartTime = Time.time;
			LoadSavedValues();			
			bSimComplete = false;
		}
		isPlaying = nowPlaying; 
		if( ! isPlaying )
		{
			rigidbody.velocity = Vector3.zero;
		}	
	}
	
	private void LoadSavedValues()
	{			
		Quaternion tempRotation = Quaternion.identity;
		tempRotation.eulerAngles = new Vector3( 180-rho_prev, (360-theta_prev)-135, 0 );
		transform.rotation = tempRotation;
	
		rigidbody.mass = mass_prev;
		float impulseForce = force_prev / mass_prev;
		float thetaRads = ((360-theta_prev)-135)* Mathf.Deg2Rad;
		float rhoRads = (180-rho_prev)* Mathf.Deg2Rad;
		velocity_x = impulseForce * Mathf.Sin(thetaRads);
		velocity_z = impulseForce * Mathf.Cos(thetaRads);
		velocity_y_0 = impulseForce * Mathf.Sin(rhoRads);
		gravity = gravity_prev;
	}
	
	public bool isSimComplete()
	{
		return bSimComplete;
	}
	
	public Vector3 InitialPosition 
	{ 
		get 
		{
			return initialPosition;
		}
	}
}
