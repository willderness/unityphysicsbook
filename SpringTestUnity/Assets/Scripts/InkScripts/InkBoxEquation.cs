using System;
using UnityEngine;
using System.Text.RegularExpressions;
using Mono.CSharp;


public class InkBoxEquation : InkBox
{	
	enum EFields
	{
		EQUATION = 0,
		ENUM_LENGTH = 1
	}
	
	// Wrench attributes
	// Public
	// Private attributes
	private Rect eqnLabel;
	private string equation;
	public Func<float,float,float> generatorFunc;
	
	void Start ()
	{
		// Create label areas for each variable	
		InkStart ();	
		
		LabelRects = new Rect[(int)EFields.ENUM_LENGTH];
		
		canvasOffset = new RectOffset(200, 200, 200, -10);
		canvasRectHideOffset = new RectOffset(100,100,100,0);
		
		int x = 650;
		
		LabelRects[(int)EFields.EQUATION] = new Rect (x, Screen.height - 50, 240, 60);
		x += 290;				
		
		// Initialize game object attributes (i.e. biker attributes)		
		equation = "sin[2*x]";
		results = equation;
		generatorFunc = null;
		OnEnterButtonClick();	
		
		editable = new bool[]{ true };
	}
	
	public override void OnGuiVariables ()
	{
		// Display the labels for each variable	
		GUI.Label (LabelRects[(int)EFields.EQUATION], "f(x,z)=" + equation.Trim());
	}
	
	private bool bHasNewEquation;
	public bool HasNewEquation()
	{	
		if( bHasNewEquation )
		{
			bHasNewEquation = false;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public override void SaveValue(int i)
	{
		// Split into lines
		string[] lines = Regex.Split (results, "\n");
		bHasNewEquation = true;
		//for (int i = 0; i < lines.Length; i++) 
		//{
			string line = lines[0];//lines [i];
			var s = @"new Func<System.Single,System.Single,System.Single> ( (x,z) => ((System.Single)(" + EqnParser.ParseEquation(line) + ")) );";
			print ( s);
			try {
							
				Evaluator.Init(new string [0]);
				"using System;".Run();
				"using UnityEngine;".Run();

 				var func = (Func<float, float,float>)Evaluator.Evaluate(s);	
				
				//if an exception is thrown in Evaluate above, the function will not be updated.
				generatorFunc = func;
				equation = line;
			}
			catch(Exception e )
			{
				Debug.Log("Expression didn't parse: " + s );
				bHasNewEquation = false;
			}
		//}
	}
	
	public override void OnEnterButtonClick ()
	{
		// Split into lines
		string[] lines = Regex.Split (results, "\n");
		bHasNewEquation = true;
		//for (int i = 0; i < lines.Length; i++) 
		//{
			string line = lines[0];//lines [i];
			var s = @"new Func<System.Single,System.Single,System.Single> ( (x,z) => ((System.Single)(" + EqnParser.ParseEquation(line) + ")) );";
			print ( s);
			try {
							
				Evaluator.Init(new string [0]);
				"using System;".Run();
				"using UnityEngine;".Run();

 				var func = (Func<float, float,float>)Evaluator.Evaluate(s);	
				
				//if an exception is thrown in Evaluate above, the function will not be updated.
				generatorFunc = func;
				equation = line;
			}
			catch(Exception e )
			{
				Debug.Log("Expression didn't parse: " + s );
				bHasNewEquation = false;
			}
		//}
	}
}


	static class Extensions
{
 public static object Compile(this string code)
 {
 return Evaluator.Evaluate(code);
 }
 public static void Run(this string code)
 {
 Evaluator.Run(code);
 }
}