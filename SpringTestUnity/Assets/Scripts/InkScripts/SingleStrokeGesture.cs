using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SingleStrokeGestureRecognition
{
    public ConfidenceNode max;
    
    public SingleStrokeGestureRecognition(InkStroke stroke, List<InkPoint> corners, Features features)
    {
        Analyze(stroke, corners, features);
    }

    public void Analyze(InkStroke stroke, List<InkPoint> corners, Features features)
    {
        List<ConfidenceNode> confidenceRatings = new List<ConfidenceNode>();

        //Add all the single stroke gestures to the confidence ratings list
        confidenceRatings.Add(new ConfidenceNode(Is_ScratchOut(features), (int)GestureType.SCRATCHOUT));
        Debug.Log("IsScratchOut = " + confidenceRatings[confidenceRatings.Count-1].confidence);
        confidenceRatings.Add(new ConfidenceNode(Is_Straight_Line(features), (int)GestureType.STRAIGHTLINE));
        Debug.Log("IsStraightLine= " + confidenceRatings[confidenceRatings.Count-1].confidence);
        Debug.Log("");
        
        max = new ConfidenceNode(0.0, -1);

        foreach (ConfidenceNode result in confidenceRatings)
        {
            if (result.confidence > max.confidence)
            {
                max.confidence = result.confidence;
                max.shape = result.shape;
            }
        }
    }

    private double Is_ScratchOut(Features features)
    {
        //Check for a scratchout
        double featuresSum = 0;

        if (features.PathDistance / features.SelfIntersections > 1)
            featuresSum += 1.0;

        if (features.PathDistance / features.BoundingBoxPerimeter > 1.25)
            featuresSum += 2.0;
  
        return Ratio(featuresSum, 3.0);
    }

    private double Is_Straight_Line(Features features)
    {
        //Check for straight line
        double featuresSum = 0.0;

        if (features.TotalCusps == 2)
            featuresSum += 1.0;

        if (features.LineDistance > 25)
            featuresSum += 1.0;

        if (features.StartPointEndPointHorzDiff > 25)
            featuresSum += 1.0;

        featuresSum += Ratio(features.LineDistance, features.PathDistance);

        return Ratio(featuresSum, 3.7);
    }

    double Ratio(double a, double b)
    {
        //Return a number between 0 and 1
        if (a <= b)
            return a / b;
        else
            return b / a;
    }

    double Ratio_Min_Thresh(double a, double b, double c)
    {
        //Return a number between 0 and 1, and if a is under the threshold c return 1
        if (a < c)
            return 1.0;
        else if (a <= b)
            return a / b;
        else
            return b / a;
    }
}
