using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class InkPoint
{
	public Vector3 pos;
	public float time;
	
	public InkPoint( Vector3 point, float time)
	{
		this.pos = point;
		this.time = time;
	}
}

public class InkStroke
{
	public List<InkPoint> points;
	public InkStroke ()
	{
		points = new List<InkPoint> ();
	}
	
	public Rect boundingRect 
	{
		get 
		{
			return new Rect((float)minX, (float)minY, (float)(maxX-minX), (float)(maxY-minY));						
		}
	}

	/*
	~InkStroke( )
	{
		if( line != null )
		{
			LineRenderer.Destroy(line);
			GameObject.Destroy(go,.1f);
		}
	}
	*/
	public void Add( Vector3 point, float time )
	{
		points.Add(new InkPoint(point, time));
	}
	
	public void Add( InkPoint point )
	{
		points.Add(point);
	}
	
	//Function that finds the minimum x point in stroke
    public double minX
    {
		get {
	        double min = 9999999.99;
	
	        foreach (InkPoint point in points)
	        {
	            if (point.pos.x < min)
	                min = point.pos.x;
	        }
	
	        return min;
		}
    }

    //Function that finds the minimum y point in stroke
    public double minY
    {
		get {
	        double min = 9999999.99;
	
	        foreach (InkPoint point in points)
	        {
	            if (point.pos.y < min)
	                min = point.pos.y;
	        }
	
	        return min;
		}
    }

    //Function that finds the maximum x point in stroke
    public double maxX
    {
		get {
	        double max = 0.0;
	
	        foreach (InkPoint point in points)
	        {
	            if (point.pos.x > max)
	                max = point.pos.x;
	        }
	
	        return max;
		}
    }

    //Function that finds the maximum y point in stroke
    public double maxY
    {
		get {
	        double max = 0.0;
	
	        foreach (InkPoint point in points)
	        {
	            if (point.pos.y > max)
	                max = point.pos.y;
	        }
	
	        return max;
		}
    }
	
	/** See Gaussian Smoothing Slide of
	 *  http://www.eecs.ucf.edu/courses/cap6105/fall2012/lectures/preprocessing.pdf
	 *  for explanation of filtering algorithm and nomenclature.
	 **/
	public int gaussSigma = 1;

	private Vector3 gaussFilterSingle (InkStroke ink, int i)
	{
		int threeSigma = 3 * gaussSigma;
		int rangeMin = i - threeSigma;
		int rangeMax = i + threeSigma;
		if (rangeMin < 0 || rangeMax >= ink.points.Count) {
			return ink.points [i].pos;
		}
		
		float twoSigmaSqrd = 2 * Mathf.Pow (gaussSigma, 2);
		
		float w_j_denom = 0f;
		for (int k = -threeSigma; k <= threeSigma; k++) {
			w_j_denom += Mathf.Exp (-1 * (Mathf.Pow ((float)k, 2f)) / twoSigmaSqrd);
		}


		List<InkPoint> subsetPoints = new List<InkPoint> ();
		//List<InkPoint> subsetPointsTail = new List<InkPoint> ();
		/*while( rangeMin < 0 )
		{
			subsetPoints.Add( ink.points[0] );
			rangeMin++;
		}

		while( rangeMax > ink.points.Count )
		{
			subsetPointsTail.Add( ink.points[ink.points.Count-1] );
			rangeMax--;
		}
		*/
		subsetPoints.AddRange (ink.points.GetRange (rangeMin, 1+rangeMax - rangeMin));
		//subsetPoints.AddRange( subsetPointsTail );

		int j = -threeSigma;
		Vector3 filteredPoint = new Vector3 (0, 0, 0);
		for (j = -threeSigma; j <= threeSigma; j++) {
			float w_j = Mathf.Exp (-1 * Mathf.Pow ((float)j, 2f) / twoSigmaSqrd) / w_j_denom;
			//subsetPoints[j+threeSigma].Scale(new Vector3(w_j, w_j, w_j));
			filteredPoint = filteredPoint + (subsetPoints [j + threeSigma].pos * w_j);
		}
		
		// Is this correct - there was no return val
		return filteredPoint;
	}

	public void gaussFilterStroke ()
	{
		InkStroke filtered = new InkStroke ();
		int i = 0;
		for (i = 0; i < points.Count; i++) {
			Vector3 point = gaussFilterSingle (this, i);
			if( point != null )
				filtered.Add (point, points[i].time);
		}
		this.points = filtered.points;
	}
	
	/** See Gaussian Smoothing Slide of
	 *  http://www.eecs.ucf.edu/courses/cap6105/fall2012/lectures/preprocessing.pdf
	 *  for explanation of filtering algorithm and nomenclature.
	 **/	
	public float hookMinPercent = 50;
	public float hookMaxPercent = 50;
	public float dehookDistanceSqr = 400f;	
	
	public void dehooking()
	{
		int hookMin = (int)(points.Count*hookMinPercent/100);
		int hookMax = (int)(points.Count*hookMaxPercent/100);
		Debug.Log( "Count: " + points.Count + " Hookmin " + hookMin + " hookMax " + hookMax );
		float maxDistSqr = 0f;
		List<InkPoint> badPtsToRemove = new List<InkPoint>();
		float distSqr = 0;
		for( int i=1; i< Mathf.Min(hookMin, points.Count - hookMax); i++ )
		{
			distSqr = (points[i].pos-points[0].pos).sqrMagnitude;
			Debug.Log( "i: " + i + "dist|max: " + distSqr + " " + maxDistSqr );
			if( distSqr > dehookDistanceSqr )
				break;
			if( distSqr >= maxDistSqr )
			{
				maxDistSqr = distSqr;
			}
			else
			{
				for( int j=0; j<i; j++ )
					badPtsToRemove.Add(points[j]);
				break;
			}
		}
		maxDistSqr = 0;
		for( int i = points.Count-2; i > Mathf.Max(hookMax, points.Count - hookMin); i-- )
		{			
			distSqr = (points[points.Count-1].pos-points[i].pos).sqrMagnitude;
			if( distSqr > dehookDistanceSqr )
				break;
			if( distSqr >= maxDistSqr )
			{
				maxDistSqr = distSqr;
			}
			else
			{
				for( int j = points.Count - 1; j > i; j-- )
					badPtsToRemove.Add( points[j] );
				break;
			}
		}
		Debug.Log( "Hook: " + badPtsToRemove.Count);
		RemovePointsFromPointsList(badPtsToRemove);
	}
		
	public float SelfIntersectionThreshold = 5;
	public void FilterStroke(  )
	{
		if( points.Count != 0 )
		{
			InkPoint curPt = points[0];
			List<InkPoint> badPtsToRemove = new List<InkPoint>();
			
			for( int i=1; i< points.Count; i++ )
			{
				if( curPt == points[i] ) 
					badPtsToRemove.Add( points[i] );
				else 
					curPt = points[i];
			}
			
			RemovePointsFromPointsList(badPtsToRemove);			
		}
	}
	
	private void RemovePointsFromPointsList( List<InkPoint> badPtsToRemove )
	{
		InkStroke newInk = new InkStroke();
		while( points.Count > 0 )
		{
			if( badPtsToRemove.Count > 0 && points[0] == badPtsToRemove[0] )
			{
				badPtsToRemove.RemoveAt(0);
			}
			else {
				newInk.Add( points[0] );
			}
			points.RemoveAt( 0 );
		}
		
		points = newInk.points;
	}
}
