using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IStraw
{
    public List<InkPoint> corners;
    public List<double> pathDistance;

    int W;
    InkStroke originalStroke;
	InkStroke editedStroke;  // Originally updated sPoints, will now use editedStroke
    //List<Vector2> sPoints;
    //List<int> stylusTimes;
    int[] resampleTimes;
    double meanTime;
    double totalStraw;
	
    //Function that returns the corners for the resampled points
    public void Find_Corners(InkStroke stroke)
    {         
        int index;
        double S;
        totalStraw = 0;
        InkStroke resampled;
        InkStroke resampled1;
        corners = new List<InkPoint>();
        //sPoints = new PointCollection();
        resampleTimes = new int[10000];
        pathDistance = new List<double>();
        List<int> newCorners = new List<int>();
        List<int> newCorners1 = new List<int>();

        if (stroke.points.Count > 6)
        {

            S = Determine_Resample_Spacing(stroke);
            resampled = Resample_Points(0, stroke, S);
            newCorners = Get_Corners(resampled);
            resampled1 = Resample_Points(1, stroke, S);
            newCorners1 = Get_Corners(resampled1);

            index = 0;

            //Combine the corners together
            for (int i = 0; i < newCorners.Count; i++)
            {
                if (newCorners1.Count <= index)
                {
                    while (newCorners1[index] < newCorners[i] - 1)
                    {
                        if (newCorners.Count > i)
                            newCorners[i] = newCorners1[index];
                        else
                            newCorners.Insert(i, newCorners1[index]);

                        index++;
                    }

                    if (newCorners1[index] <= newCorners[i] + 1)
                        index++;
                }
            }

            foreach (int newCorner in newCorners)
                corners.Add(resampled.points[newCorner]);
        }
    }

    //Function finds the interspacing distance for the resampled points
    private double Determine_Resample_Spacing(InkStroke stroke)
    {
        Vector3 topLeft = new Vector3();
        Vector3 bottomRight = new Vector3();

        topLeft.x = (float)stroke.minX;
        topLeft.y = (float)stroke.minY;

        bottomRight.x = (float)stroke.maxX;
        bottomRight.y = (float)stroke.maxY;

        double diagonal = Distance(bottomRight, topLeft);

        return diagonal / 40.0;
    }

    //Function that finds the euclidean (chord) distance between the points px and py
    private double Distance(Vector3 px, Vector3 py)
    {
        double deltaX = py.x - px.x;
        double deltaY = py.y - px.y;
        return Mathf.Sqrt((float)(deltaX * deltaX + deltaY * deltaY));
    }

    //Function that determines the resample points
    private InkStroke Resample_Points(int pass, InkStroke stroke, double S)
    {
        int numS = 0;
        meanTime = 0;
        InkStroke resampled = new InkStroke();

        int numBetween = 0;
        Rect bound = stroke.boundingRect;
        double diag = Distance(new Vector3(bound.x, bound.y, 0), new Vector3(bound.x+bound.width, bound.y+bound.height, 0));
        double cellDist = diag / 40;

        double length = 0;
        int preIndex = 0;

        if (pass == 0)
        {
            resampled.Add(stroke.points[0]);
            resampleTimes[numS] = 0;
            numS++;
        }

        for (int i = 1; i < stroke.points.Count; i++)
        {
            numBetween++;

            double dist = Distance(stroke.points[i - 1].pos, stroke.points[i].pos);

            if (length + dist >= cellDist || (pass == 2 && numS == 0 && length + dist >= cellDist / 2.0))
            {
                Vector3 newPoint = new Vector3();
                Vector3 preP = stroke.points[i - 1].pos;
                Vector3 curP = stroke.points[i].pos;

                if (pass == 2 && numS == 0)
                {
                    newPoint.x = (float)(preP.x + ((cellDist / 2.0 - length) / dist) * (curP.x - preP.x));
                    newPoint.y = (float)(preP.y + ((cellDist / 2.0 - length) / dist) * (curP.y - preP.y));
                }
                else
                {
                    newPoint.x = (float)(preP.x + ((cellDist - length) / dist) * (curP.x - preP.x));
                    newPoint.y = (float)(preP.y + ((cellDist - length) / dist) * (curP.y - preP.y));
                }
                
				float time = stroke.points[i].time - stroke.points[preIndex].time;
                resampled.Add(new InkPoint(newPoint, time));
                length = Distance(newPoint, curP);
                
                if (resampleTimes[numS] < 0)
                    resampleTimes[numS] = 0;

                meanTime += resampleTimes[numS];

                preIndex = i;
                numBetween = 0;
                numS++;

                while (length > cellDist)
                {
					time = stroke.points[i].time - stroke.points[i-1].time;
                    newPoint.x += (float)((cellDist / dist) * (curP.x - preP.x));
                    newPoint.y += (float)((cellDist / dist) * (curP.y - preP.y));
                    resampled.Add(new InkPoint(newPoint, time));
                    length -= cellDist;
     
                    if (resampleTimes[numS] < 0)
                        resampleTimes[numS] = 0;
     
                    meanTime += resampleTimes[numS];
                    numS++;
                }
            }
            else
            {
                length += dist;
                if ((length > cellDist) && (i == stroke.points.Count - 1))
                {
					float time = stroke.points[i].time - stroke.points[preIndex].time;
                	resampled.Add(new InkPoint(stroke.points[i].pos, time));
                    numS++;
                }
            }
        }

        meanTime /= numS;

        return resampled;
    }

    //Function that computes the resampled points that correspond to corners
    private List<int> Get_Corners(InkStroke stroke)
    {
        W = 3;
        double[] straws = new double[stroke.points.Count];

        List<int> corners = new List<int>();
        if (stroke.points.Count > 3+W)
		{
	        corners.Add(0);
	
			straws[0] = 0.0;
	
	        //Middle straws
	        for (int i = W; i < stroke.points.Count - W; i++)
	        {
	            straws[i] = Distance(stroke.points[i - W].pos, stroke.points[i + W].pos);
	            totalStraw += straws[i];
	        }
	
	        //First two straws
	        straws[1] = Distance(stroke.points[0].pos, stroke.points[1 + W].pos) * 2 * W / (1 + W);
	        straws[2] = Distance(stroke.points[0].pos, stroke.points[2 + W].pos) * 2 * W / (2 + W);
	
	        //Last two straws
	        straws[stroke.points.Count - 2] = Distance(stroke.points[stroke.points.Count - 1].pos, stroke.points[stroke.points.Count - 2 - W].pos) * 2 * W / (1 + W);
	        straws[stroke.points.Count - 3] = Distance(stroke.points[stroke.points.Count - 1].pos, stroke.points[stroke.points.Count - 3 - W].pos) * 2 * W / (2 + W);
	
	        corners = Init_Corners(stroke.points, corners, straws);
	        corners = Polyline_Proc(stroke.points, corners, straws);
	        corners = Curve_Process_Pass1(stroke.points, corners);
	        corners = Curve_Process_Pass2(stroke.points, corners);
		}
			
        return corners;
    }

    //Function that determines the initial corner set
    private List<int> Init_Corners(List<InkPoint> points, List<int> corners, double[] straws)
    {
        W = 3;
        double threshold = totalStraw / (points.Count - 2 * W) * 0.95;

        for (int i = W; i < points.Count - W; i++)
        {
            if (straws[i] < threshold)
            {
                double localMin = 10000;
                int localMinIndex = i;
                while ((i < points.Count - W) && (straws[i] < threshold))
                {
                    if (straws[i] < localMin)
                    {
                        localMin = straws[i];
                        localMinIndex = i;
                    }
                    i++;
                }
                corners.Add(localMinIndex);
            }
        }
        corners.Add(points.Count - 1);

        return corners;
    }

    //Function that returns a set of corners post processed with higher level polyline rules
    private List<int> Polyline_Proc(List<InkPoint> points, List<int> corners, double[] straws)
    {
        bool stop = false;
        while (!stop)
        {
            stop = true;
            for (int i = 1; i < corners.Count; i++)
            {
                int c1 = corners[i - 1];
                int c2 = corners[i];

                if (!Is_Line(points, c1, c2, 0.975) && (c2 > c1 + 1))
                {
                    int newC = Halfway_Corner(straws, c1, c2);
                    
                    for (int j = corners.Count; j > i; j--)
                    {
                        if(j == corners.Count)
                            corners.Add(corners[j-1]);
                        else
                            corners[j] = corners[j - 1];
                    }
                    
                    corners[i] = newC; 
                    stop = false;
                }
            }
        }
        
        corners = Adjust_Corners(points, corners);
        corners = Triplet_Collinear_Test(points, corners);
        corners = Sharp_Noise_Process(straws, corners);

        return corners;
    }

    //Function that returns a set of corners post processed with higher level curve rules
    private List<int> Curve_Process_Pass1(List<InkPoint> points, List<int> corners)
    {
        //int preCorner = corners[0];

        for(int i = 1; i < corners.Count - 1; i++)
        {
            double[] angles = Comp_Angles1(points, corners, i);
           // preCorner = corners[i];
            bool notCorner = Not_Corner1(angles, corners, i);

            if(notCorner)
            {
                corners.RemoveAt(i);
                i--;
            }
        }

        return corners;
    }

    //Function that returns a set of corners post processed with higher level curve rules
    private List<int> Curve_Process_Pass2(List<InkPoint> points, List<int> corners)
    {
        for(int i = 1; i < corners.Count - 2; i++)
        {
            bool notCorner = false;
            bool hasCross = false;
            int theCorner = corners[i];
            int start0 = theCorner - 12;
            int end0 = theCorner + 12;
            if (start0 < corners[i - 1]) start0 = corners[i - 1];
            if (end0 > corners[i + 1]) end0 = corners[i + 1];
            int start1 = theCorner - (int)Mathf.Ceil((float)(theCorner - start0) / 3);
            int end1 = theCorner + (int)Mathf.Ceil((float)(end0 - theCorner) / 3);
            double angle3;

            if (theCorner + 1 > points.Count)
                angle3 = Get_Angle(points[theCorner].pos, points[theCorner - 1].pos, points[theCorner + 1].pos);
            else
            {
                if (theCorner == 1)
                    theCorner = 2;
                
                angle3 = Get_Angle(points[theCorner - 1].pos, points[theCorner - 2].pos, points[theCorner].pos);
            }

            if (!Same_Direction(points[theCorner].pos, points[start0].pos, points[end0].pos,
                        points[start1].pos, points[end1].pos))
            {
                start0 = theCorner - 4;
                end0 = theCorner + 4;
                if (start0 < corners[i - 1]) start0 = corners[i - 1];
                if (end0 > corners[i + 1]) end0 = corners[i + 1];
                start1 = theCorner - 1;
                end1 = theCorner + 1;

                if (end1 >= points.Count)
                    end1 = points.Count - 1;

                if (!Same_Direction(points[theCorner].pos, points[start0].pos, points[end0].pos, points[start1].pos, points[end1].pos))
                    continue;
                
                hasCross = true;
            }
            else if (!Is_Line(corners[i - 1], theCorner, 0.975, points) && !Is_Line(theCorner, corners[i + 1], 0.975, points))
            {
                if (!Same_Direction(points[theCorner].pos, points[start0].pos, points[start1].pos,
                            points[end1].pos, points[end0].pos) && angle3 > 135)
                    notCorner = true;
            }

            double angle1 = Get_Angle(points[theCorner].pos, points[start0].pos, points[end0].pos);
            double angle2 = Get_Angle(points[theCorner].pos, points[start1].pos, points[end1].pos);
            
            double threshold = 96.3;
            
            if (Is_Line(corners[i - 1], theCorner, 0.975, points) || Is_Line(theCorner, corners[i + 1], 0.975, points))
                threshold = 128;
            
            if ((angle2 > 26.1 + 0.93 * angle1 && ((angle3 > 31 + angle1 && angle3 > threshold) || angle3 > 161))|| (hasCross && angle2 - angle1 > 15 && angle3 > 20))
                notCorner = true;
            
            if (notCorner)
            {
                for (int j = i; j < corners.Count - 1; j++)
                {
                    corners[j] = corners[j + 1];
                }
                i--;
            }
        }
        return corners;
    }

    //Function that returns whether or not the stroke segment between a and b is a line
    private bool Is_Line(List<InkPoint> points, int a, int b, double threshold)
    {
        double distance = Distance(points[a].pos, points[b].pos);
        double pathDistance = Path_Distance(points, a, b);

        if (distance / pathDistance > threshold)
            return true;
        else
            return false;
    }

    //Function that returns a set of corners post processed with the adjust corners algorithm
    private List<int> Adjust_Corners(List<InkPoint> points, List<int> corners)
    {
        List<Vector3> pos = new List<Vector3>();
        List<double> angle = new List<double>();

        for(int i = 1; i < corners.Count - 1; i++)
        {
            int index = corners[i];
   
            if(index > 2 && index < points.Count - 3)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (pos.Count > j)
                        pos[j] = points[index - 3 + j].pos;
                    else
                        pos.Insert(j, points[index - 3 + j].pos);
                }

                for (int j = 0; j < 5; j++)
                {
                    if (angle.Count > j)
                        angle[j] = Get_Angle(pos[j + 1], pos[j], pos[j + 2]);
                    else
                        angle.Insert(j, Get_Angle(pos[j + 1], pos[j], pos[j + 2]));
                }

                if(angle[1] < angle[3])
                {
                    if(angle[0] < angle[1] && angle[0] < angle[2])
                        index -= 2;
                    else if(angle[1] < angle[2])
                        index--;
                }
                else
                {
                    if(angle[4] < angle[3] && angle[4] < angle[2])
                        index += 2;

                    else if(angle[3] < angle[2])
                        index ++;
                }
                corners[i] = index;
            }
        }

        return corners;
    }

    //Function that returns a possible corner between a and b
    private int Halfway_Corner(double[] straws, int a, int b)
    {
        int quarter = (b - a) / 4;
        
        if (quarter == 0)
            quarter = 1;
        
        double minVal = 9999999.99;
        int minIndex = a + 1;
        for (int i = a + quarter; i < b - quarter; i++)
        {
            if (straws[i] < minVal)
            {
                minVal = straws[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

    //Function that returns a corners post-processed with the triplet collinear test
    private List<int> Triplet_Collinear_Test(List<InkPoint> points, List<int> corners)
    {
        for (int i = 1; i < corners.Count - 1; i++)
        {
            int c1 = corners[i - 1];
            int c2 = corners[i + 1];
            
            if (Is_Line(points, c1, c2, 0.988))
            {
                corners.RemoveAt(i);
                i--;
            }
        }
		
        double meanTime = AverageTimes(resampleTimes);

        for (int i = 1; i < corners.Count - 1; i++)
        {
            int c = corners[i];
            int c1 = corners[i - 1];
            int c2 = corners[i + 1];
            double threshold = 0.9747;
            
            if (c2 - c1 > 10)
                threshold += 0.0053;

            if (resampleTimes[c] > 2 * meanTime || resampleTimes[c - 1] > 2 * meanTime || resampleTimes[c + 1] > 2 * meanTime)
                threshold += 0.0066;
            
            if (Is_Line(points, c1, c2, threshold))
            {
                corners.RemoveAt(i);
                i--;
            }
        }
        return corners;
    }

	double AverageTimes(int[] intArray)
	{
		double sum  = 0;
		int i = 1;
		for(i = 1; i < 10000; i++)
		{
			if (intArray[i] > 0)
				sum+= i;
			else
				break;
		}
		
		return sum/i;
	}
	
    //Function that returns a set of corners that avoid sharp noise
    private List<int> Sharp_Noise_Process(double[] straws, List<int> corners)
    {
        for (int i = 1; i < corners.Count; i++)
        {
            int c1 = corners[i-1];
            int c2 = corners[i];

            if (c2 - c1 <= 1 || (c2 - c1 <= 2 && i == 0 && i == corners.Count - 2))
            {
                if (straws[c1] < straws[c2])
                    corners.Remove(c2);
                else
                    corners.Remove(c1);
                i--;
            }
        }
        return corners;
    }

    //Function that returns the angles used for checking the corner i
    private double[] Comp_Angles1(List<InkPoint> points, List<int> corners, int i)
    {
        int c = corners[i];
        int s = c - 12;
        int e = c + 12;

        double[] a = new double[4];
        Vector3 pos = points[c].pos;
        
        if(s < corners[i-1])
            s = corners[i-1];

        if(e > corners[i+1])
            e = corners[i+1];

		a[1] = Get_Angle(pos, points[s].pos, points[e].pos);

        s = corners[i] - (int)(Mathf.Ceil((float)((c - s) / 3.0)));
        e = corners[i] - (int)(Mathf.Ceil((float)((c - e) / 3.0)));

        a[2] = Get_Angle(pos, points[s].pos, points[e].pos);
        a[3] = Get_Angle(pos, points[c-1].pos, points[c+1].pos);
        
        if(c - corners[i-1] > 6)
            a[3] = Get_Angle(pos, points[c-2].pos, points[c+1].pos);

        if(corners[i+1] - c > 6)
            a[3] = Get_Angle(pos, points[c-1].pos, points[c+2].pos);            
        
        return a;
    }

    //Function that checks if i is a corner
    private bool Not_Corner1(double[] angles, List<int> corners, int i)
    {
        if(angles[3] > 161)
            return true;

        if(angles[2] > 36 + 0.85 * angles[1] && angles[1] > 20 && angles[3] > 80 + 0.55 * angles[1])
            return true;

        if((corners[i] - corners[i-1] < 3 || corners[i+1] - corners[i] < 3) && angles[2] > 130)
            return true;

        return false;
    }

    //Function for calculating the angles for the points center, start, and end
    private double Get_Angle(Vector3 center, Vector3 start, Vector3 end)
    {
        Vector2 direction1 = new Vector2((start.x - center.x), (start.y  - center.y));
        direction1.Normalize();

        Vector2 direction2 = new Vector2((end.x - center.x), (end.y - center.y));
        direction2.Normalize();

        double angle = Mathf.Acos((direction1.x * direction2.x) + (direction1.y * direction2.y));
        return angle * (180 / Mathf.PI);
    }

    //Function for outputing the angles used for checking i
    private double[] Comp_Angles2(List<InkPoint> points, List<int> corners, int i)
    {
        int c = corners[i];
        
        int s1;
        int e1;
        int s0 = c - 12;
        int e0 = c + 12;
        
        double[] a = new double[4];
        Vector3 pos = points[c].pos;

        if(s0 < corners[i-1])
            s0 = corners[i-1];

        if(e0 > corners[i+1])
            e0 = corners[i+1];

        s1 = c - (int)(Mathf.Ceil((float)((corners[i] - s0) / 3.0)));
        e1 = c - (int)(Mathf.Ceil((float)((corners[i] - e0) / 3.0)));

        a[3] = Get_Angle(pos, points[c-1].pos, points[c+1].pos);

        if(Diff_Dir(points, c, s0, e0, s1, e1))
        {
            s0 = c - 4;
            e0 = c + 4;

            if(s0 < corners[i-1])
                s0 = corners[i-1];

            if(e0 > corners[i+1])
                e0 = corners[i+1];

            s1 = c - 1;
            e1 = c + 1;

            if(Diff_Dir(points, c, s0, e0, s1, e1))
            {
                a[0] = -1.0;
                return a;
            }

            a[0] = 0.0;
        }
        else if(!Is_Line(points, c, corners[i-1], 0.975) && !Is_Line(points, c, corners[i+1], 0.975))
            if (Diff_Dir(points, c, s0, s1, e1, e0) && a[3] > 135)
                a[0] = 1.0;

        a[1] = Get_Angle(pos, points[s0].pos, points[e0].pos);
        a[2] = Get_Angle(pos, points[s1].pos, points[e1].pos);
       
        return a;
    }

    //Function that returns true or false if the direction of rotation from oa to ob is different than the direction from oc to od 
    private bool Diff_Dir(List<InkPoint> points, int o, int a, int b, int c, int d)
    {      
        Vector2 d0 = new Vector2((points[a].pos.x - points[o].pos.x), (points[a].pos.y - points[o].pos.y));
        Vector2 d1 = new Vector2((points[o].pos.x - points[b].pos.x), (points[o].pos.y - points[b].pos.y));
        Vector2 d2 = new Vector2((points[c].pos.x - points[o].pos.x), (points[c].pos.y - points[o].pos.y));
        Vector2 d3 = new Vector2((points[o].pos.x - points[d].pos.x), (points[o].pos.y - points[d].pos.y));

        double cross0 = Vector2.Angle(d0, d1);
        double cross1 = Vector2.Angle(d2, d3);

        double result = cross0 * cross1;
        
        if(result > 0)
            return false;
        else
            return true;
    }

    //Function that calculates the path (stroke segment) distance between the points a and b
    private double Path_Distance(List<InkPoint> points, int a, int b)
    {
        double d = 0;
        
        for (int i = a; i < b; i++)
            d += Distance(points[i].pos, points[i + 1].pos);
        
        return d;
    }

    //Function that creates a new resampled point
    private InkPoint New_Point(InkPoint pre, InkPoint nxt, double ratio, int i, int last)
    {
		float time = nxt.time - pre.time;
        InkPoint q = new InkPoint(new Vector3((float)(pre.pos.x + ratio * (nxt.pos.x - pre.pos.x)), (float)(pre.pos.y + ratio * (nxt.pos.y - pre.pos.y)), 0), time);

        

        if(time < 0)
            time = 0;

        q.time = time;
        
        return q;
    }   

    //Function that checks whether the rotate direction is the same
    private bool Same_Direction(Vector3 O, Vector3 A, Vector3 B, Vector3 C, Vector3 D)
    {
        Vector2 d0 = new Vector2(A.x - O.x, A.y - O.y);
        Vector2 d1 = new Vector2(O.x - B.x, O.y - B.y);
        Vector2 d2 = new Vector2(C.x - O.x, C.y - O.y);
        Vector2 d3 = new Vector2(O.x - D.x, O.y - D.y);
		
        double cross0 = Vector2.Angle(d0, d1);
        double cross1 = Vector2.Angle(d2, d3);
        
        double result = cross0 * cross1;
        
        if (result > 0)
            return true;
        else
            return false;
    }
	
	

    //Function that checks whether two points in a line
    private bool Is_Line(int x, int y, double threshold, List<InkPoint> points)
    {
        double distance = Distance(points[x].pos, points[y].pos);
        double pathDist = 0;

        for (int i = x; i < y; i++)
            pathDist += Distance(points[i].pos, points[i + 1].pos);

        if (distance / pathDist > threshold)
            return true;
        else
            return false;
    }
}

// Deleted functions
/*
 * //Function that finds the minimum x point in stroke
    private double Min_X(List<InkPoint> points)
    {
        double min = 9999999.99;

        foreach (Point point in points)
        {
            if (point.x < min)
                min = point.x;
        }

        return min;
    }

    //Function that finds the minimum y point in stroke
    private double Min_Y(List<InkPoint> points)
    {
        double min = 9999999.99;

        foreach (Point point in points)
        {
            if (point.y < min)
                min = point.y;
        }

        return min;
    }

    //Function that finds the maximum x point in stroke
    private double Max_X(List<InkPoint> points)
    {
        double max = 0.0;

        foreach (Point point in points)
        {
            if (point.x > max)
                max = point.x;
        }

        return max;
    }

    //Function that finds the maximum y point in stroke
    private double Max_Y(List<InkPoint> points)
    {
        double max = 0.0;

        foreach (Point point in points)
        {
            if (point.y > max)
                max = point.y;
        }

        return max;
    }
    
    */


