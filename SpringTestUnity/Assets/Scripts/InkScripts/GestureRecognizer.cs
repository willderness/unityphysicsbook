using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Node that contains the shape type and the confidence value
public struct ConfidenceNode
{
    public double confidence;
    public int shape;

    public ConfidenceNode(double conf, int s)
    {
        confidence = conf;
        shape = s;
    }
}

//Enum for gestures and shapes
enum GestureType { SQUARE, RECTANGLE, TRIANGLE, CIRCLE, ELLIPSE, ARROW, SCRATCHOUT, TAP, STRAIGHTLINE, ZERO, ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, PLUS, MINUS, ASTERISK, T, A, N, S, C, I, SQUAREROOT, NODE, ERROR };

public class GestureRecognition
{
    public Vector3 centroid;
    public ConfidenceNode recognized;

    public void Analyze(InkStroke stroke, List<InkPoint> corners)
    {
		if( stroke.points.Count == 0 )
		{
			recognized.shape = -1;
		}
		else
		{
        recognized = new ConfidenceNode(0.0, -1);
        Features features = new Features(stroke, corners);
        centroid = features.BoundingBoxCenter;

        //Look for shapes
        //ShapeRecognition shapeRecognizer = new ShapeRecognition(points, corners, features);
        
        //Look for single stroke gestures
        SingleStrokeGestureRecognition ssRecognizer = new SingleStrokeGestureRecognition(stroke, corners, features);

        //Find the most confident shape
        //if (shapeRecognizer.max.confidence > recognized.confidence)
        //{
        //    recognized.confidence = shapeRecognizer.max.confidence;
        //    recognized.shape = shapeRecognizer.max.shape;
        //}

        //Find the most confident single stroke gesture
        if (ssRecognizer.max.confidence > recognized.confidence)
        {
            recognized.confidence = ssRecognizer.max.confidence;
            recognized.shape = ssRecognizer.max.shape;
        }

        //Only return the recognized object if it is 90% accurate
        if (recognized.confidence < 0.9)
            recognized.shape = -1;
		}
    }
}
