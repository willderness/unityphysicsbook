using System;
using UnityEngine;

public class TextAreaRenderer: MonoBehaviour {
		void OnPostRender() {
		
		// Start Drawing GL lines
		GL.PushMatrix ();
//		SpringScript.textAreaList[0].mat.SetPass (0);
		//GL.LoadOrtho();
		GL.LoadPixelMatrix ();
		
		//This section works but springscript has been disabled.
//		foreach (TextArea textArea in SpringScript.textAreaList)
//		{
//			if (textArea.mode == Mode.DRAW)
//			{
//				Rect canvasRect = textArea.bounds;
//				
//				GL.Begin (GL.QUADS);
//				GL.Color (Color.white);
//		
//				GL.Vertex3 (canvasRect.x, Screen.height - canvasRect.y, 0);
//				GL.Vertex3 (canvasRect.x, Screen.height - (canvasRect.y + canvasRect.height), 0);
//				GL.Vertex3 (canvasRect.x + canvasRect.width, Screen.height - (canvasRect.y + canvasRect.height), 0);
//				GL.Vertex3 (canvasRect.x + canvasRect.width, Screen.height - canvasRect.y, 0);
//				GL.End ();
//			}
//		}
		/*
		SpringScript.mat.SetPass (1);
		GL.Begin (GL.LINES);
		GL.Color (Color.black);
		
		// Draw ink to canvas 	
		foreach (InkStroke stroke in strokes) {
			int i = 0;
			while (i < stroke.points.Count-1) {
				GL.Vertex (stroke.points [i]);// + new Vector3(20,0,0));
				GL.Vertex (stroke.points [i + 1]);// +  new Vector3(20,0,0));
				i++;
			}	
		}
		GL.End ();
		*/
		GL.PopMatrix ();  
	}
}

