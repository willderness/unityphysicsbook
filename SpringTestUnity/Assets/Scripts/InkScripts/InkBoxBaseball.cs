using System;
using UnityEngine;
using System.Text.RegularExpressions;

public class InkBoxBaseball : InkBox
{	
	enum EFields
	{
		MASS = 0,
		FORCE = 1,
		ANGLE_A = 2,
		ANGLE_B = 3,
		ENUM_LENGTH = 4
	}
	// Hitter attributes
	public float rho;
	public float theta;
	public float mass;
	public float force;
	public Vector3 gravity;
	
	void Start ()
	{
		InkStart ();	
		LabelRects = new Rect[(int)EFields.ENUM_LENGTH];
		
		canvasOffset = new RectOffset(125, 125, 330, -30);
		canvasRectHideOffset = new RectOffset(100,100,100,0);
		
		int x = 400;
		LabelRects[(int)EFields.MASS] = new Rect (x, Screen.height - 50, 140, 60);
		x += 200;
		LabelRects[(int)EFields.FORCE] = new Rect (x, Screen.height - 50, 140, 60);
		x += 200;
		LabelRects[(int)EFields.ANGLE_A] = new Rect (x, Screen.height - 50, 130, 60);
		x += 180;
		LabelRects[(int)EFields.ANGLE_B] = new Rect (x, Screen.height - 50, 130, 60);
		x += 180;		
		//EquationRect = new Rect (x, Screen.height - 50, 90, 30);
		//x += 100;
		
		// Initialize game object attributes (i.e. baseball attributes)
		rho = 10;
		mass = 1;
		force = 35;
		gravity = new Vector3 (0, -9.8f, 0);		
		
		canvasRect = new Rect(0,0,0,0);//		canvasOffset.Add(LabelRects[0]);
		
		editable = new bool[]{ true, true, true, true };
	}
	
	public override void OnGuiVariables ()
	{
		
		GUI.Label (LabelRects[(int)EFields.MASS], "mass = " + mass.ToString () + "kg");
		GUI.Label (LabelRects[(int)EFields.FORCE], "force = " + force.ToString () + "N");
		GUI.Label (LabelRects[(int)EFields.ANGLE_A], "a = " + theta.ToString () + "\u00B0");
		GUI.Label (LabelRects[(int)EFields.ANGLE_B], "b = " + rho.ToString () + "\u00B0");	
		//GUI.Label (EquationRect, "Equations...");
	}
	
	public override void SaveValue(int i)
	{
		string input = results.Replace(" ", string.Empty);
		double Num;
		bool isNum = double.TryParse(input, out Num);
		
		if (isNum) 
		{			
			switch (i) 
			{
				case (int)EFields.MASS:
					this.mass = (float)Num;
					break;
				case (int)EFields.FORCE:
					this.force = (float)Num;
					break;
				case (int)EFields.ANGLE_A:
					this.theta = (float)Num;
					break;
				case (int)EFields.ANGLE_B:
					this.rho = (float)Num;
					break;
				default:
					break;
			}
		}
	}
	
	public override void OnEnterButtonClick ()
	{
		// Split into lines
		string[] lines = Regex.Split (results, "\n");
		for (int i = 0; i < lines.Length; i++) {
			string line = lines [i];
			InkBox.VarType varType = InkBox.VarType.UNKNOWN;
					
			// In the outfield scene, you can't edit m, f, a, b
			if ( (line.Contains ("m") || line.Contains ("M")))
				varType = InkBox.VarType.MASS;
			else if ((line.Contains ("f") || line.Contains ("F")))
				varType = InkBox.VarType.FORCE;
			else if ( (line.Contains ("a") || line.Contains ("A")))
				varType = InkBox.VarType.ANGLE_HORIZ;
			else if ( (line.Contains ("b") || line.Contains ("B")))
				varType = InkBox.VarType.ANGLE_VERT;
					
			if (varType != InkBox.VarType.UNKNOWN) {
				double Num;
				line = line.Replace (" ", string.Empty);
				line = line.Remove (0, 2);
				bool isNum = double.TryParse (line, out Num);
				if (isNum) {			
					switch (varType) {
					case InkBox.VarType.MASS:
						this.mass = (float)Num;
						break;
					case InkBox.VarType.FORCE:
						this.force = (float)Num;
						break;
					case InkBox.VarType.ANGLE_HORIZ:
						this.theta = (float)Num;
						break;
					case InkBox.VarType.ANGLE_VERT:
						this.rho = (float)Num;
						break;
					default:
						break;
					}
				}
			}				
		}
	}
	
	public override void DoDragDelta( Vector3 dragDelta )
	{
		Quaternion newRotation = Quaternion.LookRotation(dragDelta*-1);
		theta = (360-newRotation.eulerAngles.y)+45;
		rho = newRotation.eulerAngles.x;
		force = dragDelta.magnitude * 10;
	}
}

