using System;
using UnityEngine;
using System.Text.RegularExpressions;

public class InkBoxOutfield : InkBoxBaseball
{	

	enum EFields
	{
		VELOCITY = 0,
		TIME = 1,
		//DISTANCE = 2,
		ENUM_LENGTH = 2
	}
	
	// Outfileder attributes
	// Public
	public float distance;
	public float velocity;
	public float time;
	
	// Private
	private Rect vLabel;
	private Rect dLabel;
	private Rect tLabel;

	void Start ()
	{
		InkStart ();			

		LabelRects = new Rect[(int)EFields.ENUM_LENGTH];
		
		canvasOffset = new RectOffset(125, 125, 330, -30);
		canvasRectHideOffset = new RectOffset(100,100,100,0);
		
		int x = 500;
		
		LabelRects[(int)EFields.VELOCITY] = new Rect (x, Screen.height - 50, 130, 60);
		x += 180;
		LabelRects[(int)EFields.TIME] = new Rect (x, Screen.height - 50, 130, 60);
		x += 180;
		//LabelRects[(int)EFields.DISTANCE] = new Rect (x, Screen.height - 50, 90, 30);
		//x += 100;
		//EquationRect = new Rect (x, Screen.height - 50, 90, 30);
		//x += 100;
		
		// Initialize game object attributes (i.e. baseball attributes)
		rho = 10;
		mass = 1;
		force = 35;
		gravity = new Vector3 (0, -9.8f, 0);		
		velocity = 0;
		distance = 0;
		time = 0;
		
		editable = new bool[]{ true, true };
	}
	
	public override void OnGuiVariables ()
	{		
		GUI.Label (LabelRects[(int)EFields.VELOCITY], string.Format("v = {0:0.00}m/s", velocity));
		GUI.Label (LabelRects[(int)EFields.TIME], string.Format("t = {0:0.00}s", time));
		//GUI.Label (EquationRect, "Equations...");
	}
	
	public override void SaveValue(int i)
	{
		string input = results.Replace(" ", string.Empty);
		double Num;
		bool isNum = double.TryParse(input, out Num);
		
		if (isNum) 
		{			
			switch (i) 
			{
				case (int)EFields.VELOCITY:
					this.velocity = (float)Num;						
					if( this.distance != 0  && this.velocity != 0) 
						this.time = this.distance/this.velocity;						
					break;
				case (int)EFields.TIME:
					this.time = (float)Num;
					if( distance != 0  && this.time != 0) 
						this.velocity = distance / this.time;
					break;			
				default:
					break;
			}
		}
	}
	
	public override void OnEnterButtonClick ()
	{
		// Split into lines
		string[] lines = Regex.Split (results, "\n");
		for (int i = 0; i < lines.Length; i++) {
			string line = lines [i];
			InkBox.VarType varType = InkBox.VarType.UNKNOWN;
					
			// In the outfield scene, you can't edit m, f, a, b
			if (line.Contains ("d") || line.Contains ("D"))
				varType = InkBox.VarType.DISTANCE;
			else if (line.Contains ("v") || line.Contains ("V"))
				varType = InkBox.VarType.VELOCITY;
			else if (line.Contains ("t") || line.Contains ("T"))
				varType = InkBox.VarType.TIME;
					
			if (varType != InkBox.VarType.UNKNOWN) {
				double Num;
				line = line.Replace (" ", string.Empty);
				line = line.Remove (0, 2);
				bool isNum = double.TryParse (line, out Num);
				if (isNum) {			
					switch (varType) {
					case InkBox.VarType.VELOCITY:
						this.velocity = (float)Num;
						
						if( this.distance != 0  && this.velocity != 0) 
							this.time = this.distance/this.velocity;
						
						break;
						/*
					case InkBox.VarType.DISTANCE:
						this.distance = (float)Num;
						break;
						*/
					case InkBox.VarType.TIME:
						this.time = (float)Num;
						if( distance != 0  && this.time != 0) 
							this.velocity = distance / this.time;
						break;
					default:
						break;
					}
				}
			}				
		}
	}
}

