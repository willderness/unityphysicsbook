using UnityEngine;
using System.Collections;

public class ShowOverlay : MonoBehaviour {
	public Texture [] images;
	public Texture overlayBg;
	public bool bShowOverlay;
	public Rect rectHintBox;
	public Rect rectHintOverlay;
	private int maxWidth = 0;
	public GUISkin guiSkin;
	// Use this for initialization
	void Start () {
	  	bShowOverlay = false;
		rectHintBox = new Rect( Screen.width*.95f, 0, Screen.width*0.05f, Screen.height);
		foreach( Texture image in images )
		{
			maxWidth = Mathf.Max( image.width, maxWidth );
		}
		rectHintOverlay = new Rect( Screen.width - maxWidth - 20, 0, Screen.width, Screen.height );
	}
	void OnGUI() {
		GUI.skin = guiSkin;
		if(  rectHintBox.Contains( Input.mousePosition ) )
		{
			GUI.DrawTexture(rectHintOverlay, overlayBg, ScaleMode.StretchToFill);
			int heightDelta = 0;
			foreach( Texture image in images )
			{
				GUI.DrawTexture( new Rect( Screen.width-image.width - 10, Screen.height/4 + heightDelta, 
											image.width,image.height), image, ScaleMode.ScaleToFit );
				heightDelta += image.height + 20;
			}
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
