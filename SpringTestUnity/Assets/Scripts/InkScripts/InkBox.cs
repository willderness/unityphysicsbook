
#define FLIPPING
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using ClientDll;


public enum Mode {DRAW, TEXT};

public class InkBox : MonoBehaviour {
	
	protected enum VarType {
		UNKNOWN,
		MASS,
		FORCE,
		ANGLE_HORIZ,
		ANGLE_VERT,
		DISTANCE,
		VELOCITY,
		TIME,
		HEIGHT,
		PE,
		KE,
		TORQUE,
	};
	
	// Public attributes
	public Material mat;
	public bool visible;
	public Rect canvasRect;
	
	// Private attributes
	// Window/Canvas items
	private Rect buttonRect;	
	private bool buttonPressed = false;
	private bool outfieldScene = false;
	
	//public int canvasWidth = 1000;
	
	
	// Ink items
	private List<InkStroke> strokes = new List<InkStroke> ();
	private InkStroke prevStrokeUnfilt = new InkStroke();	
	private bool drawing = false;
	private Vector3 currMousePos = Vector3.zero;
	private GUIStyle guiStyleTextField;
	
	// Server processing items
	private Client client;
	public string results = "";	
	private System.Diagnostics.Process server;	
	private System.Diagnostics.Process stylusEmulator;	
	//Overlay images items.
	public Texture [] equationImages;
	public Texture overlayBg;
	public Rect showCanvasRect;
	public GUISkin guiSkin;
	protected RectOffset canvasRectHideOffset;
	protected Rect[] LabelRects;
	protected RectOffset canvasOffset;
	protected int currentCanvasRect = -1;
	protected Rect equationRect;
	protected bool equationsVisible = false;
	protected RectOffset equationCanvasOffset = new RectOffset(100, 100, 330, -30);
	protected bool[] editable;
	protected Rect selectedRect;
	
	public bool Contains(Vector2 pt) 
	{
		if( canvasRect.Contains( pt ) && ! buttonRect.Contains(pt) )
		{
			//print( "Contains: true" + " " + Time.deltaTime );
		
			return true;
		}
		return false;
	}
	
	void Awake()
    {
		System.Diagnostics.Process[] processes;/*= System.Diagnostics.Process.GetProcessesByName("RemoteRecognizer.exe");		
		foreach (System.Diagnostics.Process process in processes)
		{
		    process.Kill();
		}
		*/	
		foreach(System.Diagnostics.Process myProc in System.Diagnostics.Process.GetProcesses())
		{
			try
			{
				if (myProc.ProcessName == "RemoteRecognizer.exe")
				{
					myProc.Kill();
					//procFound = true;
					break;
				}
			}
			catch (System.Exception ex)
			{ 
			}
		}		
		System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
		startInfo.FileName = ExeLocations.RecognizerExe;
		startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;		
		server = new System.Diagnostics.Process();
		server.StartInfo = startInfo;
		server.Start();
		
		bool procFound = false;
		foreach(System.Diagnostics.Process myProc in System.Diagnostics.Process.GetProcesses())
		{
			try
			{
				if (myProc.ProcessName == "StylusMouseEmulation.exe")
				{
					//myProc.Kill();
					procFound = true;
					break;
				}
			}
			catch (System.Exception ex)
			{ 
			}
		}		
		//processes = System.Diagnostics.Process.GetProcessesByName("StylusMouseEmulation.exe");		
		///if( processes.Length == 0 )
		if( ! procFound && ExeLocations.stylusEmulator == null )
		{
			startInfo = new System.Diagnostics.ProcessStartInfo();
			startInfo.FileName = ExeLocations.StylusEmulator;
			startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
			ExeLocations.stylusEmulator = new System.Diagnostics.Process();
			ExeLocations.stylusEmulator.StartInfo = startInfo;
			ExeLocations.stylusEmulator.Start();
		}
		
	}
	void OnDestroy()
	{
		try
		{
		client.Disconnect();
			
		//server.Kill();
		if(ExeLocations.stylusEmulator != null)
		{
			ExeLocations.stylusEmulator.Kill();
			ExeLocations.stylusEmulator = null;
		}
		}
		catch (System.Exception ex)
		{}
		
	}
	
	// Use this for initialization
	protected void InkStart () {		
		// Initialize Client
		client = new Client();
		client.Connect();
		if (!client.Initialized)
			Debug.Log("Failed to connect to recognition engine");	
		
		
		// GUI elements
		guiStyleTextField = new GUIStyle();
		guiStyleTextField.fontSize = 75;
		
		// Initialize Ink Box attributes
		//this.canvasRect = new Rect(Screen.width-canvasWidth, 0,  canvasWidth, Screen.height);
		this.canvasRect = new Rect(Screen.width-Screen.width/2, 0,  Screen.width/2, Screen.height);
		this.buttonRect = new Rect(Screen.width-Screen.width/2, Screen.height*9/10,  Screen.width/2, Screen.height*1/10);
		this.showCanvasRect = new Rect( Screen.width*.95f, 0, Screen.width*0.05f, Screen.height);

//        this.massLabel = new Rect(40, 320, 100, 30);
//        this.forceLabel = new Rect(200, 320, 100, 30);
//        this.aLabel = new Rect(40, 370, 100, 30);
//        this.bLabel = new Rect(200, 370, 100, 30);
		
		visible = false;
		equationsVisible = false;
		
		// Initialize drawing attributes
		strokes = new List<InkStroke> ();
	}
	
	// Update is called once per frame
	void Update () {
		//print(Time.deltaTime + " Strokes: " + strokes.Count );
//		if( strokes.Count != 0 )
//		{
//			print(Time.deltaTime + "SC: " + strokes[0].points.Count );
//		}
		if (!client.Initialized)
		{
		//	client.Connect();		
		}
			
		HandleDrawing();
		
		Vector2 flippedMousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
		if( visible || equationsVisible)
		{
			//Mouse y coordinate is reversed but hintbox is height of entire screen, so it doesn't matter.
			if(  (! canvasRect.Contains( flippedMousePos )) && (! drawing) && (! selectedRect.Contains( flippedMousePos )) )
			{
				visible = false;				
				equationsVisible = false;
				
				// save recognized value if it's an int
				SaveValue(currentCanvasRect);
				
			 	ClearStrokes();
			}
		}
		else
		{
		    for( int i =0; i < (int)LabelRects.Length; i++ )
			{
				if( LabelRects[i].Contains( flippedMousePos ) && editable[i] )
				{
					ClearStrokes();
					results = "";
					//print(Time.deltaTime + " " + i );
					selectedRect = LabelRects[i];
					canvasRect = canvasOffset.Add(LabelRects[i]);
					visible = true;
					equationsVisible = false;
					currentCanvasRect = i;
				}
			}
			
			if (equationRect.Contains(flippedMousePos))
			{
				equationsVisible = true;
				selectedRect = equationRect;
				visible = false;
				if (Application.loadedLevel == 3)
				{
					RectOffset newOffset = new RectOffset(100, 100, 520, -30);
					canvasRect = newOffset.Add(equationRect);
				}
				else
					canvasRect = equationCanvasOffset.Add(equationRect);
			}
			/*//Mouse y coordinate is reversed but hintbox is height of entire screen, so it doesn't matter.
			if(  canvasRectHideOffset.Add(canvasRect).Contains( flippedMousePos ) )
			{
				visible = true;
			}*/
			
		}//*/visible = true; //WLH TEMPORARY while debugging.
		
	}
	
	public void DoPostRender() {
		if (!mat) {
			Debug.LogError ("Please Assign a material on the inspector");
			return;
		}
		
		//print ( "R: " + visible + " " + Time.time);
		//Dont render lines when not visible.
		if( visible )
		{ 
#if OLDLINESTYLE	
			// Start Drawing GL lines
			GL.PushMatrix ();
	
			mat.SetPass (0);
			//GL.LoadOrtho();
			GL.LoadPixelMatrix ();
	
				GL.Begin (GL.QUADS);
				GL.Color (Color.white);
		
				GL.Vertex3 (canvasRect.x, 					 Screen.height - canvasRect.y, 0);
				GL.Vertex3 (canvasRect.x, 					 Screen.height - (canvasRect.y + canvasRect.height), 0);
				GL.Vertex3 (canvasRect.x + canvasRect.width, Screen.height - (canvasRect.y + canvasRect.height), 0);
				GL.Vertex3 (canvasRect.x + canvasRect.width, Screen.height - canvasRect.y, 0);
				GL.End ();
		
			//mat.SetPass (1);
			GL.Begin (GL.LINES);
			GL.Color (Color.black);
	//		Vector3 screenFlip = new Vector3(0, Screen.height, 0);
			// Draw ink to canvas 	
			//Vector3 flippedCanvasPos = new Vector3( canvasPos.x, - canvasPos.y, 0 );
			
			
			foreach (InkStroke stroke in strokes) {
				int i = 0;
				while (i < stroke.points.Count-1) {
					GL.Vertex ((stroke.points [i].pos) );// + new Vector3(20,0,0));
					GL.Vertex ((stroke.points [i + 1].pos) );// +  new Vector3(20,0,0));
					i++;
				}	
			}
			GL.End ();

			GL.PopMatrix ();  
				
#endif
		}	
	}

	void OnGUI() 
	{
		if( visible )
		{ 
			GUI.Box( canvasRect, "", guiSkin.box);
			foreach (InkStroke stroke in strokes) 
			{
				int i = 0;
				while (i < stroke.points.Count-1) {
					DrawLine(stroke.points [i].pos, stroke.points [i + 1].pos);
					i++;
				}	
			}
		}
		
		if (equationsVisible && Application.loadedLevel != 8)
		{
			GUI.Box( canvasRect, "", guiSkin.box);
			int heightDelta = 0;
			//foreach( Texture image in equationImages )
			//{
				GUI.DrawTexture( canvasRect, equationImages[0], ScaleMode.ScaleToFit );
				heightDelta += equationImages[0].height + 20;
			//}
		}
		
		GUI.skin = guiSkin;
		
		// Text boxes to display variables and values
		OnGuiVariables();
		
		// Create recognition results textbox
		Rect resultsRect = LabelRects[(int)LabelRects.Length - 1];
		if (Application.loadedLevel == 4 || Application.loadedLevel == 5) resultsRect.x = resultsRect.x + resultsRect.width + 60;
		else resultsRect.x = resultsRect.x + resultsRect.width + 90;
		resultsRect.width = 240;
		GUI.Label (resultsRect, "Recognition: " + results.ToString ());
		
		// Create equations textbox
		if (Application.loadedLevel != 8)
		{
			equationRect = resultsRect;
			if (Application.loadedLevel == 4 || Application.loadedLevel == 5) equationRect.x = resultsRect.x + resultsRect.width + 60;
			else equationRect.x = resultsRect.x + resultsRect.width + 90;
			equationRect.width = 140;
			GUI.Label (equationRect, "Equations... ");
		}
		
		GUI.enabled = true;	
    }
	void AddPointToStrokes(Vector2 point )
	{
		if( prevStrokeUnfilt != null )
			prevStrokeUnfilt.Add (currMousePos, Time.time);
		if( strokes.Count > 0 )
			strokes [strokes.Count - 1].Add (currMousePos, Time.time);
	}

	void HandleDrawing() {

		currMousePos = Input.mousePosition;

		// Check if button pressed
		buttonPressed = Input.GetMouseButton(0);
		Vector2 flippedMousePos = new Vector2(currMousePos.x, Screen.height - currMousePos.y);
		//buttonPressed = GameObject.Find("ZSStylus").GetComponent<ZSStylusSelector>().GetButton(0); //screenPos.z > 0.39 ? true : false; //
		
		if (buttonPressed && (!drawing) && visible) {
			// Check here if trying to draw within one of the canvasses
			if (this.Contains(flippedMousePos))
			{
				drawing = true;
				InkStroke newStroke = new InkStroke ();
				strokes.Add (newStroke);
				prevStrokeUnfilt = new InkStroke ();
				AddPointToStrokes( flippedMousePos );
			}	
		} 
		else if (drawing && (!buttonPressed) )
		{
			drawing = false;
			//Do Gaussian Smoothing of entire list.
			if (strokes.Count > 0)
			{
				strokes[strokes.Count-1].FilterStroke( );
				//strokes[strokes.Count-1].dehooking();
				strokes[strokes.Count-1].gaussFilterStroke(  );
				//strokes [strokes.Count - 1] = filteredStroke; //prevStrokeUnfilt;//
				StrokeCollected( strokes [strokes.Count - 1] );
				//prevStrokeUnfilt = null;
			}
		}
		
		if( drawing )
		{
			if (this.Contains(flippedMousePos))
			{
				//Vector3 clickLocation = new Vector3 (flippedMousePos.x, (flippedMousePos.y), 0); //new Vector3(screenPos.x + 5, screenPos.y + 100, 0); //
				//print("Drawing"+ Time.deltaTime);
				// Check if mouse is within the bounds of the canvas, X < 1920, X > 1200, Y < 200, Y > 0
				//if (drawing) //&& currMousePos.x > canvasRect.x && currMousePos.x < canvasRect.x + canvasRect.width && currMousePos.y < canvasRect.height && currMousePos.y > 0) 				

				AddPointToStrokes( flippedMousePos );
			}
		}	
	}		
	
	void StrokeCollected (InkStroke stroke)
	{
		// Check for scratch out gesture
		IStraw istraw = new IStraw();
		istraw.Find_Corners(stroke);
		GestureRecognition recognizer = new GestureRecognition();
		recognizer.Analyze(stroke, istraw.corners);
		Debug.Log("Gesture Recognized = " + recognizer.recognized.shape);
		
		if (recognizer.recognized.shape == (int)GestureType.SCRATCHOUT)
		{			
			// Remove scribble erase stroke
			InkStroke scirbbleStroke = stroke;
			strokes.Remove(scirbbleStroke);
			
			// Find which strokes are covered by this scratchout
			List<int> indices = new List<int>();
			List<InkStroke> strokesToRemove = new List<InkStroke>();
			for (int i = 0; i < strokes.Count; i++)
			{
				InkStroke currStroke = strokes[i];
				foreach (InkPoint point in scirbbleStroke.points)
				{
					if (currStroke.boundingRect.Contains(point.pos)) 
					{
						indices.Add(i);
						strokesToRemove.Add(currStroke);
						break;
					}
				}
			}						
			
			// Remove strokes and create message string
			strokes.RemoveAll(s=>strokesToRemove.Contains(s));
			string clearStrokeString = Messages.ClearStroke + ",";			
			for (int i = 0; i < indices.Count; i++)
			{
				print( "i: " + i + " indec: " + indices[i] );
				//strokes.RemoveAt(indices[i]);			
				clearStrokeString += "," + indices[i];
			}
			//client.Send(Messages.ClearRecognizer);
			//this.ClearError();
			print( "clearStrokeString = " + clearStrokeString );
			client.Send(clearStrokeString);
	        string result = client.Receive();
			
			results = ExtractMath(result);
			Debug.Log(results);	
		}
		else{
			string strokeString = Messages.InkStroke + ",";
	        for(int i=0 ; i< stroke.points.Count ; ++i)
	        {
	            Vector3 pt = stroke.points[i].pos;
	            strokeString += "," + pt.x.ToString() + "," + (Screen.height - pt.y).ToString();
	        }
					
			
			client.Send(strokeString);
	        string result = client.Receive();
			
			results = ExtractMath(result);
			Debug.Log(results);
			
			//OnEnterButtonClick();		
		
			// save recognized value if it's an int
			SaveValue(currentCanvasRect);
		}		        
	}
	
	string ExtractMath (string serverMsg)
	{
		string[] values = serverMsg.Split (new char[] { ',' });

		//Skip "[message_type],," in the front of the response
		if (values.Length >= 3)
			return values [2];
		else
			return "";
	}
	
	public virtual void OnGuiVariables(){}
	public virtual void OnEnterButtonClick(){}
	public virtual void UpdateGUI(){}
	public virtual void DoDragDelta( Vector3 dragDelta ){}
	public virtual void SaveValue(int i){}
	
	
	public void OnMouseDown()
	{
		if( LabelRects == null ) return;
		Vector2 flippedMousePos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
		
        for( int i =0; i < (int)LabelRects.Length; i++ )
		{
			if( LabelRects[i].Contains( flippedMousePos ) )
			{
				//print(Time.deltaTime + " " + i );
				canvasRect = canvasOffset.Add(LabelRects[i]);
			}
		}
    }
	private void ClearStrokes()
	{
		strokes.Clear();
		client.Send(Messages.ClearRecognizer);
	}
	public GUISkin lineSkin;
	private void DrawLine(Vector2 pointRect, Vector2 point2Rect)
	{
		Matrix4x4 oldMatrix = GUI.matrix;

		GUI.skin = lineSkin;
//	     float x1 = pointRect.x;
//         float y1 = pointRect.y;
//         float x2 = point2Rect.x;
//         float y2 = point2Rect.y;
// 
         float hypotenuseDist = Vector3.Distance(new Vector3(pointRect.x, pointRect.y, 0), new Vector3(point2Rect.x, point2Rect.y, 0));
         /*float adjacentDist = Vector3.Distance(new Vector3(pointRect.x, pointRect.y, 0), new Vector3(point2Rect.x, pointRect.y, 0));
         float drawOneAngle = (Mathf.Acos(adjacentDist/hypotenuseDist)) * Mathf.Rad2Deg;  //Mathf.acos outputs in radians convert to degrees by multiplying by Mathf.Rad2Deg
         float drawTwoAngle = (180 - 90 -drawOneAngle);  //All triangles add up to 180 degrees, so start with 180, subtract the 90 degree right angle as well as the first angle we found from above to find the third angle
         float lineDist = Vector2.Distance(new Vector2(pointRect.x, pointRect.y),new Vector2(point2Rect.x, point2Rect.y));
        Rect lineRect = new Rect (pointRect.x, pointRect.y, lineDist+5, 4);
		//Rect lineRect = new Rect (pointRect.x, pointRect.y, lineDist, 5);
        float rotAngle = 0; 
		//Remember screen.height is inversed
         if (x1>x2 && y1>y2) //Q1
          rotAngle = -drawTwoAngle - 90;
         if (x1<x2 && y1>y2) //Q2
          rotAngle = drawTwoAngle - 90;
         if (x1<x2 && y1<y2) //Q3
          rotAngle = -drawTwoAngle + 90;
         if (x1>x2 && y1<y2) //Q4
          rotAngle = drawTwoAngle + 90;
 
         GUI.backgroundColor = Color.black;
         Vector2 pivotPoint = new Vector2 (lineRect.x, lineRect.y);
         GUIUtility.RotateAroundPivot(rotAngle, pivotPoint);
         GUI.Box(lineRect,"");
         GUI.matrix = oldMatrix;
         GUI.backgroundColor = Color.white;
		//GL.PopMatrix();
		
		*/
		float pixelDensity = 2f;
		for( float t = 0; t < 1; t += (1f/(pixelDensity*hypotenuseDist) ) )
		{
			Vector2 newpoint = Vector2.Lerp(pointRect, point2Rect, t);
			Rect lineRect = new Rect (newpoint.x, Screen.height - newpoint.y, 5, 5);
			
         	GUI.Box(lineRect,"");
		}
	}
}
