using UnityEngine;
using System.Collections;

public class StereoCanvas : MonoBehaviour {
	
	private InkBox inkbox;
	public Material mat;
	
	// Use this for initialization
	void Start () {
		inkbox = FindObjectOfType(typeof(InkBox)) as InkBox;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnPostRender()
	{
#if OLDRENDERWAY
		if (!mat) {
			Debug.LogError ("Please Assign a material on the inspector");
			return;
		}
		
		//print ( "R: " + visible + " " + Time.time);
		//Dont render lines when not visible.
		if( inkbox.visible )
		{ 
			// Start Drawing GL lines
			GL.PushMatrix ();
	
			mat.SetPass (0);
			//GL.LoadOrtho();
			GL.LoadPixelMatrix ();
	
				GL.Begin (GL.QUADS);
				GL.Color (Color.white);
		
				GL.Vertex3 (inkbox.canvasRect.x, 					 Screen.height - inkbox.canvasRect.y, 0);
				GL.Vertex3 (inkbox.canvasRect.x, 					 Screen.height - (inkbox.canvasRect.y + inkbox.canvasRect.height), 0);
				GL.Vertex3 (inkbox.canvasRect.x + inkbox.canvasRect.width, Screen.height - (inkbox.canvasRect.y + inkbox.canvasRect.height), 0);
				GL.Vertex3 (inkbox.canvasRect.x + inkbox.canvasRect.width, Screen.height - inkbox.canvasRect.y, 0);
				GL.End ();
				
//			mat.SetPass (1);
//			GL.Begin (GL.LINES);
//			GL.Color (Color.black);
//	//		Vector3 screenFlip = new Vector3(0, Screen.height, 0);
//			// Draw ink to canvas 	
//			//Vector3 flippedCanvasPos = new Vector3( canvasPos.x, - canvasPos.y, 0 );
//			foreach (InkStroke stroke in strokes) {
//				int i = 0;
//				while (i < stroke.points.Count-1) {
//					GL.Vertex ((stroke.points [i].pos) );// + new Vector3(20,0,0));
//					GL.Vertex ((stroke.points [i + 1].pos) );// +  new Vector3(20,0,0));
//					i++;
//				}	
//			}
//			GL.End ();
			
			GL.PopMatrix ();  
		}	
#endif
	}
}
