using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Features
{
    //List of features
    public Vector3 BoundingBoxCenter;
    public int TotalCusps;
    public double PathDistance;
    public double LineDistance;
    public double LargeSegment;
    public double SmallSegment;
    public int SelfIntersections;
    public List<double> LineDistances;
    public double BoundingBoxPerimeter;
    public double BoundingBoxSmallSegment;
    public double BoundingBoxLargeSegment;
    public double StartEndPointDistance;
    public double TotalTime;
    public double StartPointEndPointHorzDiff;

    public Features(InkStroke stroke, List<InkPoint> corners)
    {
        LineDistance = 0;
        LineDistances = new List<double>();

        //Compute the features
        TotalCusps = corners.Count;
        PathDistance = Path_Distance(stroke.points);
        LineDistances = Line_Distance(corners);
        SmallSegment = Small_Segment(LineDistances);
        LargeSegment = Large_Segment(LineDistances);
        SelfIntersections = Self_Intersections(stroke.points);
        StartEndPointDistance = Start_And_End_Point_Distance(stroke.points);
        BoundingBoxPerimeter = Bounding_Box_Perimeter(stroke);
        BoundingBoxCenter = Bounding_Box_Center(stroke);
        BoundingBoxSmallSegment = Bounding_Box_Small_Segment(stroke);
        BoundingBoxLargeSegment = Bounding_Box_Large_Segment(stroke);
        TotalTime = Total_Time(stroke.points);
        StartPointEndPointHorzDiff = Start_Point_End_Point_Horizontal_Difference(stroke.points);

        //Compute the line distance sum
        foreach (double dist in LineDistances)
            LineDistance += dist;
    }

    private double Start_And_End_Point_Distance(List<InkPoint> points)
    {
        //Distance from the start point to end point
        return Distance(points[0].pos, points[points.Count - 1].pos);    
    }

    private double Distance(Vector3 px, Vector3 py)
    {
        //Distance between two points
        double deltaX = py.x - px.x;
        double deltaY = py.y - px.y;
        return Mathf.Sqrt((float)(deltaX * deltaX + deltaY * deltaY));
    }

    private List<double> Line_Distance(List<InkPoint> corners)
    {
        List<double> distance = new List<double>();
        
        //Add line distances to list
        for (int i = 0; i < corners.Count - 1; i++)
            distance.Add(Distance(corners[i].pos, corners[i + 1].pos));

        //Return list of line distances
        return distance;
    }

    private double Path_Distance(List<InkPoint> points)
    {   
        double d = 0;

        //Compute the path distance
        for (int i = 0; i < points.Count - 1; i++)
            d += Distance(points[i].pos, points[i + 1].pos);

        return d;      
    }

    private double Bounding_Box_Perimeter(InkStroke stroke)
    {
        double boundingPerimeter = 0;

        Rect bound = stroke.boundingRect;

        //Calculate the boudning box perimeter
        boundingPerimeter += 2 * (bound.width + bound.height);

        return boundingPerimeter;
    }

    private int Self_Intersections(List<InkPoint> points)
    {
        int intersections = 0;
        
        //Determine the number of self intersections
        for(int i = 0; i < points.Count - 1; i++)
            for(int j = i+5; j < points.Count - 1; j++)
            {
                //Line 1
                double a1 = points[i+1].pos.y - points[i].pos.y;
                double b1 = points[i].pos.x - points[i+1].pos.x;
                double c1 = points[i+1].pos.x * points[i].pos.y - points[i].pos.x * points[i+1].pos.y;

                //Line 2
                double a2 = points[j+1].pos.y - points[j].pos.y;
                double b2 = points[j].pos.x - points[j+1].pos.x;
                double c2 = points[j+1].pos.x * points[j].pos.y - points[j].pos.x * points[j+1].pos.y;
    
                double denom = a1 * b2 - a2 * b1;

                //Intersection point
                Vector2 intersect = new Vector2((float)((b1 * c2 - b2 * c1) / denom), (float)((a2 * c1 - a1 * c2) / denom));

                //Determine intersections
                bool inSeg = Range(points[i].pos.x, points[i + 1].pos.x, intersect.x) && Range(points[i].pos.y, points[i + 1].pos.y, intersect.y);
                bool inSeg2 = Range(points[j].pos.x, points[j + 1].pos.x, intersect.x) && Range(points[j].pos.y, points[j + 1].pos.y, intersect.y);

                if (inSeg && inSeg2)
                {
                    intersections++;
                }
            }
        return intersections;
    }

    private bool Range(double a, double b, double c)
    {
        //Return true if c is within a and b
        if (c >= a && c <= b)
            return true;
        if (c >= b && c <= a)
            return true;

        return false;
    }

    private Vector3 Bounding_Box_Center(InkStroke stroke)
    {
        //Find centroid of the bounding box
        Rect bound = stroke.boundingRect;
        Vector3 center = new Vector3((bound.x + bound.width) / 2, (bound.y + bound.height) / 2, 0);

        return center;
    }

    private double Bounding_Box_Small_Segment(InkStroke stroke)
    {
        Rect bound = stroke.boundingRect;

        //Return the smallest edge of the bounding box
        if (bound.width < bound.height)
            return bound.width;
        else
            return bound.height;
    }

    private double Bounding_Box_Large_Segment(InkStroke stroke)
    {
        Rect bound = stroke.boundingRect;

        //Return the largest edge of the bounding box
        if (bound.width > bound.height)
            return bound.width;
        else
            return bound.height;
    }
    
    private double Total_Time(List<InkPoint> points)
    {
        double time = 0.0;

        //Determine the time it took to create the stroke
        if (points.Count > 1)
            time = points[points.Count-1].time - points[0].time;

        return time;
    }

    private double Small_Segment(List<double> LineDistances)
    {
        if (LineDistances.Count == 0)
            return 0.0;

        double smallSeg = LineDistances[0];

        //Find the smallest line segment
        foreach (double segment in LineDistances)
            if (segment < smallSeg)
                smallSeg = segment;

        return smallSeg;
    }

    private double Large_Segment(List<double> LineDistances)
    {
        if (LineDistances.Count == 0)
            return 0.0;
        
        double largeSeg = LineDistances[0];

        //Find the largest line segment
        foreach (double segment in LineDistances)
            if (segment > largeSeg)
                largeSeg = segment;

        return largeSeg;
    }

    private double Start_Point_End_Point_Horizontal_Difference(List<InkPoint> points)
    {
        return Mathf.Abs(points[points.Count-1].pos.x - points[0].pos.x);
    }
}
