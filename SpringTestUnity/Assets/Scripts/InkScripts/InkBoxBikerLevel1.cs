using System;
using UnityEngine;
using System.Text.RegularExpressions;

public class InkBoxBikerLevel1 : InkBox
{	
	enum EFields
	{
		HEIGHT = 0,
		MASS = 1,
		GRAVITY = 2,
		VELOCITY = 3,
		PE = 4,
		KE = 5,
		ENUM_LENGTH = 6
	}
	
	
	
	// Biker attributes
	// Public
	public float height;
	public float mass;
	public Vector3 gravity;
	public Vector3 velocity;
	public float potentialEnergy;
	public float kineticEnergy;
	
	// Private attributes
	private Rect heightLabel;
	private Rect massLabel;
	private Rect gravityLabel;
	private Rect velocityLabel;
	private Rect PElabel;
	private Rect KElabel;

	void Start ()
	{
		// Create label areas for each variable	
		InkStart ();	
		
		LabelRects = new Rect[(int)EFields.ENUM_LENGTH];
		
		canvasOffset = new RectOffset(125, 125, 300, -10);
		canvasRectHideOffset = new RectOffset(100,100,100,0);
		
		int x = 150;
		
		LabelRects[(int)EFields.HEIGHT] = new Rect (x, Screen.height - 50, 150, 60);
		x += 190;
		LabelRects[(int)EFields.MASS] = new Rect (x, Screen.height - 50, 150, 60);
		x += 190;
		LabelRects[(int)EFields.GRAVITY] = new Rect (x, Screen.height - 50, 170, 60);
		x += 200;
		LabelRects[(int)EFields.VELOCITY] = new Rect (x, Screen.height - 50, 230, 60);
		x += 260;
		LabelRects[(int)EFields.PE] = new Rect (x, Screen.height - 50, 150, 60);
		x += 180;
		LabelRects[(int)EFields.KE] = new Rect (x, Screen.height - 50, 150, 60);
		x += 180;				
		
		// Initialize game object attributes (i.e. biker attributes)
		height = 5.0f;
		mass = 0;
		gravity = new Vector3 (0, -9.8f, 0);		
		velocity = new Vector3(0,0,0);
		potentialEnergy = 0;
		kineticEnergy = 0;
		
		editable = new bool[]{true, false, false, false, false, false };
	}
	
	public override void OnGuiVariables ()
	{
		// Display the labels for each variable	
		float height_rounded = Mathf.Round(height * 10f) / 10f;  
		
		GUI.Label (LabelRects[(int)EFields.HEIGHT], "height = " + height_rounded.ToString () + "m");
		GUI.Label (LabelRects[(int)EFields.MASS], "mass = " + mass.ToString () + "kg");
		GUI.Label (LabelRects[(int)EFields.GRAVITY], "gravity = " + gravity.y.ToString () + "m/s\u00B2");	
		
		// round values
		float v_x = Mathf.Round(velocity.x * 10f) / 10f;  
		float v_y = Mathf.Round(velocity.y * 10f) / 10f;  
		float v_z = Mathf.Round(velocity.z * 10f) / 10f;
		float pe = Mathf.Round(potentialEnergy * 10f) / 10f;
		float ke = Mathf.Round(kineticEnergy * 10f) / 10f;
		
		// Switch x and z for display purposes
		GUI.Label (LabelRects[(int)EFields.VELOCITY], "velocity = (" + v_z.ToString() + ", " + v_y.ToString() + ", " + v_x.ToString() + ") m/s");		
		GUI.Label (LabelRects[(int)EFields.PE], "PE = " + pe.ToString () + "J");	
		GUI.Label (LabelRects[(int)EFields.KE], "KE = " + ke.ToString () + "J");	
		
		//GUI.Label (EquationRect, "Equations...");
	}	
	
	public override void SaveValue(int i)
	{
		string input = results.Replace(" ", string.Empty);
		double Num;
		bool isNum = double.TryParse(input, out Num);
		
		if (isNum) 
		{			
			switch (i) 
			{
				case (int)EFields.HEIGHT:
					this.height = (float)Num;					
					break;
						
				default:
					break;
			}
		}
	}
	
	public override void OnEnterButtonClick ()
	{
		// Split into lines
		string[] lines = Regex.Split (results, "\n");
		for (int i = 0; i < lines.Length; i++) 
		{
			string line = lines [i];
			InkBox.VarType varType = InkBox.VarType.UNKNOWN;
					
			// In the outfield scene, you can't edit m, f, a, b
			if ( (line.Contains ("h") || line.Contains ("H")))
				varType = InkBox.VarType.HEIGHT;
			//else if ((line.Contains ("f") || line.Contains ("F")))
			//	varType = InkBox.VarType.FORCE;
					
			if (varType != InkBox.VarType.UNKNOWN) 
			{
				double Num;
				line = line.Replace (" ", string.Empty);
				line = line.Remove (0, 2);
				bool isNum = double.TryParse (line, out Num);
				if (isNum) {			
					switch (varType) {
					case InkBox.VarType.HEIGHT:
						this.height = (float)Num;
						break;					
					default:
						break;
					}
				}
			}				
		}
	}
}

