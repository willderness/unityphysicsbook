using System;
using UnityEngine;
using System.Text.RegularExpressions;

public class InkBoxWrench : InkBox
{	
	enum EFields
	{
		FORCE = 0,
		RADIUS = 1,
		THETA = 2,
		TORQUE = 3,
		ENUM_LENGTH = 4
	}
	
	// Wrench attributes
	// Public
	public float radius;
	public float forcePerp; // Perpendicular force amount
	public Vector3 forceVector;
	public float torque;
	public float theta;
	public float maxRadius = 10f;
	
	// Private attributes
	private Rect forcePerpLabel;
	private Rect radiusLabel;
	private Rect thetaLabel;
	private Rect torqueLabel;

	void Start ()
	{
		// Create label areas for each variable	
		InkStart ();	
		
		LabelRects = new Rect[(int)EFields.ENUM_LENGTH];
		
		canvasOffset = new RectOffset(125, 125, 300, -10);
		canvasRectHideOffset = new RectOffset(100,100,100,0);
		
		int x = 300;
		
		LabelRects[(int)EFields.FORCE] = new Rect (x, Screen.height - 50, 150, 60);
		x += 200;
		LabelRects[(int)EFields.RADIUS] = new Rect (x, Screen.height - 50, 170, 60);
		x += 220;
		LabelRects[(int)EFields.THETA] = new Rect (x, Screen.height - 50, 170, 60);
		x += 220;
		LabelRects[(int)EFields.TORQUE] = new Rect (x, Screen.height - 50, 170, 60);
		x += 220;
		
		// Initialize game object attributes (i.e. biker attributes)
		radius = 2.0f;
		forcePerp = 4f;
		forceVector = Vector3.zero;		
		theta = 270f;
		torque = Mathf.Sin(Mathf.Deg2Rad*theta)*radius*forcePerp;		
		
		editable = new bool[]{ true, true, true, false };
	}
	
	public override void OnGuiVariables ()
	{
		// round values
		//float v_x = Mathf.Round(velocity.x * 10f) / 10f; 
		
		// Display the labels for each variable	
		GUI.Label (LabelRects[(int)EFields.FORCE], "Force(f): " + String.Format("{0:F2}N",forcePerp));
		GUI.Label (LabelRects[(int)EFields.RADIUS], "Radius(r): " + String.Format("{0:F2}m",radius));
		GUI.Label (LabelRects[(int)EFields.THETA], "Angle(a): " + String.Format("{0:F2}\u00B0",theta));	
		GUI.Label (LabelRects[(int)EFields.TORQUE], "Torque(t): " +String.Format("{0:F2}Nm", torque));
		
		//GUI.Label (EquationRect, "Equations...");
	}
	
	public override void SaveValue(int i)
	{
		string input = results.Replace(" ", string.Empty);
		double Num;
		bool isNum = double.TryParse(input, out Num);
		
		if (isNum) 
		{			
			switch (i) 
			{
				case (int)EFields.FORCE:
					this.forcePerp = (float)Num;
					this.torque = Mathf.Sin(Mathf.Deg2Rad*theta)*radius*forcePerp;
					break;
				case (int)EFields.RADIUS:
					this.radius = (float)Num;
					this.torque = Mathf.Sin(Mathf.Deg2Rad*theta)*radius*forcePerp;
					break;
				case (int)EFields.THETA:
					this.theta = (float)Num % 360;
					this.torque = Mathf.Sin(Mathf.Deg2Rad*theta)*radius*forcePerp;						
					break;
				default:
					break;
			}
		}
	}
	
	public override void OnEnterButtonClick ()
	{
		// Split into lines
		string[] lines = Regex.Split (results, "\n");
		for (int i = 0; i < lines.Length; i++) 
		{
			string line = lines [i];
			InkBox.VarType varType = InkBox.VarType.UNKNOWN;
					
			// In the outfield scene, you can't edit m, f, a, b
			if ( (line.Contains ("f") || line.Contains ("F")))
				varType = InkBox.VarType.FORCE;
			if ( (line.Contains ("r") || line.Contains ("R")))
				varType = InkBox.VarType.DISTANCE;
			if ( (line.Contains ("a") || line.Contains ("A")))
				varType = InkBox.VarType.ANGLE_HORIZ;
			if ( (line.Contains ("t") || line.Contains ("T")))
				varType = InkBox.VarType.TORQUE;
			//else if ((line.Contains ("f") || line.Contains ("F")))
			//	varType = InkBox.VarType.FORCE;
					
			if (varType != InkBox.VarType.UNKNOWN) 
			{
				double Num;
				line = line.Replace (" ", string.Empty);
				line = line.Remove (0, 2);
				bool isNum = double.TryParse (line, out Num);
				
				if (isNum) {			
					switch (varType) {
					case InkBox.VarType.FORCE:
						this.forcePerp = (float)Num;
						this.torque = Mathf.Sin(Mathf.Deg2Rad*theta)*radius*forcePerp;
						break;	
					case InkBox.VarType.DISTANCE:
						this.radius = (float)Num;
						this.torque = Mathf.Sin(Mathf.Deg2Rad*theta)*radius*forcePerp;
						break;
					case InkBox.VarType.ANGLE_HORIZ:
						this.theta = (float)Num % 360;
						this.torque = Mathf.Sin(Mathf.Deg2Rad*theta)*radius*forcePerp;						
						break;
					case InkBox.VarType.TORQUE:
						this.torque = (float)Num;
						float sineAngle = Mathf.Sin(Mathf.Deg2Rad*theta);
						float forceAngled = sineAngle*forcePerp;
						if( ( torque / forceAngled ) > maxRadius )
						{
							radius = maxRadius;
							forceAngled = torque / maxRadius;
							forcePerp = forceAngled/sineAngle;					
						}
						else
						{
							radius = torque / forceAngled;
						}							
						break;
					default:
						break;
					}
				}
			}				
		}
	}
}

