using UnityEngine;
using System.Collections;

public class StrikeCounter : MonoBehaviour {
	
	public int numStrikesRemaining = 3; 
	public Texture iconTexture;
	// Use this for initialization
	void Start () {
	
	}
	public bool IsOut()
	{
		return (numStrikesRemaining == 0);	
	}
	public void Swing()
	{
		numStrikesRemaining--;
	}
	public uint StrikesRemaining
	{
		get;
		private set;
	}
	void OnGUI()
	{
		if( iconTexture != null )
		{
			GUI.enabled = true;
			float iconStart = ((Screen.width) - (50*numStrikesRemaining))*0.5f;
			for( int i = 0; i < numStrikesRemaining; i++ )
			{
				GUI.DrawTexture(new Rect(iconStart + ( 50 * i),10,50,50), iconTexture, ScaleMode.ScaleToFit, true, 0.0f);
			}
		}
	}
}
