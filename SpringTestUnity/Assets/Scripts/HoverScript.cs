using UnityEngine;
using System.Collections;

public class HoverScript : MonoBehaviour {

	public string levelToLoad;
	public AudioClip soundHover;
	public AudioClip beep;
	public bool quitButton = false;
	private ZSCore zscore;
	void Start()
	{
		zscore = FindObjectOfType(typeof(ZSCore)) as ZSCore;
		if( zscore != null )
		{
			zscore.SetHeadTrackingScale(0.5f);
		}
		
    Screen.showCursor = false;
    Screen.lockCursor = true;
	}
	void OnMouseEnter() {
		audio.PlayOneShot(soundHover);
	}
	void Update()
	{
		/*if( Camera.main == null) return;
		  Ray ray= Camera.main.ScreenPointToRay (Input.mousePosition);
 
 	float Range = 50;
    Debug.DrawRay(ray.origin, ray.direction * 500, Color.green);
 
 	RaycastHit Hit;
    if (Physics.Raycast (ray, out Hit, Range)) {
         Debug.Log("Hit: " + Hit.collider.gameObject.name + Time.deltaTime);
     }*/
		/*if( Camera.main != null )
			transform.LookAt(Camera.main.transform.position);*/	
	}
	void OnDragBegin()
	{
		OnMouseDown();	
	}
	void OnMouseDown() {
		//yield return new WaitForSeconds(0.35);
		if (quitButton) {
			Application.Quit();	
		}
		else{
			Application.LoadLevel(levelToLoad);
		}
	}
	void OnDestroy()
	{
	   Screen.showCursor = true;
    	Screen.lockCursor = false;
	}
}
