using UnityEngine;
using System;
using Mono.CSharp;
using System.Text.RegularExpressions;

public class MenuGraph : MonoBehaviour {
	
	public int resolution = 10;
	public int gridResolution = 11;
	private int currentResolution;
	private ParticleSystem.Particle[] points;
	private string stringToEdit;
	InkBoxEquation inkbox;
	public GameObject prefabGridX;
	public GameObject prefabGridY;
	public GameObject prefabGridZ;
	public float fWorldScale = 2f;
	GameObject[] gridX,gridY,gridZ;
	private Rect xLabelRect, yLabelRect, zLabelRect;
	float halfWorldScale;
	private ZSCore zscore;
	
	public Func<float,float,float> generatorFunc;
	
	void Start () {		
		zscore = FindObjectOfType(typeof(ZSCore)) as ZSCore;
		if (zscore != null)
		{
			zscore.SetHeadTrackingEnabled(true);
			//print("Head tracking scale = " + zscore.GetHeadTrackingScale());
			zscore.SetHeadTrackingScale(1f);
			zscore.SetInterPupillaryDistance(0.01f);
			zscore.SetStereoLevel(1.0f);
			zscore.SetWorldScale(10.0f);//8
			zscore.SetFieldOfViewScale(1.0f);
			zscore.SetZeroParallaxOffset(0.0f);
			zscore.SetNearClip(0.01f);
			zscore.SetFarClip(10000.0f);
		}
		
		
		halfWorldScale = fWorldScale/2;
		CreatePoints();
		//stringToEdit = "sin(x)";
//		e = new Expression(stringToEdit);
		//Eval(EqnParser.ParseEquation(stringToEdit));
		inkbox = GetComponent<InkBoxEquation>();		
	
		gridX = new GameObject[gridResolution];
		//gridY = new GameObject[10];
		gridZ = new GameObject[gridResolution];
		for( int i = 0; i< gridResolution; i++ )
		{			
			gridX[i] = GameObject.Instantiate( prefabGridX ) as GameObject;
			gridX[i].renderer.enabled = bGridEnabled;
			/*gridY[i] = GameObject.Instantiate( prefabGridY ) as GameObject;
			gridY[i].renderer.enabled = bGridEnabled;*/
			gridZ[i] = GameObject.Instantiate( prefabGridZ ) as GameObject;
			gridZ[i].renderer.enabled = bGridEnabled;
		}
		
		xLabelRect = new Rect(10, Screen.height - 200, 200, 50);
		yLabelRect = new Rect(10, Screen.height - 140, 200, 50);
		zLabelRect = new Rect(10, Screen.height - 80, 200, 50);
		
		EvaluateEquation();
	}
	
	private void CreatePoints () {
		if(resolution < 2){
			resolution = 2;
		}
		/*else if(resolution > 100){
			resolution = 100;
		}*/
		currentResolution = resolution;
		points = new ParticleSystem.Particle[resolution * resolution];
		SetPointInitialLocations();
	}
	private void SetPointInitialLocations()
	{
		float incrementX = fWorldScale / (resolution - 1);
		float incrementZ = fWorldScale / (resolution - 1);
		int i = 0;
		for(int x = 0; x < resolution; x++){
			for(int z = 0; z < resolution; z++){
				Vector3 p = new Vector3(x * incrementX - halfWorldScale, 0f, z * incrementZ - halfWorldScale);
				points[i].position = p;
				points[i].color = new Color(p.x, 0f, p.z);
				points[i++].size = 0.1f;
			}
		}
	}

	void Update () {
		if(currentResolution != resolution){
			CreatePoints();
		}
		//FunctionDelegate f = functionDelegates[(int)function];
		float t = Time.timeSinceLevelLoad;
		/*float incrementX = fSliderScaleX / (resolution - 1);
		float incrementZ = fSliderScaleZ / (resolution - 1);
		*/	
		for(int i = 0; i < points.Length; i++){
			Vector3 p = points[i].position;
			float x = p.x * fSliderScaleX;
			float z = p.z * fSliderScaleZ;
		  	/*e.Parameters["x"] = x;
	  		e.Parameters["z"] = z;
			object output =  e.Evaluate();*/
			/*if( e.HasErrors() )
			{
				print(e.Error);
			}*/
			
			//print ("Time: " + Time.timeSinceLevelLoad + " " + output);
			//float.TryParse(output.ToString(), out p.y);
			
			p.y = generatorFunc(x,z) / fSliderScaleY;
			
			
//			p.y = f(p, t);
			points[i].position = p;
			Color c = points[i].color;
			c.g = p.y;
			points[i].color = c;
		}
		
		particleSystem.SetParticles(points, points.Length);
		
		if( bGridEnabled )
		{
			for( int i = 0; i < gridResolution; i++)
			{
				float delta = (float)(i-(0.5f*gridResolution))/10;
				gridX[i].transform.position = new Vector3(0, 0, (((fWorldScale * delta) - (halfWorldScale)) / fSliderScaleZ ) );
				//gridY[i].transform.position = new Vector3( (delta * fSliderScaleY)-0.5f,0, 0);
				gridZ[i].transform.position = new Vector3((((fWorldScale * delta) - (halfWorldScale)) /  fSliderScaleX), 0, 0);
			}
		}
	}
	
	public GUISkin guiSkin;
	float fSliderScaleX = 1f;
	float fSliderScaleY = 1f;
	float fSliderScaleZ = 1f;
	public bool bGridEnabled = false;
	void OnGUI()
	{
		
	}

	
	public void EvaluateEquation ()
	{
		string results = "sin[x]^2 + cos[z]^2";
		// Split into lines
		string[] lines = Regex.Split (results, "\n");
		for (int i = 0; i < lines.Length; i++) 
		{
			string line = lines [i];
			var s = @"new Func<System.Single,System.Single,System.Single> ( (x,z) => ((System.Single)(" + EqnParser.ParseEquation(line) + ")) );";
			print ( s);
			try {
							
				Evaluator.Init(new string [0]);
				"using System;".Run();
				"using UnityEngine;".Run();

 				var func = (Func<float, float,float>)Evaluator.Evaluate(s);	
				
				//if an exception is thrown in Evaluate above, the function will not be updated.
				generatorFunc = func;
			}
			catch(Exception e )
			{
				Debug.Log("Expression didn't parse: " + s );
			}
		}
	}
	
	
}
