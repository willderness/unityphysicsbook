using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour {
	
	public Texture ucfLogoTexture;
	public Texture isuelabLogoTexture;
	
	private ZSCore zscore;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI()
	{
		GUI.DrawTexture(new Rect(10,10,266,100), ucfLogoTexture, ScaleMode.ScaleToFit, true, 0.0f);
		GUI.DrawTexture(new Rect(Screen.width -110,10,100,100), isuelabLogoTexture, ScaleMode.ScaleToFit, true, 0.0f);
	}
}
