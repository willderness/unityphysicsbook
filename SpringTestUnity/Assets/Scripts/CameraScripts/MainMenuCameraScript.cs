using UnityEngine;
using System.Collections;

public class MainMenuCameraScript : MonoBehaviour {
	
	ZSCore zscore;	
	private System.Diagnostics.Process stylusEmulator;	
	
	// Use this for initialization
	void Start () {
		zscore = FindObjectOfType(typeof(ZSCore)) as ZSCore;
		
		System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
		bool procFound = false;
		foreach(System.Diagnostics.Process myProc in System.Diagnostics.Process.GetProcesses())
		{
			try
			{
				if (myProc.ProcessName == "StylusMouseEmulation.exe")
				{
					//myProc.Kill();
					procFound = true;
					break;
				}
			}
			catch (System.Exception ex)
			{ 
			}
		}		
		//processes = System.Diagnostics.Process.GetProcessesByName("StylusMouseEmulation.exe");		
		///if( processes.Length == 0 )
		
		if( ! procFound && ExeLocations.stylusEmulator == null )
		{
			startInfo = new System.Diagnostics.ProcessStartInfo();
			startInfo.FileName = ExeLocations.StylusEmulator;
			startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
			ExeLocations.stylusEmulator = new System.Diagnostics.Process();
			ExeLocations.stylusEmulator.StartInfo = startInfo;
			ExeLocations.stylusEmulator.Start();
		}
	}
	public float WorldScaleCamera = 120;
	// Update is called once per frame
	void Update () {
		if (zscore != null)
		{
			zscore.SetInterPupillaryDistance(0.02f);
			zscore.SetStereoLevel(1.0f);
			//zscore.SetWorldScale(150.0f);
			/*if (Application.loadedLevel == 1)
				zscore.SetWorldScale(40.0f);
			else*/
				zscore.SetWorldScale(WorldScaleCamera);
			
//			if (Application.loadedLevel == 0)
//				zscore.SetHeadTrackingEnabled(true);
//			else
				//zscore.SetHeadTrackingEnabled(false);
			
			zscore.SetFieldOfViewScale(1.0f);
			zscore.SetZeroParallaxOffset(0.0f);
			zscore.SetNearClip(0.01f);
			zscore.SetFarClip(10000.0f);
		}
	}
	
	void OnDestroy()
	{
		//server.Kill();
		if(ExeLocations.stylusEmulator != null)
		{
			ExeLocations.stylusEmulator.Kill();
			ExeLocations.stylusEmulator = null;
		}
	}
}
