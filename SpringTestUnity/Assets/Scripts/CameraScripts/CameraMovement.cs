using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CameraMovement : MonoBehaviour {
	
	
	public sealed class CameraMode {

	    private readonly string name;
	    public readonly int value;
		private static readonly Dictionary<string, CameraMode> instance = new Dictionary<string,CameraMode>();
	
	    public static readonly CameraMode OVERHEAD = new CameraMode (0, "Camera\nOverhead"); 
	    public static readonly CameraMode FOLLOW = new CameraMode (1, "Camera\nFollow"); 
	    public static readonly CameraMode UMPIRE = new CameraMode (2, "Camera\nUmpire");
		public static readonly CameraMode OUTFIELD = new CameraMode (3, "Camera\nOutfield");
			
	    private CameraMode(int value, string name){
	        this.name = name;
	        this.value = value;
			instance[name] = this;
	    }
	
	    public override string ToString(){
	        return name;
	    }
		public static explicit operator CameraMode(string str)
		{
		    CameraMode result;
		    if (instance.TryGetValue(str, out result))
		        return result;
		    else
		        throw new InvalidCastException();
		}
	}
	
	
	GUIContent[] comboBoxList;
	private ComboBox comboBoxControl;// = new ComboBox();
	private GUIStyle listStyle = new GUIStyle();

	public Transform Overhead;
	public Transform Umpire;
	public Transform Outfield;
	
	private int outfieldScenePadding = 0;
	private ZSCore zscore;
	
	private GameObject goBaseball;
	private GameObject goRunner;
	private void Start()
	{
		
		if( Outfield != null )
			comboBoxList = new GUIContent[4];
		else
			comboBoxList = new GUIContent[3];
		comboBoxList[CameraMode.UMPIRE.value] = new GUIContent(CameraMode.UMPIRE.ToString());
		comboBoxList[CameraMode.FOLLOW.value] = new GUIContent(CameraMode.FOLLOW.ToString());
		comboBoxList[CameraMode.OVERHEAD.value] = new GUIContent(CameraMode.OVERHEAD.ToString());
		if( Outfield != null )
		{
			comboBoxList[CameraMode.OUTFIELD.value] = new GUIContent(CameraMode.OUTFIELD.ToString());
			goRunner = GameObject.Find("3rd Person Controller");
		}
		//listStyle.normal.textColor = Color.white; 
		listStyle.onHover.background =
		listStyle.hover.background = new Texture2D(2, 2);
		listStyle.fontSize = 16;		
		listStyle.fixedHeight = 70;
		listStyle.padding.left = 4;
		listStyle.padding.right = 4;
		listStyle.padding.top = 15;
		listStyle.padding.bottom = 10;
 
		/*if (Application.loadedLevelName == "OutfieldScene")
		{
			outfieldScenePadding = 50;
		}*/
		
		comboBoxControl = new ComboBox(new Rect(315 + outfieldScenePadding, 5, 150, 75), comboBoxList[0], comboBoxList, "button", "box", listStyle);
		cameraMode = CameraMode.UMPIRE;
		comboBoxControl.SelectedItemIndex = 0;
		goBaseball = GameObject.FindGameObjectWithTag("Player");
		originalScale = goBaseball.transform.localScale;
		
		zscore = FindObjectOfType(typeof(ZSCore)) as ZSCore;
	}
	public CameraMode cameraMode;
	public GUISkin guiSkin;
	void OnGUI() {
		GUI.skin = guiSkin;
		/*if (GUI.Button(new Rect(60, 10, 150, 50), trackBallText))
		{
        	trackBall = !trackBall;
			trackBallText = "Stop Tracking Ball";	
			
			if( trackBall == false )
			{
				Camera.mainCamera.transform.position = 	mainCamInitPos;
		 		Camera.mainCamera.transform.rotation = mainCamInitRot;
				trackBallText = "Track Ball";
			}
		}*/
					
		comboBoxControl.Show();
	}
	Vector3 originalScale;
	// Update is called once per frame
	void Update () {
		CameraMode oldCameraMode = cameraMode;
		cameraMode = (CameraMode)comboBoxList[comboBoxControl.SelectedItemIndex].text;
		if( oldCameraMode != cameraMode )
		{
			if( cameraMode == CameraMode.OVERHEAD )
			{
				Camera.mainCamera.transform.position = Overhead.position;//	OverheadPos;
		 		Camera.mainCamera.transform.rotation = Overhead.rotation;//OverheadRot;
				
				originalScale = goBaseball.transform.localScale;
				goBaseball.transform.localScale = originalScale * 10;
				
				ZSCore_SetFar();
			}
			else if( cameraMode == CameraMode.UMPIRE ) 
			{	
				Camera.mainCamera.transform.position = Umpire.position;//	UmpirePos;
		 		Camera.mainCamera.transform.rotation = Umpire.rotation; //Rot;
				goBaseball.transform.localScale = originalScale;
				
				ZSCore_SetClose();
			}
			else if( cameraMode == CameraMode.OUTFIELD ) 
			{	
				Camera.mainCamera.transform.position = Outfield.position;//	UmpirePos;
		 		Camera.mainCamera.transform.rotation = Outfield.rotation; //Rot;
				goBaseball.transform.localScale = originalScale * 3;
				
				ZSCore_SetClose();
			}
			if( cameraMode == CameraMode.FOLLOW )
			{
				goBaseball.transform.localScale = originalScale;
				
				ZSCore_SetClose();
			}
		}
		
		if( cameraMode == CameraMode.FOLLOW )
		{
			Camera.mainCamera.transform.position = 	goBaseball.transform.position+ new Vector3(1,1,1.5f);//0.5f, 0.5f, 0.5f) ;
			Camera.mainCamera.transform.LookAt( goBaseball.transform.position );	
		}
		else if( cameraMode == CameraMode.OUTFIELD )
		{		
			if( goRunner != null )
			{
			Camera.mainCamera.transform.position = -(goRunner.transform.position-Outfield.position) + goRunner.transform.position - ( goBaseball.transform.position - goRunner.transform.position).normalized*1f;
			}
			else
				Camera.mainCamera.transform.position = Outfield.position;//	UmpirePos;
	 		Camera.mainCamera.transform.rotation = Outfield.rotation; //Rot;
			Camera.mainCamera.transform.LookAt( goBaseball.transform );
			
		}
	}
	
	private void ZSCore_SetClose()
	{
		if (zscore != null)
		{
			zscore.SetHeadTrackingEnabled(true);
			zscore.SetHeadTrackingScale(1f);
			zscore.SetInterPupillaryDistance(0.01f);
			zscore.SetStereoLevel(1.0f);
			zscore.SetWorldScale(8.0f);
			zscore.SetFieldOfViewScale(1.0f);
			zscore.SetZeroParallaxOffset(0.0f);
			zscore.SetNearClip(0.01f);
			zscore.SetFarClip(10000.0f);
		}
	}
	
	private void ZSCore_SetFar()
	{
		if (zscore != null)
		{
			zscore.SetHeadTrackingEnabled(true);
			zscore.SetHeadTrackingScale(1f);
			zscore.SetInterPupillaryDistance(0.02f);
			zscore.SetStereoLevel(1.0f);
			zscore.SetWorldScale(150.0f);
			zscore.SetFieldOfViewScale(1.0f);
			zscore.SetZeroParallaxOffset(0.0f);
			zscore.SetNearClip(0.01f);
			zscore.SetFarClip(10000.0f);
		}
	}
}
