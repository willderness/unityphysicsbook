using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using ClientDll;

public class MainScript : MonoBehaviour {
	InkBox[] inkBoxes;
		
	void Awake()
	{		
		inkBoxes = FindObjectsOfType(typeof(InkBox)) as InkBox[];       	
	}
	void OnDestroy()
	{
			
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	/*
	void OnPostRender() {
		foreach( InkBox inkBox in inkBoxes )
		{			
    		inkBox.DoPostRender();
       	}
	}*/
	void OnMouseDown()
	{		
		foreach( InkBox inkBox in inkBoxes )
		{			
    		inkBox.OnMouseDown();
       	}
	}
}
