using UnityEngine;
using System.Collections;

public class StylusBat : MonoBehaviour {
	
  	ZSCore m_zsCore;
	bool m_wasButtonPressed;
	bool m_isButtonPressed;
	int STYLUS_BUTTON = 2;
	Matrix4x4 m_lastPose;
	Vector3 startPos;
	
	// Use this for initialization
	void Awake () {
	    m_zsCore = GameObject.Find("ZSCore").GetComponent<ZSCore>();
	}
	
	void Start() {
		startPos = Vector3.zero;	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	float maxDistance = 0;
	void LateUpdate()
    {
		m_wasButtonPressed = m_isButtonPressed;
		//Have to combine mouse state down here so asynchronous clients see it at the right time.
		m_isButtonPressed = m_zsCore.IsTrackerTargetButtonPressed(ZSCore.TrackerTargetType.Primary, STYLUS_BUTTON) || Input.GetMouseButton(STYLUS_BUTTON);

		Matrix4x4 pose = m_zsCore.GetTrackerTargetWorldPose(ZSCore.TrackerTargetType.Primary);
		if (pose != m_lastPose)
		{
		m_lastPose = pose;
		
		Vector3 position = pose * new Vector4(0, 0, 0, 1);
		Vector3 forward = (Vector3)(pose * Vector3.forward);
		Vector3 up = (Vector3)(pose * Vector3.up);
		
		Quaternion rotation = Quaternion.LookRotation(forward, up);
		
		transform.position = position + new Vector3(0,0,-2);
		transform.rotation = rotation;
		//transform.localScale = m_zsCore.GetWorldScale() * Vector3.one;
		}
		
		if( m_isButtonPressed == true )
		{
			if( m_wasButtonPressed == false )
			{
				startPos = transform.position;
			}
			Vector3 delta = startPos - transform.position;
			maxDistance = Mathf.Max( maxDistance, delta.magnitude );
			print( "Max: " + maxDistance  );
		}
	}
}
