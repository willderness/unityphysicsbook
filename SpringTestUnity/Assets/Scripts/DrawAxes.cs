using UnityEngine;
using System.Collections;

public class DrawAxes : MonoBehaviour {
	
	public int vertexCount = 2;
	public Material a90_Material;
	public Material a0_Material;
	private LineRenderer a_90;
	private LineRenderer a_0;
	public GameObject text3D_a90;
	public GameObject text3D_a0;
	Vector3 a90_initialPos;
	Vector3 a0_initialPos;
	Quaternion a90_initialRot;
	Quaternion a0_initialRot;
	public CameraMovement cameraMovement;
	
	// Use this for initialization
	void Start () {
		a_90 = gameObject.AddComponent<LineRenderer>();
		//a90_Material.shader = Shader.Find("Specular");
		//a90_Material.color = Color.red;
		a_90.material = a90_Material;
		a_90.SetVertexCount( vertexCount );
		a_90.SetWidth(0.1f,0.1f);
		
		TextMesh textMesh_a0 = text3D_a0.GetComponent<TextMesh>();
		textMesh_a0.text = textMesh_a0.text + "\u00B0";
		a0_initialPos = text3D_a0.transform.position;
		a0_initialRot = text3D_a0.transform.rotation;
		
		TextMesh textMesh_a90 = text3D_a90.GetComponent<TextMesh>();
		textMesh_a90.text = textMesh_a90.text + "\u00B0";
		a90_initialPos = text3D_a90.transform.position;
		a90_initialRot = text3D_a90.transform.rotation;
				
		cameraMovement = GetComponent<CameraMovement>();
//		a_0 = gameObject.AddComponent<LineRenderer>();
//		a_0.material = a0_Material;
//		a_0.SetVertexCount( vertexCount );
//		a_0.SetWidth(0.1f,0.1f);
		
		axisLocation = new Rect( 50, Screen.height - 230, axisTexture.width, axisTexture.height );
	}
	
	// Update is called once per frame
	void Update () {
		//a_90.SetPosition(0, new Vector3(0.0f, 0.005f, -2.0f));
		//a_90.SetPosition(1, new Vector3(-45.0f, 0.005f, -50.0f));
		if (cameraMovement.cameraMode == CameraMovement.CameraMode.OVERHEAD)
		{
			text3D_a90.transform.position = a90_initialPos;
			text3D_a90.transform.rotation = a90_initialRot;
			
			text3D_a0.transform.position = a0_initialPos;
			text3D_a0.transform.rotation = a0_initialRot;
		}
		else
		{
			text3D_a90.transform.LookAt(Camera.mainCamera.transform);
			text3D_a90.transform.Rotate(new Vector3(0, 170, 0));
			
	//		a_0.SetPosition(0, new Vector3(0.0f, 0.005f, -2.0f));
	//		a_0.SetPosition(1, new Vector3(35.0f, 0.005f, -50.0f));
			text3D_a0.transform.LookAt(Camera.mainCamera.transform);
			text3D_a0.transform.Rotate(new Vector3(0, 170, 0));
		}
	}
	public Texture axisTexture;
	private Rect axisLocation;
	void OnGUI()
	{
		
		GUI.DrawTexture( axisLocation, axisTexture );
	}
}
