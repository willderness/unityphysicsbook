using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ThirdPersonController : MonoBehaviour {
    public AnimationClip idleAnimation;
    public AnimationClip walkAnimation;
    public AnimationClip runAnimation;
	public AudioClip runningSoundEffect;
    
    public float walkMaxAnimationSpeed = 0.75F;
    public float trotMaxAnimationSpeed = 1F;
    public float runMaxAnimationSpeed = 1F;
    public float landAnimationSpeed =1F;
    private Animation _animation;
	private AudioSource runningAudioSource;
	
    enum CharacterState {
        Idle = 0,
        Walking = 1,
        Trotting = 2,
        Running = 3,
    }
	
    private CharacterState _characterState;
    // The speed when walking
    public float walkSpeed = 2.0F;
    // after trotAfterSeconds of walking we trot with trotSpeed
    public float trotSpeed = 4.0F;
    // when pressing "Fire3" button (cmd) we start running
    public float runSpeed = 6.0F;
    public float inAirControlAcceleration = 3.0F;
 
    // The gravity for the character
    public float gravity = 20.0F;
    // The gravity in controlled descent mode
    public float speedSmoothing = 10.0F;
    public float rotateSpeed = 500.0F;
    public float trotAfterSeconds = 3.0F;
 
	// The camera doesnt start following the target immediately but waits for a split second to avoid too much waving around.
	private float lockCameraTimer = 0.0F;
	// The current move direction in x-z
	private Vector3 moveDirection = Vector3.zero;
	// The current x-z move speed
	private float moveSpeed = 0.0F;
   
    // The last collision flags returned from controller.Move
	private CollisionFlags collisionFlags ; 
	 
	// Are we moving backwards (This locks the camera to not do a 180 degree spin)
	private bool movingBack = false;
	// Is the user pressing any keys?
	private bool isMoving = false;
	// When did the user start walking (Used for going into trot after a while)
	private float walkTimeStart = 0.0F;
	private bool isControllable = true;
	
	public bool overrunning = false;
    // Use this for initialization
    void  Awake (){
		moveDirection = transform.TransformDirection(Vector3.forward);
		_animation = GetComponent<Animation>();
		if(!_animation)
			Debug.Log("The character you would like to control doesn't have animations. Moving her might look weird.");
		if(!idleAnimation) {
			_animation = null;
			Debug.Log("No idle animation found. Turning off animations.");
		}
		if(!walkAnimation) {
			_animation = null;
			Debug.Log("No walk animation found. Turning off animations.");
		}
		if(!runAnimation) {
			_animation = null;
			Debug.Log("No run animation found. Turning off animations.");
		}	
		
		runningAudioSource = AddAudio(runningSoundEffect, false, false, 1);
	}
	
	AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, float vol)
	{
		AudioSource newAudio = gameObject.AddComponent<AudioSource>();
		newAudio.clip = clip;
		newAudio.loop = loop;
		newAudio.playOnAwake = playAwake;
		newAudio.volume = vol;
		
		return newAudio;
	}
	
    /*void  UpdateSmoothedMovementDirection (){
		Transform cameraTransform = Camera.main.transform;
    
		// Forward vector relative to the camera along the x-z plane    
		Vector3 forward= cameraTransform.TransformDirection(Vector3.forward);
		forward.y = 0;
		forward = forward.normalized;
		// Right vector relative to the camera
		// Always orthogonal to the forward vector
		 Vector3 right= new Vector3(forward.z, 0, -forward.x);
		float v= Input.GetAxisRaw("Vertical");
		float h= Input.GetAxisRaw("Horizontal");
		// Are we moving backwards or looking backwards
		if (v < -0.2f)
			movingBack = true;
		else
			movingBack = false;
		bool wasMoving= isMoving;
		isMoving = Mathf.Abs (h) > 0.1f || Mathf.Abs (v) > 0.1f;
		// Target direction relative to the camera
		Vector3 targetDirection= h * right + v * forward;
		
		// Lock camera for short period when transitioning moving & standing still
		lockCameraTimer += Time.deltaTime;
		if (isMoving != wasMoving)
			lockCameraTimer = 0.0f;
		// We store speed and direction seperately,
		// so that when the character stands still we still have a valid forward direction
		// moveDirection is always normalized, and we only update it if there is user input.
		if (targetDirection != Vector3.zero)
		{
			// If we are really slow, just snap to the target direction
			if (moveSpeed < walkSpeed * 0.9f)
			{
				moveDirection = targetDirection.normalized;
			}
			// Otherwise smoothly turn towards it
			else
			{
				moveDirection = Vector3.RotateTowards(moveDirection, targetDirection, rotateSpeed * Mathf.Deg2Rad * Time.deltaTime, 1000);
				moveDirection = moveDirection.normalized;
			}
		}
		// Smooth the speed based on the current target direction
		float curSmooth= speedSmoothing * Time.deltaTime;
		// Choose target speed
		//* We want to support analog input but make sure you cant walk faster diagonally than just forward or sideways
		float targetSpeed= Mathf.Min(targetDirection.magnitude, 1.0f);
	
		_characterState = CharacterState.Idle;        
		// Pick speed modifier
		if (Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift))
		{
			targetSpeed *= runSpeed;
			_characterState = CharacterState.Running;
		}
		else if (Time.time - trotAfterSeconds > walkTimeStart)
		{
			targetSpeed *= trotSpeed;
			_characterState = CharacterState.Trotting;
		}
		else
		{
			targetSpeed *= walkSpeed;
			_characterState = CharacterState.Walking;
		}
		
		moveSpeed = Mathf.Lerp(moveSpeed, targetSpeed, curSmooth);
		
		// Reset walk time start when we slow down
		if (moveSpeed < walkSpeed * 0.3f)
			walkTimeStart = Time.time;
		
	}	*/	
	void Start()
	{
		// Check the character's initial position
		startPosition = transform.position;	
	}
	void  Update (){
		
		// Check if hit stadium boundary
//		foreach (GameObject boundary in stadiumBoundaries)
//		{
//			//float distance = boundary.transform.collider.bounds.Intersects(new Bounds(transform.position, transform.localScale));
//			//if (distance < 100.0)
//			if (boundary.transform.collider.bounds.Intersects(new Bounds(transform.position, new Vector3(0.5f, 0.5f, 0.5f))))
//				StopRunning();
//		}
		
		if (!isControllable)
		{
			// kill all inputs if not controllable.
			Input.ResetInputAxes();
		}
	 		
		
		if (isSimRunning && !atTarget)
		{
			animation[runAnimation.name].speed = Mathf.Clamp(6.0f/*controller.velocity.magnitude*/, 0.0f, runMaxAnimationSpeed);
			_animation.CrossFade(runAnimation.name); 
			
			float timePassed = (Time.time)-startTime;	
			
			if (timePassed >= totalTime) atTarget = true;										
			
			Vector3 slope = Vector3.zero;
			if (overrunning)// Add slope
			{
				slope = (endPosition - startPosition)*10;
				totalTime =  Vector3.Distance(startPosition, (endPosition + slope))/ib.velocity;
				
			}
			
			if (!atTarget)
					transform.position = Vector3.Lerp(startPosition, endPosition + slope, timePassed/(totalTime));
		}
		else
		{
			animation.CrossFade(idleAnimation.name);
		}
		
		/*UpdateSmoothedMovementDirection();				
		
		// Calculate actual motion
		Vector3 movement= moveDirection * moveSpeed; //+ new Vector3 (0, verticalSpeed, 0) + inAirVelocity;
		movement *= Time.deltaTime;
		
		// Move the controller
		CharacterController controller = GetComponent<CharacterController>();
		collisionFlags = controller.Move(movement);
		
		// ANIMATION sector
		if(_animation) {			
			if(controller.velocity.sqrMagnitude < 0.1f) {
				_animation.CrossFade(idleAnimation.name);
			}
			else 
			{
				if(_characterState == CharacterState.Running) {
					_animation[runAnimation.name].speed = Mathf.Clamp(controller.velocity.magnitude, 0.0f, runMaxAnimationSpeed);
					_animation.CrossFade(runAnimation.name);    
				}
				else if(_characterState == CharacterState.Trotting) {
					_animation[walkAnimation.name].speed = Mathf.Clamp(controller.velocity.magnitude, 0.0f, trotMaxAnimationSpeed);
					_animation.CrossFade(walkAnimation.name);   
				}
				else if(_characterState == CharacterState.Walking) {
					_animation[walkAnimation.name].speed = Mathf.Clamp(controller.velocity.magnitude, 0.0f, walkMaxAnimationSpeed);
					_animation.CrossFade(walkAnimation.name);   
				}
				
			}			
		}
		// ANIMATION sector
		
		// Set rotation to the move direction
		transform.rotation = Quaternion.LookRotation(moveDirection);	
		*/			 				
	}
	
	void  OnControllerColliderHit ( ControllerColliderHit hit   ){
		//  Debug.DrawRay(hit.point, hit.normal);
		if (hit.moveDirection.y > 0.01f) 
			return;
	}   
	
	float  GetSpeed (){
		return moveSpeed;
	}

	 
	bool  IsGrounded (){
		return (collisionFlags & CollisionFlags.CollidedBelow) != 0;
	}
	 
	Vector3  GetDirection (){
		return moveDirection;
	}
	 
	bool  IsMovingBackwards (){
		return movingBack;
	}
	 
	float  GetLockCameraTimer (){
		return lockCameraTimer;
	}
	 
	bool IsMoving (){
		 return Mathf.Abs(Input.GetAxisRaw("Vertical")) + Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0.5f;
	}	
	
	bool isSimRunning = false; 
	bool atTarget = false;
	float totalTime = 0.0f;
	float startTime = 0.0f;
	float endTime = 0.0f;
	Vector3 startPosition = Vector3.zero;
	Vector3 endPosition = Vector3.zero;
	private InkBoxOutfield ib;
	void OnGUI()
	{
//		if( GUI.Button( new Rect(215,5,50,30), isSimRunning?"Stop":"Go" ) )
//		{			
//			StartRunning();
//		}
	}
	
	public void StartRunning()
	{
		isSimRunning = !isSimRunning;			
			
		runningAudioSource.PlayOneShot(runningSoundEffect);
		
		if (isSimRunning)
		{
			// Check if the user has entered velocity or distance/time
			GameObject obj = GameObject.Find("Baseball");
			ib = obj.GetComponent<InkBoxOutfield>();
			float userVelocity = ib.velocity;
			float userDistance = ib.distance;
			float userTime = ib.time;
			
			totalTime = ib.time;
			// Check the character's initial position
			startPosition = transform.position;
			
			// Check the initial position of the target
			PhysicsHandler ph = GameObject.FindGameObjectWithTag("Player").GetComponent<PhysicsHandler>();
			endPosition = ph.getFinalPosition();//GameObject.Find("Target").transform.position;	
			
			if( endPosition.y != startPosition.y )
				endPosition = new Vector3(ph.finalPositionX, startPosition.y, ph.finalPositionZ );
			
			float velocity = ib.velocity;
			float totalDistance = ib.distance;//Vector3.Distance(startPosition, endPosition);

			print("distance = " + totalDistance + ", time = " + totalTime);
														
			// Rotate character towards target
			transform.LookAt(endPosition);
			
			startTime = Time.time;
		}
		else
		{
			Reset();
			runningAudioSource.Stop();
		}
	}
	
	public void StopRunning()
	{	
		animation.CrossFade(idleAnimation.name);
		runningAudioSource.Stop();
		isSimRunning = false;
	}
	public void Reset()
	{
		// Start back in the outfield
		transform.position = startPosition;
		atTarget = false;
		overrunning = false;
		// Start looking forward
//		Transform cameraTransform = Camera.main.transform;
//		Vector3 forward= cameraTransform.TransformDirection(Vector3.forward);
//		forward.y = 0;
//		forward = forward.normalized;
//		transform.LookAt(forward);
		
	}
	
	void OnControllerColliderHit(Collision collision)
	{
		print("Entering Controller Collider, Baseball Game Manager " + collision.gameObject.name);
		if( collision.gameObject.name.Contains("Stadium_boundary"))
		{
			StopRunning();
		}
	}
	
	void OnCollisionEnter(Collision collision) {
		print("Entering Collision, Baseball Game Manager " + collision.gameObject.name);
		if( collision.gameObject.name.Contains("Stadium_boundary"))
		{
			StopRunning();
		}		
	}
	
	void OnTriggerEnter(Collider collision)
	{
		print("Entering Trigger, Baseball Game Manager " + collision.gameObject.name);
		if( collision.gameObject.name.Contains("Stadium_boundary"))
		{
			StopRunning();
		}	
	}
}