using UnityEngine;
using System.Collections;

public class OutfieldGameManager : MonoBehaviour {

	public GUISkin guiSkin;
	// Public attributes
	public AudioClip batSoundEffect;
	public AudioClip missSoundEffect;
	public AudioClip cheerSoundEffect;
	public AudioClip outSoundEffect;
	public int numStrikesRemaining = 4; 
	public float CatchDistance = .5f;
	public Texture ballTexture;
	public Transform targetPrefab;
	
	// Private attributes
	//private bool isPlaying = false;
	private bool bGameOver = false;
	private bool targetHit = false;
	private PhysicsHandler[] handlers;
	private GameInstructions gameInstructions;
	private ThirdPersonController runnerMan;
	float fTimeUntilReset = -1;
	public Transform TargetArea;
	private InkBoxOutfield ib;
	private bool overrunFlag = false;
	private float fOverrunTime = 0.0f;
	
	enum GameState { PRE_PLAY, PLAYING, STRIKE_MESSAGE, WAITING, WIN, LOSE }
	GameState gameState = GameState.PRE_PLAY;
	
	// Use this for initialization
	void Start () {
		
		handlers = FindObjectsOfType(typeof(PhysicsHandler)) as PhysicsHandler[];	
		runnerMan = FindObjectOfType(typeof(ThirdPersonController)) as ThirdPersonController;
		ib = GetComponent<InkBoxOutfield>();
		print("RN: " + runnerMan);		
		gameInstructions = GetComponent<GameInstructions>();	
		
		gameInstructions.instructions.Add( new InstructionItem(
			"You're our best outfielder!\nYour goal is to get the batter out,\ncatch that baseball!!\n\n Use your knowledge of physics\n and kinematic equations.", 
			new Rect(800,200,400,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"Variables:\nThese are the variables you can edit\n to change the speed of the outfielder,\n and they show their current values.", 
			new Rect(400,700,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"INK CANVAS:\nHover over each variable\nto see the ink canvas\n" +
			"and equations in play.", 
			new Rect(1100,700,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"INK CANVAS:\nUsing this canvas you can assign\n new values to the variables.\n\n" +
			"Try writing in 5 for velocity(v),\n the value of v changes \nwhen you leave the box.", 
			new Rect(700,400,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"CAMERAS:\nUse the Camera Menu\nto change Cameras views.\n\n" + 
			"Try Umpire view now,\n and then change back to overhead.\n", 
			new Rect(500,100,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"\nYou're up!\n\nYou have 3 chances.\n\nGOOD LUCK!!", 
			new Rect(875,100,200,150)));
				
		//isPlaying = false;
		gameState = GameState.PRE_PLAY;
		bGameOver = false;
		targetHit = false;
		numStrikesRemaining = 3;
	
		ib.rho = 65;
		
		int angleOffset = Random.Range(10,30);
		if( Random.value > 0.5 )
			ib.theta = angleOffset;
		else
			ib.theta = 90-angleOffset;
		
		ib.force = 15 + (int)(Random.value*10);
	}
	
	// Update is called once per frame
	void Update () { 		
		// Update the displayed distance from the runner to the target
		TargetArea.position = handlers[0].getFinalPosition();
		TargetArea.position += Vector3.up*0.1f;
		if( ib.distance == 0 )
			ib.distance = (TargetArea.position - runnerMan.transform.position).magnitude;
		
		
		
		// Check each state of the game
		if(gameState == GameState.PRE_PLAY) // !isPlaying
		{
			// Check for overrunning the ball
			if (ib.time > 0 && ib.time + 2 < handlers[0].fAirTime)
			{
				// Let the player overun the ball.	
				overrunFlag = true;
				fOverrunTime = handlers[0].fAirTime + 2;
				runnerMan.overrunning = true;
			}
			else
			{
				overrunFlag = false;
				runnerMan.overrunning = false;
			}
		}
		else if( gameState == GameState.PLAYING ) //isPlaying )
		{
			if (overrunFlag)
			{
				fOverrunTime -=Time.deltaTime;
				if (fOverrunTime <=0 )
				{
					gameState = GameState.STRIKE_MESSAGE; //isPlaying = false;
				}
			}
			else //if( !overrunFlag && ((transform.position - runnerMan.transform.position).magnitude < CatchDistance) )
			{
				// If the ball has reached the target stop the ball
				float targetMagnitude = (transform.position - TargetArea.position).magnitude;
				float runnerMagnitude = (transform.position - runnerMan.transform.position).magnitude;
				
				if (runnerMagnitude < 2.0) // Check if runner has caught the ball
				{
					// Check if the target is hit and the runner caught the ball - win the game
					targetHit = true;	
					
					foreach( PhysicsHandler handler in handlers )
					{
						handler.SetPlaying(false);	
					}
					fTimeUntilReset = 5f;
					bGameOver = true;
					gameState = GameState.WIN;			
				}
				else if (targetMagnitude < 2.0) // Check if ball has hit the target
				{
					foreach( PhysicsHandler handler in handlers )
					{
						handler.SetPlaying(false);
					}
					gameState = GameState.STRIKE_MESSAGE;
				}
				else // Otherwise, check if the simulation has stopped running 
				{						
					foreach( PhysicsHandler handler in handlers )
					{
						//isPlaying = isPlaying && (!handler.isSimComplete());
						if (gameState == GameState.PLAYING && !handler.isSimComplete())
							gameState = GameState.PLAYING;
						else
							gameState = GameState.STRIKE_MESSAGE;
					}
				}
			}
			
			if( gameState == GameState.STRIKE_MESSAGE ) //! isPlaying )
			{
				PhysicsHandler.HINT_LEVEL hl = PhysicsHandler.HINT_LEVEL.NO_HINT;
				if( numStrikesRemaining == 1 )
				{
					if (overrunFlag)
						gameInstructions.instructions.Add( new InstructionItem(
							"STRIKE TWO!!\n\n" +
							"You overran the ball.\nThis guide will help a little more.", 
							new Rect((Screen.width/2)-250,200,500,100)));
					else
						gameInstructions.instructions.Add( new InstructionItem(
							"STRIKE TWO!!\n\n" +
							"You didn't make it in time.\nThis guide will help a little more.", 
							new Rect((Screen.width/2)-250,200,500,100)));
					hl = PhysicsHandler.HINT_LEVEL.HINT_ALL;
				}
				else if( numStrikesRemaining == 2 )
				{
					hl = PhysicsHandler.HINT_LEVEL.HINT_SOME;
					if (overrunFlag)
						gameInstructions.instructions.Add( new InstructionItem(
							"STRIKE ONE!!\n\n" +
							"You overran the ball.\nTry again!\nThis guide will help a little more.", 
							new Rect((Screen.width/2)-250,250,500,100)));
					else
						gameInstructions.instructions.Add( new InstructionItem(
							"STRIKE ONE!!\n\n" +
							"You didn't make it in time.\nTry again!\nThis guide will help a little more.", 
							new Rect((Screen.width/2)-250,250,500,100)));
				}
				else if( numStrikesRemaining == 0 )
				{
					hl = PhysicsHandler.HINT_LEVEL.HINT_ALL;
					gameState = GameState.LOSE;
					bGameOver = true;
					
					audio.PlayOneShot(outSoundEffect);
				}
				foreach( PhysicsHandler handler in handlers )
				{
					handler.SetPlaying(false);						
					handler.hintLevel = hl;
				}
				runnerMan.StopRunning();
				fTimeUntilReset = 5f;
				gameState = GameState.WAITING;
			}
		} // End if Playing
		else if (gameState == GameState.WAITING || gameState == GameState.WIN || gameState == GameState.LOSE)
		{
			fTimeUntilReset-=Time.deltaTime;
			if( fTimeUntilReset <= 0 )
			{
				foreach( PhysicsHandler handler in handlers )
				{
					handler.Reset();	
					handler.SetPlaying(false);
				}	
				gameState = GameState.PRE_PLAY;
				runnerMan.Reset();
				overrunFlag = false;
				fOverrunTime = 0;
			}
		}		
	}
	
	void OnGUI()
	{
		GUI.skin = guiSkin;
		if( bGameOver )
		{
			gameInstructions.Skip();
			GUIStyle myStyle = "box";
			myStyle.fontSize = 16;
			
			if( targetHit )
			{
				GUI.Box(new Rect((Screen.width/2)-250,200,500,100), "GREAT Catch!!\n\nPlay Again?", myStyle);
			}
			else
			{
				GUI.Box(new Rect((Screen.width/2)-250,200,500,100), "YOU'RE OUT!!\n\nTry again and remember to use the equations.", myStyle);
			}
			GUILayout.BeginArea (new Rect((Screen.width/2)-250,300,500,75));
			GUILayout.BeginHorizontal(GUILayout.ExpandHeight(true));
			if( GUILayout.Button(  "Main Menu", GUILayout.ExpandHeight(true) ) )
			{
				Application.LoadLevel("MainMenuScene");					
			}
			if( GUILayout.Button(  "Play Again", GUILayout.ExpandHeight(true) ) )
			{
				Application.LoadLevel("OutfieldScene");					
			}
			GUILayout.EndHorizontal();
	        GUILayout.EndArea ();	
		}
		else
		{
			if( GUI.Button( new Rect(5,5,150,75), "Main Menu" ) )
			{
				Application.LoadLevel("MainMenuScene");					
			}
			GUI.enabled = (!(gameState == GameState.PLAYING) && numStrikesRemaining > 0);
			if( GUI.Button( new Rect(160,5,150,75), "Play Ball" ) )
			{
				// Start ball simulation
				numStrikesRemaining--;
				//isPlaying = !isPlaying;
				gameState = GameState.PLAYING;
				foreach( PhysicsHandler handler in handlers )
				{
					handler.Reset();
					//handler.SetPlaying(isPlaying);
					handler.SetPlaying(true);
				}
				
				audio.PlayOneShot(batSoundEffect);
				gameInstructions.Skip();
				
				// Start player running
				runnerMan.StartRunning();
			}
			GUI.enabled = true;
			/*else if(isPlaying && GUI.Button( new Rect(160,5,150,75), "Stop Play" ) )
			{				
				// Start player running
				runnerMan.StopRunning();
			}*/
			// Camera Combo box is at (315, 5, 150, 75)
			if( GUI.Button( new Rect(470,5,150,75), "Instant Replay" ) )
			{
				if (numStrikesRemaining < 3)
				{
					runnerMan.Reset();
					//isPlaying = !isPlaying;
					gameState = GameState.PLAYING;
					foreach( PhysicsHandler handler in handlers )
					{
						handler.Reset();
						//handler.SetPlaying(isPlaying);
						handler.SetPlaying(true);
					}
					audio.PlayOneShot(batSoundEffect);
					
					// Start player running	
					runnerMan.StartRunning();
				}
			}
			if( GUI.Button( new Rect(625,5,150,75), "Skip Instructions" ) )
			{
				gameInstructions.Skip();
			}
		}
		GUI.enabled = true;
		float ballIconStart = ((Screen.width) - (50*numStrikesRemaining))*0.5f;
		for( int i = 0; i < numStrikesRemaining; i++ )
		{
			GUI.DrawTexture(new Rect(ballIconStart + ( 50 * i),10,50,50), ballTexture, ScaleMode.ScaleToFit, true, 0.0f);
		}		
	}
	
    void OnCollisionEnter(Collision collision) {
		print("Entering Collision, Baseball Game Manager " + collision.gameObject.name);
		//if (!overrunFlag)
		//{
			if( collision.gameObject.name == "Target" )
			{				
				foreach( PhysicsHandler handler in handlers )
				{
					handler.SetPlaying(false);
				}
			}
//			else if( collision.gameObject.name == "stadium" )
//			{									
//				foreach( PhysicsHandler handler in handlers )
//				{
//					if (handler.isPlaying == true) audio.PlayOneShot(missSoundEffect);
//					handler.SetPlaying(false);
//				}
//			}
			else if( collision.gameObject.name == "3rd Person Controller")
			{
				audio.PlayOneShot(cheerSoundEffect);
				targetHit = true;
				runnerMan.StopRunning();
				/*GameObject.Instantiate( targetPrefab );
				Destroy(gameObject);*/
			}
		//}
	}
	
}
