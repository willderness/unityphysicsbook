using UnityEngine;
using System.Collections;

public class BaseballGameManager : MonoBehaviour {
	
	public GUISkin guiSkin;
	// Public attributes
	public AudioClip batSoundEffect;
	public AudioClip missSoundEffect;
	public AudioClip cheerSoundEffect;
	public AudioClip outSoundEffect;
	public int numStrikesRemaining = 4; 
	public Texture ballTexture;
	public Transform targetPrefab;
	
	// Private attributes
	private bool isDragging = false;
	private bool isPlaying = false;
	private bool bGameOver = false;
	private bool targetHit = false;
	private PhysicsHandler handler;
	private InkBox[] inkBoxes;
	private GameInstructions gameInstructions;
	float fTimeUntilReset = -1;

	private ZSStylusBaseballThrower zsStylusThrower;
	public GameObject zsStylusObject;
	// Use this for initialization
	void Start () {
		handler = GetComponent<PhysicsHandler>();
		inkBoxes = FindObjectsOfType(typeof(InkBox)) as InkBox[];  
		gameInstructions = GetComponent<GameInstructions>();
		
		gameInstructions.instructions.Add( new InstructionItem(
			"BATTER UP!\nYour goal is to make the baseball hit the target \n using your knowledge of physics\n and kinematic equations.", 
			new Rect(800,200,400,150)));		
		gameInstructions.instructions.Add( new InstructionItem(
			"Variables:\nThese are the variables you can edit\n to change the trajectory of the ball,\n and they show their current values.", 
			new Rect(400,700,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"INK CANVAS:\nHover over each variable\nto see the ink canvas\n" +
			"and equations in play.", 
			new Rect(1100,700,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"INK CANVAS:\nUsing this canvas you can assign\n new values to the variables.\n\n" +
			"Try writing in 45 for force(f),\n the value of force changes \nwhen you leave the box.", 
			new Rect(700,400,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"CAMERAS:\nUse the Camera Menu\nto change Cameras views.\n\n" + 
			"Try Umpire view now,\n and then change back to overhead.\n", 
			new Rect(500,100,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"\nYou're up Batter!\n\nYou have 3 Strikes.\n\nGOOD LUCK!!", 
			new Rect(875,100,200,150)));
		
		isDragging = false;
		isPlaying = false;
		bGameOver = false;
		targetHit = false;
		numStrikesRemaining = 3;
		
		if( zsStylusObject != null )
		{
			zsStylusThrower = zsStylusObject.GetComponentInChildren<ZSStylusBaseballThrower>();		
		}		
	}
	
	// Update is called once per frame
	void Update () { 
		if( isDragging )
		{
			
			Vector3 dragDelta = handler.InitialPosition - transform.position;
				
			Vector3 rotatedVector =  dragDelta + Quaternion.LookRotation(dragDelta) * (Vector3.forward*500);

			Debug.DrawLine(handler.InitialPosition, rotatedVector, Color.blue);
			
			foreach( InkBox inkBox in inkBoxes )
				inkBox.DoDragDelta( dragDelta );
			
		}
		if( targetHit )
		{
			//targetHit = false;
			isPlaying = false;
			handler.SetPlaying(false);	
			fTimeUntilReset = 5f;
			bGameOver = true;
		}
		if( isPlaying )
		{
				isPlaying = isPlaying && (!handler.isSimComplete());
			if( ! isPlaying )
			{
				PhysicsHandler.HINT_LEVEL hl = PhysicsHandler.HINT_LEVEL.NO_HINT;
				if( numStrikesRemaining == 1 )
				{
					gameInstructions.instructions.Add( new InstructionItem(
						"STRIKE TWO!!\n\n" +
						"This guide will help a little more.", 
						new Rect((Screen.width/2)-250,200,500,100)));
					hl = PhysicsHandler.HINT_LEVEL.HINT_ALL;
				}
				else if( numStrikesRemaining == 2 )
				{
					hl = PhysicsHandler.HINT_LEVEL.HINT_SOME;
					gameInstructions.instructions.Add( new InstructionItem(
						"STRIKE ONE!!\n\n" +
						"Try again. Use this guide to help you aim.", 
						new Rect((Screen.width/2)-250,200,500,100)));
				}
				else if( numStrikesRemaining == 0 )
				{
					hl = PhysicsHandler.HINT_LEVEL.HINT_ALL;
					bGameOver = true;
					
					audio.PlayOneShot(outSoundEffect);
				}
				handler.SetPlaying(false);						
				handler.hintLevel = hl;
				fTimeUntilReset = 5f;
				
				if( zsStylusThrower != null )
				{
					zsStylusThrower.SetDraggable(true);	
				}
			}
		}
		else
		{
			fTimeUntilReset-=Time.deltaTime;
			if( fTimeUntilReset <= 0 )
			{
				handler.Reset();	
				handler.SetPlaying(false);
			}
		}
		/*foreach( InkBox inkBox in inkBoxes )
			inkBox.visible = !isPlaying;*/
			
	}
	
	void OnGUI()
	{
		GUI.skin = guiSkin;
		if( bGameOver )
		{
			gameInstructions.Skip();
			GUIStyle myStyle = "box";
			myStyle.fontSize = 16;
			if( targetHit )
			{
				GUI.Box(new Rect((Screen.width/2)-250,200,500,100), "GREAT SHOT!!\n\nPlay Again?", myStyle);
			}
			else
			{
				GUI.Box(new Rect((Screen.width/2)-250,200,500,100), "YOU'RE OUT!!\n\nTry again and remember to use the equations.", myStyle);
			}
			GUILayout.BeginArea (new Rect((Screen.width/2)-250,300,500,75));
			GUILayout.BeginHorizontal(GUILayout.ExpandHeight(true));
			if( GUILayout.Button(  "Main Menu", GUILayout.ExpandHeight(true) ) )
			{
				Application.LoadLevel("MainMenuScene");					
			}
			if( GUILayout.Button(  "Play Again", GUILayout.ExpandHeight(true) ) )
			{
				Application.LoadLevel("BaseBallScene");					
			}
			GUILayout.EndHorizontal();
	        GUILayout.EndArea ();	
		}
		else
		{
			if( GUI.Button( new Rect(5,5,150,75), "Main Menu" ) )
			{
				Application.LoadLevel("MainMenuScene");					
			}
			GUI.enabled = (!isPlaying && numStrikesRemaining > 0);
			if( GUI.Button( new Rect(160,5,150,75), "Play Ball" ) )
			{
				// Start ball simulation
				numStrikesRemaining--;
				PlayBall();
			}
			// Camera Combo box is at (315, 5, 150, 75)
			if( GUI.Button( new Rect(470,5,150,75), "Instant Replay" ) )
			{
				if (numStrikesRemaining < 3)
				{
					PlayBall();
				}
			}
			if( GUI.Button( new Rect(625,5,150,75), "Skip Instructions" ) )
			{
				gameInstructions.Skip();
			}
		}
		GUI.enabled = true;
		float ballIconStart = ((Screen.width) - (50*numStrikesRemaining))*0.5f;
		for( int i = 0; i < numStrikesRemaining; i++ )
		{
			GUI.DrawTexture(new Rect(ballIconStart + ( 50 * i),10,50,50), ballTexture, ScaleMode.ScaleToFit, true, 0.0f);
		}		
	}
	void PlayBall()
	{
		isPlaying = !isPlaying;
		handler.Reset();
		handler.SetPlaying(isPlaying);
		
		audio.PlayOneShot(batSoundEffect);
		gameInstructions.Skip();
		if( zsStylusThrower != null )
		{
			zsStylusThrower.SetDraggable(false);	
		}
	}
    void OnCollisionEnter(Collision collision) {
		print("Entering Collision, Baseball Game Manager " + collision.gameObject.name);
		
		if( collision.gameObject.name == "Target" )
		{
			audio.PlayOneShot(cheerSoundEffect);
			targetHit = true;
			/*GameObject.Instantiate( targetPrefab );
			Destroy(gameObject);*/
			
			handler.SetPlaying(false);
		}
		else if( collision.gameObject.name == "stadium" )
		{									
			if (handler.isPlaying == true) 
				audio.PlayOneShot(missSoundEffect);
			handler.SetPlaying(false);
		}
	}
	
	void OnDragBegin()
	{
		isDragging = true;
	}
	void OnDragEnd()
	{
		isDragging = false;
		PlayBall();
	}
	
	
}
