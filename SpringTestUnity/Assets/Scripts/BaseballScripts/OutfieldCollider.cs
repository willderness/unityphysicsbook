using UnityEngine;
using System.Collections;

public class OutfieldCollider : MonoBehaviour {
	
	private ThirdPersonController runnerMan;
	
	// Use this for initialization
	void Start () {
		runnerMan = FindObjectOfType(typeof(ThirdPersonController)) as ThirdPersonController;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnControllerColliderHit(Collision collision)
	{
		print("Entering Controller Collider, Baseball Game Manager " + collision.gameObject.name);
		if( collision.gameObject.name.Contains("Stadium_boundary"))
		{
			runnerMan.StopRunning();
		}
	}
	
	void OnCollisionEnter(Collision collision) {
		print("Entering Collision, Baseball Game Manager " + collision.gameObject.name);
		if( collision.gameObject.name.Contains("Stadium_boundary"))
		{
			runnerMan.StopRunning();
		}		
	}
	
	/*void OnTriggerEnter(Collision collision)
	{
		print("Entering Trigger, Baseball Game Manager " + collision.gameObject.name);
		if( collision.gameObject.name.Contains("Stadium_boundary"))
		{
			runnerMan.StopRunning();
		}	
	}*/
}
