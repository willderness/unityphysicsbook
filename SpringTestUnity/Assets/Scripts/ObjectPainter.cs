using UnityEngine;
using System.Collections;

public class ObjectPainter : MonoBehaviour {
	Vector3 startingPoint;
	// Use this for initialization
	void Start () {
		startingPoint = transform.position;
	}
	
	void OnCollisionEnter(Collision collision) {
		collision.gameObject.renderer.material = gameObject.renderer.material;
		transform.position = startingPoint;
	}
	// Update is called once per frame
	void Update () {
	
	}
}
