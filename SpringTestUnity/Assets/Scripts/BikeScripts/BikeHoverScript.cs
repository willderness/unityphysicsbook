using UnityEngine;
using System.Collections;

public class BikeHoverScript : MonoBehaviour {

	public string levelToLoad;
	public AudioClip soundHover;
	public AudioClip beep;
	public bool quitButton = false;
	
	void Start()
	{
		ZSCore zscore;
		zscore = FindObjectOfType(typeof(ZSCore)) as ZSCore;
		if( zscore != null )
		{
			zscore.SetHeadTrackingScale(0.5f);
		}
	}
	
	void OnMouseEnter() {
		//audio.PlayOneShot(soundHover);
	}
	
	void OnMouseUp() {
		//audio.PlayOneShot(beep);
		//yield return new WaitForSeconds(0.35);
		if (quitButton) {
			Application.Quit();	
		}
		else{
			Application.LoadLevel(levelToLoad);
		}
	}
	
	void OnMouseDown() {
		Application.LoadLevel(levelToLoad);	
	}
}
