using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BikeScript_Level2 : MonoBehaviour {
	public GUISkin guiSkin;
	
	// Audio
	public AudioClip screamSoundEffect;
	public AudioClip windSoundEffect;
	public AudioClip cheerSoundEffect;
	
	private AudioSource cheerAudioSource;
	private AudioSource cheerAudioSourceNew;
	
	// Constants
	int MAX_HEIGHT = 70;
	float WIN_HEIGHT = 29f;
	bool DEBUG = false;
	
	// Spline attributes
	public GameObject splineRoot;
	Transform[] splineChildren;
	
	// Turning and Speeding Up Attributes
	bool moving = false;
	bool speedUp = false;
	float initalVelocity_z = 0.0f;
	float velocityMultiplier = 1/210;
	
	// Game play attributes
	public bool isPlaying = false;
	bool heightUpdated = false;
	private int numStrikesRemaining = 3;
	
	// Position attributes
	Vector3 originalPosition = Vector3.zero;
	Vector3 originalRotation = Vector3.zero;
	Quaternion originalQuaternion = Quaternion.identity;
	public Transform centerOfMass;
	
	// GUI / UI
	public GameObject text3D_bikerHeight;	
	private GameInstructions gameInstructions;
	private InkBoxBikerLevel1 inkBoxBiker;
	public Texture helmetTexture;
	
	float fTimeUntilReset = -1;
	float elapsedTime = 0;
	bool GameOver = false;
	bool WonGame = false;
	
	private float savedPosX;
	
	// Use this for initialization
	void Start () {
		splineChildren = GameObject.Find("SplineRoot1").GetComponentsInChildren<Transform>();
		splineChildren = splineRoot.GetComponentsInChildren<Transform>();
		System.Array.Sort(splineChildren, delegate(Transform first, Transform second) {
			return second.position.y.CompareTo(first.position.y);
		});
		
		SetupCenterOfMass();
		
		// Get references to other components
		inkBoxBiker = GetComponent<InkBoxBikerLevel1>();		
		gameInstructions = GetComponent<GameInstructions>();
		
		//heightUpdated = true;
		transform.rotation = Quaternion.AngleAxis(20, Vector3.right);
		
		// Add instructions
		gameInstructions.instructions.Add( new InstructionItem(
			"Welcome Evel Knievel!\nYour goal is to find out the minimum height\n" + 
			"the biker needs to start from to\n" +
			"make it over the jump after the loop the loop\n", 
			new Rect(800,200,400,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"Variables:\nYou can edit the height\n  of the biker and the other variables\n show their current values.", 
			new Rect(400,700,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"INK CANVAS:\nHover over the height variable\n to edit.\n" +
			"Hover over equations to see them.", 
			new Rect(1100,700,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"DRAG:\nDrag the biker to new heights\n using the stylus.", 
			new Rect(700,400,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"CAMERAS:\nUse the Camera Menu\nto change Cameras views.\n\n" + 
			"Try Loop view now,\n and then change back to Side.\n", 
			new Rect(Screen.width - 400,100,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"GOOD LUCK!!", 
			new Rect(875,100,200,150)));
		
		cheerAudioSource = gameObject.AddComponent("AudioSource") as AudioSource;
			
		cheerAudioSource.clip = cheerSoundEffect;
		cheerAudioSource.volume = 0.25f;
		cheerAudioSource.Play();
		
		cheerAudioSourceNew = gameObject.AddComponent("AudioSource") as AudioSource;
			
		cheerAudioSourceNew.clip = cheerSoundEffect;
		cheerAudioSourceNew.volume = 1.0f;
		cheerAudioSourceNew.panLevel = 0;
		
		savedPosX = this.transform.position.x;
	}
	
	void SetupCenterOfMass()
	{
		if(centerOfMass != null)
			rigidbody.centerOfMass = centerOfMass.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		
		
		if ( !isPlaying && Mathf.Abs(savedPosX - this.transform.position.x) > 0.1f)
		{
			EndDragging();
		}
		
		if (isPlaying)
		{
			// Check if simulation time ended
			fTimeUntilReset -= Time.deltaTime;
			elapsedTime += Time.deltaTime;
			if (fTimeUntilReset <= 0 || transform.position.z >=213)
			{
				if (originalPosition.y > WIN_HEIGHT && Mathf.Abs(originalPosition.y - WIN_HEIGHT) < 2.0f)
					WinGame();
				else
				{
					StopPlay();				
				}
			}
			
			if (/*initalVelocity_x == 0.0f && transform.position.x > 124 &&*/ transform.position.z < 124.5) {
				if (initalVelocity_z < rigidbody.velocity.z) initalVelocity_z = rigidbody.velocity.z;
				//print("initalVelocity_x = " + initalVelocity_x);
			}
			
			if (transform.position.z > 126 && transform.position.z < 140) {
				moving = true;
			}
			
			if (moving && transform.position.x > -3.77)
			{
				//rigidbody.velocity.z += 1.0f;
				float divisor = 140;
				if (originalPosition.y > 34) divisor = 90;
				else if (originalPosition.y > 50) divisor = 10;
				
				rigidbody.velocity = new Vector3(rigidbody.velocity.x - initalVelocity_z/divisor, // 500 was a problem for 27+, 400 problem for 40+
												 rigidbody.velocity.y, 
												 rigidbody.velocity.z);
			}
			else
			{
				rigidbody.velocity = new Vector3(0, rigidbody.velocity.y, rigidbody.velocity.z);
				moving = false;
			}
			
			if (!speedUp && transform.position.z > 132 && transform.position.x < -3.5)
			{
				rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y, initalVelocity_z);
				speedUp = true;
			}
			
			// Reset displayed height
			inkBoxBiker.height = transform.position.y;
			//inkBoxBiker.OnGuiVariables();
		}
		else  // If NOT Playing
		{
			rigidbody.velocity = Vector3.zero;
			
			// Check if height has changed
			float inkBoxHeight = inkBoxBiker.height;
			
			if (Mathf.Abs(transform.position.y - inkBoxHeight) > 0.6)
			{
				print("transform.position.y = " + transform.position.y + ", inkBoxHeight = " + inkBoxHeight);
				transform.position = new Vector3(transform.position.x, inkBoxHeight, transform.position.z);
				heightUpdated = true;
				
				if (transform.position.y > 65)
				{
					// Display warning message
					gameInstructions.instructions.Add( new InstructionItem(
							"The maximum height the biker\n can sit on the ramp is 65m\n", 
							new Rect((Screen.width/2)-250,200,500,100)));
					
					// Reset height to the max of 65
					inkBoxBiker.height = 65;
					transform.position = new Vector3(transform.position.x, 65.0f, transform.position.z);
				}
			}
		}
		
		if (heightUpdated)
		{
			// Updates the 3D text with the height
//	       	TextMesh textMesh = text3D_bikerHeight.GetComponent<TextMesh>();
//			float height_rounded = Mathf.Round(inkBoxBiker.height * 10f) / 10f; 
//			textMesh.text = "Biker Height = " + height_rounded.ToString();
//	       	text3D_bikerHeight.transform.position = new Vector3(text3D_bikerHeight.transform.position.x, 
//																inkBoxBiker.height + 10, 
//																text3D_bikerHeight.transform.position.z);
			
			// Since the ramp is a curve, this calculates the position on the ramp where the biker will be given the height
			MapBikerToCurve();
			
			// Update height values
			inkBoxBiker.height = transform.position.y;
			inkBoxBiker.UpdateGUI();
			
			heightUpdated = false;
			//transform.position = new Vector3(transform.position.x, bikerHeight, transform.position.z);
		}
	}
	
	
	private void MapBikerToCurve()
	{
		// ***
		// Since the ramp is a curve, this calculates the position on the ramp where the biker will be given the height
		// ***
		print("splineChildren.Length = " + splineChildren.Length);
		
		for (int i = 0; i <= MAX_HEIGHT; i++)
		{
			Vector3 sphere_curr = GameObject.Find("Sphere"+i.ToString()).transform.position;
			
			if (Mathf.FloorToInt(transform.position.y) == Mathf.FloorToInt(sphere_curr.y))
			{
				// Find out if the position is between i and i+1, or i and i-1
				if (i == 0)
				{
					// Check if on or below minimum height
					
					// Otherwise adjust above min height
					
					// Rotate
					transform.Rotate(new Vector3(0, transform.rotation.y, transform.rotation.z));
				}
				else if (i == MAX_HEIGHT)
				{
					// Check if on or above max height
					
					// Otherwise adjust below max height
				}
				else 
				{
					Vector3 sphere_next = GameObject.Find("Sphere"+(i+1).ToString()).transform.position;
					Vector3 sphere_prev = GameObject.Find("Sphere"+(i-1).ToString()).transform.position;
					
					
					
					if (transform.position.y > sphere_curr.y)
					{
						// Need to increase in y, and decrease in z
						float changeInY = (transform.position.y - sphere_curr.y)/(sphere_next.y - sphere_curr.y);
						float changeInZ = changeInY*(sphere_curr.z - sphere_next.z);
						transform.position = new Vector3(transform.position.x, transform.position.y, sphere_curr.z - changeInZ);
						
						float slope = Mathf.Min(70, 25 * 1/(sphere_curr.z - sphere_next.z));
						transform.rotation = Quaternion.AngleAxis(slope, Vector3.right);
					}
					else
					{
						// Need to decrease in y, and increase in z
						float changeInY = (sphere_curr.y - transform.position.y)/(sphere_curr.y - sphere_prev.y);
						float changeInZ = changeInY*(sphere_prev.z - sphere_curr.z);
						transform.position = new Vector3(transform.position.x, transform.position.y, sphere_curr.z + changeInZ);
						
						float slope = Mathf.Min(70, 25 * 1/(sphere_prev.z - sphere_curr.z));
						transform.rotation = Quaternion.AngleAxis(slope, Vector3.right);
					}
					
				}
				
				break;
			}
		}
	}
	
	private void MapBikerToCurve_Dropping()
	{
		// ***
		// Since the ramp is a curve, this calculates the position on the ramp where the biker will be given the height
		// ***		
		for (int i = 0; i <= MAX_HEIGHT; i++)
		{
			Vector3 sphere_curr = GameObject.Find("Sphere"+i.ToString()).transform.position;
			
			if (Mathf.FloorToInt(transform.position.z) == Mathf.FloorToInt(sphere_curr.z))
			{
				// Find out if the position is between i and i+1, or i and i-1
				if (i == 0)
				{
					// Check if on or below minimum height
					
					// Otherwise adjust above min height
					
					// Rotate
					transform.Rotate(new Vector3(0, transform.rotation.y, transform.rotation.z));
				}
				else if (i == MAX_HEIGHT)
				{
					// Check if on or above max height
					
					// Otherwise adjust below max height
				}
				else 
				{
					Vector3 sphere_next = GameObject.Find("Sphere"+(i+1).ToString()).transform.position;
					Vector3 sphere_prev = GameObject.Find("Sphere"+(i-1).ToString()).transform.position;
					
					
					
					if (transform.position.z > sphere_curr.z) // Use previous sphere
					{
						// Increasing in z, and decreasing in y
						float changeInZ = (transform.position.z - sphere_curr.z)/(sphere_prev.z - sphere_curr.z);
						float changeInY = changeInZ*(sphere_curr.y - sphere_prev.y);
						transform.position = new Vector3(transform.position.x, sphere_curr.y - changeInY, transform.position.z);
						
						float slope = Mathf.Min(70, 25 * 1/(sphere_prev.z - sphere_curr.z));
						transform.rotation = Quaternion.AngleAxis(slope, Vector3.right);
					}
					else
					{
						// this node is taller curr, so need to decrease in z, and increase in y
						float changeInZ = (sphere_curr.z - transform.position.z)/(sphere_curr.z - sphere_next.z);
						float changeInY = changeInZ*(sphere_next.y - sphere_curr.y);
						transform.position = new Vector3(transform.position.x, sphere_curr.y + changeInY, transform.position.z);
						
						float slope = Mathf.Min(70, 25 * 1/(sphere_prev.z - sphere_curr.z));
						transform.rotation = Quaternion.AngleAxis(slope, Vector3.right);
					}
					
				}
				
				break;
			}
		}
	}
	
	public void EndDragging()
	{
		bool adjustedBiker = false;
		
		// Restore x location - depth, and check height bounds
		if (this.transform.position.y > 65)
		{
			this.transform.position = new Vector3(savedPosX, 65, GameObject.Find("Sphere65").transform.position.z);
			adjustedBiker = true; // no more adjustment needed
		}
		else if (this.transform.position.y < 5)
		{
			this.transform.position = new Vector3(savedPosX, 5, GameObject.Find("Sphere5").transform.position.z);
			adjustedBiker = true; // no more adjustment needed
		}
		else
		{
			this.transform.position = new Vector3(savedPosX, this.transform.position.y, this.transform.position.z);
		}				
		
		// ***
		// Since the ramp is a curve, this calculates the position on the ramp where the biker will be given the height
		// ***
		print("splineChildren.Length = " + splineChildren.Length);
		float range = 2.0f;
		if (!adjustedBiker)
		{
//			for (int i = 0; i <= MAX_HEIGHT; i++)
//			{
//				if (i > 48) range = 0.65f;
//				else if (i > 25) range = 1.2f;
//				GameObject sphereObject = GameObject.Find("Sphere"+i.ToString());
//				
//
//				if(Mathf.Abs(this.transform.position.z - sphereObject.transform.position.z) < range)
//				{
//					// Set position and slope ( rotation )
//					transform.position = new Vector3(savedPosX, sphereObject.transform.position.y, sphereObject.transform.position.z);
//					if (i == 0) 
//						transform.Rotate(new Vector3(0, transform.rotation.y, transform.rotation.z));
//					else
//					{
//						float slope = Mathf.Min(70, 25 * 1/(GameObject.Find("Sphere"+(i-1).ToString()).transform.position.z - sphereObject.transform.position.z));
//	//						transform.Rotate(Vector3.zero);
//	//						transform.Rotate(new Vector3(slope,
//	//													 transform.rotation.y,
//	//													 transform.rotation.z));
//						transform.rotation = Quaternion.AngleAxis(slope, Vector3.right);
//					}
//					adjustedBiker = true;
//					break;
//				}
//			}
			
			MapBikerToCurve_Dropping();	
			adjustedBiker = true;
		}
		
		// Update height values
		inkBoxBiker.height = this.transform.position.y;
		inkBoxBiker.UpdateGUI();
		
		//heightUpdated = true;	
		
		// Updates the 3D text with the height
//       	TextMesh textMesh = text3D_bikerHeight.GetComponent<TextMesh>();
//		float height_rounded = Mathf.Round(inkBoxBiker.height * 10f) / 10f; 
//		textMesh.text = "Biker Height = " + height_rounded.ToString();
//       	text3D_bikerHeight.transform.position = new Vector3(text3D_bikerHeight.transform.position.x, 
//															transform.position.y + 10, 
//															text3D_bikerHeight.transform.position.z);
		
		if (adjustedBiker)
			heightUpdated = false;
		else
			heightUpdated = true;
	}
	
	void OnGUI () {
		GUI.skin = guiSkin;
		
		if (GameOver)
		{
			gameInstructions.Skip();
			GUIStyle myStyle = "box";
			myStyle.fontSize = 16;
			
			GUI.Box(new Rect((Screen.width/2)-250,200,500,100), "Game Over...\n\nTry again and remember to use the equations!", myStyle);
			
			GUILayout.BeginArea (new Rect((Screen.width/2)-250,300,500,75));
			GUILayout.BeginHorizontal(GUILayout.ExpandHeight(true));
			if( GUILayout.Button(  "Main Menu", GUILayout.ExpandHeight(true) ) )
			{
				Application.LoadLevel("MainMenuScene");					
			}
			if( GUILayout.Button(  "Play Again", GUILayout.ExpandHeight(true) ) )
			{
				Application.LoadLevel("LoopJumpLevel2");					
			}
			GUILayout.EndHorizontal();
	        GUILayout.EndArea ();	
		}
		else if (WonGame)
		{
			gameInstructions.Skip();
			GUIStyle myStyle = "box";
			myStyle.fontSize = 16;
			
			GUI.Box(new Rect((Screen.width/2)-250,200,500,100), "YOU WON!!\n\n", myStyle);
			
			GUILayout.BeginArea (new Rect((Screen.width/2)-250,300,500,75));
			GUILayout.BeginHorizontal(GUILayout.ExpandHeight(true));
			if( GUILayout.Button(  "Main Menu", GUILayout.ExpandHeight(true) ) )
			{
				Application.LoadLevel("MainMenuScene");					
			}

			GUILayout.EndHorizontal();
	        GUILayout.EndArea ();
			
			cheerAudioSource.Play();
		}
		
		
		if( GUI.Button( new Rect(5,5,150,75), "Main Menu" ) )
		{
			Application.LoadLevel(0);
		}
		
		// Update the inkbox attributes
		inkBoxBiker.mass = rigidbody.mass;
		inkBoxBiker.velocity = rigidbody.velocity;
		inkBoxBiker.kineticEnergy = 0.5f*rigidbody.mass*rigidbody.velocity.z*rigidbody.velocity.z;
		inkBoxBiker.potentialEnergy = -1*rigidbody.mass*inkBoxBiker.gravity.y*inkBoxBiker.height;
		
		// Debug
        if (DEBUG) DoDebug();
						
		// Setup Top Button Bar
		if ( GUI.Button( new Rect(5,5,150,75), "Main Menu") )
		{
			Application.LoadLevel(0);
		}				
		if( GUI.Button( new Rect(160,5,150,75), isPlaying?"Stop":"Go" ) )
		{
			isPlaying = !isPlaying;
			if (isPlaying)
			{
				audio.PlayOneShot(screamSoundEffect);
				rigidbody.useGravity = true;
				rigidbody.constraints = RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
				originalPosition = transform.position;
				originalRotation = transform.localEulerAngles;
				originalQuaternion = transform.localRotation;
				gameInstructions.Skip();
				fTimeUntilReset = transform.position.y <=5 ? 8f : 15f;
				elapsedTime = 0;
				
				audio.PlayOneShot(screamSoundEffect);
			}
			else
			{
				rigidbody.useGravity = false;
				rigidbody.velocity = Vector3.zero;
				transform.position = originalPosition;
				transform.rotation = originalQuaternion;
				rigidbody.freezeRotation = true;
				moving = false;
				
				speedUp = false;
			}
		
		}
		if( GUI.Button( new Rect(315,5,150,75), "Skip Instructions" ) )
		{
			gameInstructions.Skip();
		}
		
		
		
		// Display helmets for strikes
		float helmetIconStart = ((Screen.width) - (50*numStrikesRemaining))*0.5f;
		for( int i = 0; i < numStrikesRemaining; i++ )
		{
			GUI.DrawTexture(new Rect(helmetIconStart + ( 50 * i),10,50,50), helmetTexture, ScaleMode.ScaleToFit, true, 0.0f);
		}	
    }
	
	void StopPlay()
	{
		isPlaying = false;
		rigidbody.useGravity = false;
		rigidbody.velocity = Vector3.zero;
		transform.position = originalPosition;
		//transform.Rotate(Vector3.zero);
		//rigidbody.transform.Rotate(originalRotation);
		transform.rotation = originalQuaternion;
		
		rigidbody.freezeRotation = true;
		moving = false;
		
		numStrikesRemaining--;
		
		if (numStrikesRemaining == 2)
		{
			// Create hint 
			if (originalPosition.y-WIN_HEIGHT > 2.0f) 
				gameInstructions.instructions.Add( new InstructionItem(
						"Too High!\n\n" +
						"Try again. \nRemember you need to go > 23m/s at the bottom of the ramp.\nKE = 1/2mv", 
						new Rect((Screen.width/2)-250,200,500,125)));
			else
				gameInstructions.instructions.Add( new InstructionItem(
						"Too Low!\n\n" +
						"Try again. \nRemember you need to go > 23m/s at the bottom of the ramp.\nKE = 1/2mv", 
						new Rect((Screen.width/2)-250,200,500,125)));
		}
		else if (numStrikesRemaining == 1)
		{
			// Create hint 
			if (originalPosition.y-WIN_HEIGHT > 2.0f) 
				gameInstructions.instructions.Add( new InstructionItem(
						"Too High!\n\n" +
						"Try again. \nRemember given no friction, \n PE at the start is completely converted to \nKE after the loop.", 
						new Rect((Screen.width/2)-250,200,500,150)));
			else
				gameInstructions.instructions.Add( new InstructionItem(
						"Too Low!\n\n" +
						"Try again. \nRemember given no friction, \n PE at the start is completely converted to \nKE after the loop.", 
						new Rect((Screen.width/2)-250,200,500,150)));
		}
		else if (numStrikesRemaining <= 0)
		{
			GameOver = true;
		}
		
		speedUp = false;
	}
	
	void WinGame()
	{
//		isPlaying = false;
//		rigidbody.useGravity = false;
//		rigidbody.velocity = Vector3.zero;
//		transform.position = originalPosition;
//		//transform.Rotate(originalRotation);
//		//rigidbody.MoveRotation(originalQuaternion);
//		rigidbody.transform.Rotate(originalRotation);
//		//rigidbody.MovePosition(originalPosition);
//		rigidbody.freezeRotation = true;
//		moving = false;
		
		if (!WonGame) cheerAudioSourceNew.PlayOneShot(cheerSoundEffect);
		
		WonGame = true;
	}
	
	void DoDebug()
	{
		GUI.TextArea(new Rect(10, 90, 150, 30), "(x, y, z) = (" + Mathf.FloorToInt(transform.position.x) + ", " + 
												 Mathf.FloorToInt(transform.position.y) + ", " + 
												 Mathf.FloorToInt(transform.position.z) + ")");
				
		//print ("rigidbody.velocity = " + rigidbody.velocity);
		GUI.TextArea(new Rect(10, 130, 150, 30), "velocity = (" + Mathf.FloorToInt(rigidbody.velocity.x) + ", " + 
												 				 Mathf.FloorToInt(rigidbody.velocity.y) + ", " + 
												 				 Mathf.FloorToInt(rigidbody.velocity.z) + ")");
		
		GUI.TextArea(new Rect(10, 170, 150, 30), "AV = (" + Mathf.FloorToInt(rigidbody.angularVelocity.x) + ", " + 
												 				 Mathf.FloorToInt(rigidbody.angularVelocity.y) + ", " + 
												 				 Mathf.FloorToInt(rigidbody.angularVelocity.z) + ")");
		
		
		GUI.TextArea(new Rect(10, 210, 150, 30), "Elapsed time = " + elapsedTime);		
	}
}
