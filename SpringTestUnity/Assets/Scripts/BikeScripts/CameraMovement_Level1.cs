using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CameraMovement_Level1 : MonoBehaviour {
	
	public sealed class CameraMode {

    private readonly string name;
    public readonly int value;
	private static readonly Dictionary<string, CameraMode> instance = new Dictionary<string,CameraMode>();

    public static readonly CameraMode LOOP = new CameraMode (0, "Loop");
    public static readonly CameraMode FOLLOW = new CameraMode (1, "Follow");
    public static readonly CameraMode OVERHEAD = new CameraMode (2, "Overhead");        

    private CameraMode(int value, string name){
        this.name = name;
        this.value = value;
		instance[name] = this;
    }

    public override string ToString(){
        return name;
    }
	public static explicit operator CameraMode(string str)
	{
	    CameraMode result;
	    if (instance.TryGetValue(str, out result))
	        return result;
	    else
	        throw new InvalidCastException();
	}
}
	GUIContent[] comboBoxList;
	private ComboBox comboBoxControl;// = new ComboBox();
	private GUIStyle listStyle = new GUIStyle();
	public Transform Overhead;
	public Transform Loop;
	
	//private BikeScript_Level1 bikeScript;
	private ZSCore zscore;
	
	private void Start()
	{
		comboBoxList = new GUIContent[3];
		comboBoxList[CameraMode.LOOP.value] = new GUIContent(CameraMode.LOOP.ToString());
		comboBoxList[CameraMode.FOLLOW.value] = new GUIContent(CameraMode.FOLLOW.ToString());
		comboBoxList[CameraMode.OVERHEAD.value] = new GUIContent(CameraMode.OVERHEAD.ToString());
		
		listStyle.normal.textColor = Color.white; 
		listStyle.onHover.background =
		listStyle.hover.background = new Texture2D(2, 2);
		listStyle.padding.left =
		listStyle.padding.right =
		listStyle.padding.top =
		listStyle.padding.bottom = 4;
 
		comboBoxControl = new ComboBox(new Rect(Screen.width-105, 10, 100, 20), comboBoxList[0], comboBoxList, "button", "box", listStyle);
		cameraMode = CameraMode.LOOP;
		
		// Set original biker scale
		GameObject physicsObject = GameObject.FindGameObjectWithTag("Player");
		originalScale = physicsObject.transform.localScale;
		
		//bikeScript = gameObject.GetComponent<BikeScript_Level1>();
		zscore = FindObjectOfType(typeof(ZSCore)) as ZSCore;
		//zscore.SetStereoEnabled(false);
		//zscore.SetHeadTrackingEnabled(false);			
	}
	public CameraMode cameraMode;
	void OnGUI() {

		/*if (GUI.Button(new Rect(60, 10, 150, 50), trackBallText))
		{
        	trackBall = !trackBall;
			trackBallText = "Stop Tracking Ball";	
			
			if( trackBall == false )
			{
				Camera.main.transform.position = 	mainCamInitPos;
		 		Camera.main.transform.rotation = mainCamInitRot;
				trackBallText = "Track Ball";
			}
		}*/
		comboBoxControl.Show();
	}
	Vector3 originalScale;
	// Update is called once per frame
	void Update () {
		CameraMode oldCameraMode = cameraMode;				
		
		bool isPlaying = false;
		if (Application.loadedLevel == 4)
			isPlaying = gameObject.GetComponent<BikeScript_Level1>().isPlaying;					
		else if (Application.loadedLevel == 5)
		 	isPlaying = gameObject.GetComponent<BikeScript_Level2>().isPlaying;
		
		if (!isPlaying)//!bikeScript.isPlaying)
		{
			cameraMode = (CameraMode)comboBoxList[2].text;
			ZSCore_SetFar();
		}
		else
		{
			if (transform.position.z > 105 && transform.position.z < 135)
			{
				cameraMode = (CameraMode)comboBoxList[0].text;
				ZSCore_SetClose();
			}
			else 
			{
				cameraMode = (CameraMode)comboBoxList[1].text;
				ZSCore_SetClose();
			}
		}
		//print ( cameraMode.ToString());
		//if( oldCameraMode != cameraMode )
		//{
			GameObject physicsObject = GameObject.FindGameObjectWithTag("Player");
			if( cameraMode == CameraMode.OVERHEAD )
			{
			if( Camera.main != null )
			{
				Camera.main.transform.position = Overhead.position;//	OverheadPos;
		 		Camera.main.transform.rotation = Overhead.rotation;//OverheadRot;
							
				physicsObject.transform.localScale = originalScale * 3;
			}
			}
			else if( cameraMode == CameraMode.LOOP ) 
			{	
				Camera.main.transform.position = Loop.position;//	LoopPos;
		 		Camera.main.transform.rotation = Loop.rotation; //Rot;
				physicsObject.transform.localScale = originalScale;
			}
			if( cameraMode == CameraMode.FOLLOW )
			{
				physicsObject.transform.localScale = originalScale;
			}
		//}
		
		if( cameraMode == CameraMode.FOLLOW )
		{
			//GameObject physicsObject = GameObject.FindGameObjectWithTag("Player");
			Camera.main.transform.position = 	physicsObject.transform.position+ new Vector3(10,10,-5);//0.5f, 0.5f, 0.5f) ;
			Camera.main.transform.LookAt( physicsObject.transform.position );	
		}
	}
	public float WorldScaleFar = 250;
	public float WorldScaleClose = 50;
	
	private void ZSCore_SetClose()
	{
		if (zscore != null)
		{
			zscore.SetHeadTrackingEnabled(true);
			zscore.SetHeadTrackingScale(1f);
			zscore.SetInterPupillaryDistance(0.01f);
			zscore.SetStereoLevel(1.0f);
			zscore.SetWorldScale(WorldScaleClose);
			zscore.SetFieldOfViewScale(1.0f);
			zscore.SetZeroParallaxOffset(0.0f);
			zscore.SetNearClip(0.01f);
			zscore.SetFarClip(100000.0f);
		}
	}
	
	private void ZSCore_SetFar()
	{
		if (zscore != null)
		{
			zscore.SetHeadTrackingEnabled(true);
			zscore.SetHeadTrackingScale(1f);
			zscore.SetInterPupillaryDistance(0.02f);
			zscore.SetStereoLevel(1.0f);
			zscore.SetWorldScale(WorldScaleFar);
			zscore.SetFieldOfViewScale(1.0f);
			zscore.SetZeroParallaxOffset(0.0f);
			zscore.SetNearClip(0.01f);
			zscore.SetFarClip(100000.0f);
		}
	}
}
