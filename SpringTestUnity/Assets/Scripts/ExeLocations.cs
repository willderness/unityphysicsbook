using UnityEngine;

public class ExeLocations
{
	public static System.Diagnostics.Process stylusEmulator = null;
	string path = Application.dataPath +  "/../../";
	public static string StylusEmulator = Application.dataPath +  "/../../" + "StylusMouseEmulation/StylusMouseEmulation.exe";
    public static string RecognizerExe = Application.dataPath +  "/../../" + "RemoteRecognizer/RemoteRecognizer/bin/Debug/RemoteRecognizer.exe";
//Switch to these for release build.
//	public static string StylusEmulator = Application.dataPath /* + "/../../" */ + "/StylusMouseEmulation.exe";
//    public static string RecognizerExe = Application.dataPath /* + "/../../" */ + "/RemoteRecognizer.exe";
}