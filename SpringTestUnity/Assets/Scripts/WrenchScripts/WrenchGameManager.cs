using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WrenchGameManager : MonoBehaviour {
	public GUISkin guiSkin;
	Transform wrenchModel;
	Transform wrenchTurnAxis;
	Transform nutModel;
	
	//nut movement objects
	GameObject nPA;
	GameObject nPB;
	Transform startM;
	Transform endM;
	
	//force rotation arrows
	Transform clockArrow;
	Transform countArrow;
	Transform torqueClockArrow;
	Transform torqueCountArrow;
	
	//rotation testing
	private Quaternion originalRot;
	private Quaternion offsetRot;
	
	//force Arrow locations
	Vector3 currentClockArrowPos;
	Vector3 currentCountArrowPos;
	Vector3 currClockArrowScale;
	Vector3 currCountArrowScale;
	
	float turnSpeed = 1.0f;
	//var rotateObject = true;
	
	bool experimentRun = false;
	bool rotateWrenchbool = false;	
	//Time before breaking.
	float nutBroke = 0;
	
	InkBoxWrench inkboxWrench;
	GameInstructions gameInstructions;
	StrikeCounter strikes;
	private ZSCore zscore;
	float MAX_TORQUE = 70;  
	// Torque needs to be between 40 and 50 inclusive to win?  
	float MIN_TORQUE = 40;
	int MAX_RADIUS = 10;
	
	private Vector3 WrenchStartingPos;
	private Quaternion WrenchStartingRot;
	float fTimeToComplete;
	bool bSimFinished;
	bool bGameOver;
	
	SplineInterpolator interpolator;
	public Transform finalCameraPos;
	void Start () {
	
		Transform wrenchMainModel = transform.Find("Model");
		wrenchModel = wrenchMainModel.FindChild("wrench");
		wrenchTurnAxis = wrenchModel.transform;//wrenchModel.transform.FindChild("mesh29");
		clockArrow = wrenchModel.FindChild("ClockwiseForceArrow").FindChild("group_0");
		countArrow = wrenchModel.FindChild("ForceArrowShifted").FindChild("group_0");
		torqueClockArrow = wrenchMainModel.FindChild("TorqueClockwise").FindChild("group_0");
		torqueCountArrow = wrenchMainModel.FindChild("TorqueCounterClockwise").FindChild("group_0");
		
		// Force arrow initial pos and scale
		currentClockArrowPos = clockArrow.transform.position;
		currentCountArrowPos = countArrow.transform.parent.transform.position;
		currClockArrowScale = clockArrow.transform.localScale;
		currCountArrowScale = countArrow.transform.parent.transform.localScale;
		
		torqueClockArrow.renderer.enabled = false;
		torqueCountArrow.renderer.enabled = false;
		clockArrow.renderer.enabled = false;
		countArrow.renderer.enabled = true;
		//wrenchModel.collider.enabled = false;
		
		/*wrenchRot = new GameObject();
		wrenchRot.transform.position = (wrenchTurnAxis.renderer.bounds.center)+(new Vector3(0f,0f,0.0019f));
		
		//Debug.Log(wrenchTurnAxis);
		
		wrenchRot.transform.parent = wrenchModel.parent;
		wrenchModel.parent = wrenchRot.transform;*/
		
		/*nPA = transform.FindChild("nutPointA");
		nPB = transform.FindChild("nutPointB");*/
		startM = transform.FindChild("nutPointA");
		endM = transform.FindChild("nutPointB");
		
		// Get game components
		inkboxWrench = GetComponent<InkBoxWrench>();
		gameInstructions = GetComponent<GameInstructions>();
		strikes = GetComponent<StrikeCounter>();
		interpolator = FindObjectOfType(typeof(SplineInterpolator)) as SplineInterpolator;
		CreateGameInstructions();
		
		zscore = FindObjectOfType(typeof(ZSCore)) as ZSCore;
		ZSCore_SetClose();
	
		torqueCountArrow.renderer.enabled = false;
		torqueClockArrow.renderer.enabled = false;
		nutBroke = 0;
		
		WrenchStartingPos = wrenchModel.transform.position;
		WrenchStartingRot = wrenchModel.transform.rotation;
		
		fTimeToComplete = 0xFFFFFFFF;
		bSimFinished = false;
		bGameOver = false;
	}
	float fCurrentRotationPercent = 0;
	float deceleration = 1.01f;
	void Update () {	
		//print("Time: " + fTimeToComplete);
		if( nutBroke > 0 )
		{
			nutBroke -= Time.deltaTime;
			if( nutBroke <= 0 )
			{
				wrenchModel.rigidbody.constraints = RigidbodyConstraints.None;
				wrenchModel.rigidbody.useGravity = true;
				torqueCountArrow.renderer.enabled = false;
				torqueClockArrow.renderer.enabled = false;
				countArrow.renderer.enabled = false;
				fTimeToComplete = 2f;
				
				gameInstructions.instructions.Add( new InstructionItem(
				"You had too much torque\n and broke the nut!\n  Remember, radius and force\n are components of Torque.\n", 
				new Rect(800,200,400,150)));
			
				strikes.Swing();
			}
			float rad2deg = (float)((180.0f / Mathf.PI));
			
			// Divide torque by 2 to make more difficult to play
			float deltaTurn = inkboxWrench.torque/2 * Time.deltaTime/30;
			wrenchModel.transform.Rotate(0,0,deltaTurn*rad2deg);
				
		}
		else if (rotateWrenchbool == true)
		{
			// Divide torque by 2 to make more difficult to play
			float rad2deg = (float)((180.0f / Mathf.PI));
			float deltaTurn = (inkboxWrench.torque/2)/deceleration * Time.deltaTime/10;
			deceleration += 0.01f;

			// Rotate wrench around bolt
			wrenchModel.transform.Rotate(0,0,deltaTurn*rad2deg);

			// Bolt moves in with rotation
			wrenchModel.transform.position = Vector3.Lerp(startM.position, endM.position,(fCurrentRotationPercent));

			
			print("deltaTurn = " + deltaTurn);
			// If it slows down to a certain point, stop the wrench and say try again with more torque
			if (Mathf.Abs( deltaTurn ) < .01 && fTimeToComplete < 2)
			{
				gameInstructions.instructions.Add( new InstructionItem(
				"You didn't have enough torque to finish,\n try adding more.\n  Remember, radius and force\n are components of Torque.\n", 
				new Rect(800,200,400,150)));
				strikes.Swing();
				
				fTimeToComplete = 5f;	
				rotateWrenchbool = false;
				bSimFinished = true;
				isRunning = false;
				//ResetTurning();
			}
			
			// Total rotation - determines if it's gone around twice = 2 x PI = 12.56..
			fCurrentRotationPercent += deltaTurn/-12.56637f;
			
			// Check if it's made 2 complete turns?
			if( fCurrentRotationPercent > 1 )
			{
				fTimeToComplete = 5f;	
				rotateWrenchbool = false;
				
				// The player should win?
			}
			
			// Check to show inward torque or outward torque?
			if( inkboxWrench.torque/2 > 0 )
			{		
				torqueCountArrow.renderer.enabled = true;
				torqueClockArrow.renderer.enabled = false;
			}
			else
			{	
				torqueCountArrow.renderer.enabled = false;
				torqueClockArrow.renderer.enabled = true;
			}
		}
		
		// Only update time and game state if the simulation is running
		//if (!bSimFinished)
		//{
			if( fTimeToComplete > 0 && fCurrentRotationPercent < 1)
			{
				fTimeToComplete -= Time.deltaTime;	
			}
			else
			{
				bSimFinished = true;
			}
			
			if( bSimFinished && experimentRun )
			{
				print("fCurrentRotationPercent = " + fCurrentRotationPercent);
				if( fCurrentRotationPercent >= 1 || strikes.IsOut() )
				{
					bGameOver = true;	
				}
				else
				{
					isRunning = false;
					ResetTurning();
				}
			}
		//}
		
		// Do this regardless of the state of the game
		translateForceArrow();
		
	}
 
	bool isRunning = false;
	
	void OnGUI(){
		GUI.skin = guiSkin;
		
		// Tool bar menu
		if( GUI.Button( new Rect(5,5,150,75), "Main Menu" ) )
		{
			Application.LoadLevel("MainMenuScene");					
		}
		if( GUI.Button( new Rect(160,5,150,75), !isRunning ? "Start" : "Stop" ) )
		{
			isRunning = !isRunning;
			
			if (isRunning)
			{
				if( strikes.numStrikesRemaining > 0 )
				{
					StartTurning();
				}
			}
			else
			{
				StopTurning();
				fTimeToComplete = 3;
			}
		}
		// Camera Combo box is at (315, 5, 150, 75)
		if( GUI.Button( new Rect(315,5,150,75), "Instant Replay" ) )
		{
			if( ! strikes.IsOut() )
			{
				StartTurning();	
			}
		}
		if( GUI.Button( new Rect(470,5,150,75), "Skip Instructions" ) )
		{
			gameInstructions.Skip();
			interpolator.Reset();
			Camera.main.transform.position = finalCameraPos.position;
			Camera.main.transform.rotation = finalCameraPos.rotation;
			
		}
		if( bGameOver )
		{
			gameInstructions.Skip();
			GUIStyle myStyle = "box";
			myStyle.fontSize = 16;
			if( fCurrentRotationPercent >= 1 )
			{
				GUI.Box(new Rect((Screen.width/2)-250,200,500,100), "Great job, you fixed the Bike!!\n\nPlay Again?", myStyle);
			}
			else
			{
				GUI.Box(new Rect((Screen.width/2)-250,200,500,100), "Oh no, You're out of chances!!\n\nTry again and remember to use the equations.", myStyle);
			}
			GUILayout.BeginArea (new Rect((Screen.width/2)-250,300,500,75));
			GUILayout.BeginHorizontal(GUILayout.ExpandHeight(true));
			if( GUILayout.Button(  "Main Menu", GUILayout.ExpandHeight(true) ) )
			{
				Application.LoadLevel("MainMenuScene");					
			}
			if( GUILayout.Button(  "Play Again", GUILayout.ExpandHeight(true) ) )
			{
				Application.LoadLevel("WrenchScene");					
			}
			GUILayout.EndHorizontal();
	        GUILayout.EndArea ();	
		}
		
	}
	
	void StartTurning()
	{
		fTimeToComplete = 100000;
		gameInstructions.Skip();
		// Divide torque by 2 to make more difficult to play - as far as spinning the wrench goes, but still check that
		// displayed torque is not greater than MAX_TORQUE
		if(Mathf.Abs( inkboxWrench.torque ) > MAX_TORQUE )
		{
			float minTime = 1f/Mathf.Abs( inkboxWrench.torque/2 );
			experimentRun = true;
			nutBroke = 0.05f;//Random.Range(minTime,.35f);		
			bSimFinished = false;
			experimentRun = true;
			
//			gameInstructions.instructions.Add( new InstructionItem(
//				"You had too much torque\n and broke the nut!\n  Remember, radius and force\n are components of Torque.\n", 
//				new Rect(800,200,400,150)));
//			
//			strikes.Swing();
		}
		else
		{		
			bSimFinished = false;
			experimentRun = true;
			rotateWrenchbool = true;
			nutBroke = 0;
			fTimeToComplete = 15;
		}		
	}
	
	void StopTurning()
	{
		nutBroke = 0f;
		isRunning = false;		
		rotateWrenchbool = false;	
		wrenchModel.rigidbody.useGravity = false;
		wrenchModel.rigidbody.constraints = RigidbodyConstraints.FreezeAll;
		wrenchModel.rigidbody.velocity = Vector3.zero;
		wrenchModel.rigidbody.angularVelocity = Vector3.zero;
	}
	void ResetTurning()
	{
		StopTurning();	
		experimentRun = false;	
		fCurrentRotationPercent = 0;
		deceleration = 1.01f;
		wrenchModel.transform.position = WrenchStartingPos;
		wrenchModel.transform.rotation = WrenchStartingRot;
		torqueCountArrow.renderer.enabled = false;
		torqueClockArrow.renderer.enabled = false;
		countArrow.renderer.enabled = true;
		fTimeToComplete = 100000;
	}
	
	void translateForceArrow(){
		float force = inkboxWrench.forcePerp;
		float angle = inkboxWrench.theta;
		float radius = inkboxWrench.radius;
		
		// Check for max radius
		if (radius > MAX_RADIUS)
		{
			gameInstructions.instructions.Add(new InstructionItem(
			"The length of the wrench is " + MAX_RADIUS + "\n", 
			new Rect(800,200,400,150)));
			
			radius = inkboxWrench.radius = MAX_RADIUS; 
			inkboxWrench.torque = Mathf.Sin(Mathf.Deg2Rad*inkboxWrench.theta)*radius*inkboxWrench.forcePerp;
		}
		
		// Adjust CC arrow
		float xOffset = radius;
		float yOffset = 0.9595f;
		float zRotate = 270f;
		if( angle <= 180 )
		{
			xOffset += 0.9f;	
			zRotate = angle+180;//270;
			yOffset *= -1;//0.9595f;
		}
		else
		{
			xOffset += 0.0169f;	
			zRotate = (angle-180);//90;
			yOffset *= 1;//0.9595f;			
		}
		
		// Divide xOffset and radius by 2 to allow for max 10cm wrench.
		countArrow.transform.parent.transform.localPosition = new Vector3(xOffset/2 + ((radius)/2), 
																		yOffset, 
																	 	countArrow.transform.parent.transform.localPosition.z);
		
		// Adjust scale in X according to force, also move in x to compensate
		countArrow.transform.parent.transform.localScale = new Vector3(currCountArrowScale.x * (force/3), currCountArrowScale.y, currCountArrowScale.z); 
		countArrow.transform.parent.localEulerAngles = new Vector3(0,180,zRotate);
	}
	
	
	void CreateGameInstructions()
	{
		gameInstructions.instructions.Add( new InstructionItem(
			"Welcome to Exploring Torque!\nYou had quite the ride!\n\n... BUT now your wheel is loose.\n\n", 
			new Rect(800,200,400,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"Your goals is to use the correct amount\n of torque to tighten the bolt.\nThe bolt needs to rotate 2 rotations to be tight.\n\nToo much force will break the bolt.\n Not enough torque and it won't be tight enough.\n", 
			new Rect(800,400,400,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"Variables:\nThese are the variables you can edit\n to change the torque on the wrench,\n and they show their current values.", 
			new Rect(400,700,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"INK CANVAS:\nHover over each variable\nto see the ink canvas\n" +
			"and equations in play.", 
			new Rect(1100,700,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"INK CANVAS:\nUsing this canvas you can assign\n new values to the variables.\n\n" +
			"Try writing in 5 for force(f),\n the value of force changes \nwhen you leave the box.", 
			new Rect(700,400,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"GOOD LUCK!!", 
			new Rect(875,100,200,150)));
	}
	
	private void ZSCore_SetClose()
	{
		if (zscore != null)
		{
			zscore.SetHeadTrackingEnabled(true);
			//print("Head tracking scale = " + zscore.GetHeadTrackingScale());
			zscore.SetHeadTrackingScale(0.5f);
			zscore.SetInterPupillaryDistance(0.01f);
			zscore.SetStereoLevel(1.0f);
			zscore.SetWorldScale(4.0f);//8
			zscore.SetFieldOfViewScale(1.0f);
			zscore.SetZeroParallaxOffset(0.0f);
			zscore.SetNearClip(0.01f);
			zscore.SetFarClip(10000.0f);
		}
	}
}
