using UnityEngine;
using System.Collections;

public class TorqueGameManager : MonoBehaviour {
	
	public GUISkin guiSkin;
	// Public attributes
	public AudioClip turningSoundEffect;
	public int numStrikesRemaining = 4; 
	public Texture ballTexture;	
	// Private attributes
	private bool isPlaying = false;
	private bool bGameOver = false;
	private bool targetHit = false;
	private PhysicsHandlerTorque[] handlers;
	private InkBox[] inkBoxes;
	private GameInstructions gameInstructions;
	float fTimeUntilReset = -1;
	
	// Use this for initialization
	void Start () {
		handlers = FindObjectsOfType(typeof(PhysicsHandlerTorque)) as PhysicsHandlerTorque[];		
		inkBoxes = FindObjectsOfType(typeof(InkBox)) as InkBox[];  
		gameInstructions = GetComponent<GameInstructions>();
		
		gameInstructions.instructions.Add( new InstructionItem(
			"BATTER UP!\nYour goal is to make the baseball hit the target \n using your knowledge of physics\n and kinematic equations.", 
			new Rect(800,200,400,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"Variables:\nThese are the variables you can edit,\nand they show their current values.", 
			new Rect(400,700,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"INK CANVAS:\nHover over each variable\nto see the ink canvas\n" +
			"and equations in play.", 
			new Rect(1100,700,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"INK CANVAS:\nUsing this canvas you can assign\n new values to the variables.\n\n" +
			"Try writing in 5 for force(f),\n the value of force changes \nwhen you leave the box.", 
			new Rect(700,400,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"CAMERAS:\nUse the Camera Menu\nto change Cameras views.\n\n" + 
			"Try Umpire view now,\n and then change back to overhead.\n", 
			new Rect(500,30,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"\nYou're up Batter!\n\nYou have 3 Strikes.\n\nGOOD LUCK!!", 
			new Rect(875,100,200,150)));
		
		isPlaying = false;
		bGameOver = false;
		targetHit = false;
		numStrikesRemaining = 3;
	}
	
	// Update is called once per frame
	void Update () { 
		if( targetHit )
		{
			//targetHit = false;
			isPlaying = false;
			foreach( PhysicsHandlerTorque handler in handlers )
			{
				handler.SetPlaying(false);	
			}
			fTimeUntilReset = 5f;
			bGameOver = true;
		}
		if( isPlaying )
		{
			foreach( PhysicsHandlerTorque handler in handlers )
			{
				isPlaying = isPlaying && (!handler.isSimComplete());
			}
			if( ! isPlaying )
			{
				PhysicsHandlerTorque.HINT_LEVEL hl = PhysicsHandlerTorque.HINT_LEVEL.NO_HINT;
				if( numStrikesRemaining == 1 )
				{
					gameInstructions.instructions.Add( new InstructionItem(
						"STRIKE TWO!!\n\n" +
						"This guide will help a little more.", 
						new Rect((Screen.width/2)-250,200,500,100)));
					hl = PhysicsHandlerTorque.HINT_LEVEL.HINT_ALL;
				}
				else if( numStrikesRemaining == 2 )
				{
					hl = PhysicsHandlerTorque.HINT_LEVEL.HINT_SOME;
					gameInstructions.instructions.Add( new InstructionItem(
						"STRIKE ONE!!\n\n" +
						"Try again. Use this guide to help you aim.", 
						new Rect((Screen.width/2)-250,200,500,100)));
				}
				else if( numStrikesRemaining == 0 )
				{
					hl = PhysicsHandlerTorque.HINT_LEVEL.HINT_ALL;
					bGameOver = true;					
				}
				foreach( PhysicsHandlerTorque handler in handlers )
				{
					handler.SetPlaying(false);						
					handler.hintLevel = hl;
				}
				fTimeUntilReset = 5f;
			}
		}
		else
		{
			fTimeUntilReset-=Time.deltaTime;
			if( fTimeUntilReset <= 0 )
			{
				foreach( PhysicsHandlerTorque handler in handlers )
				{
					handler.Reset();	
					handler.SetPlaying(false);
				}	
			}
		}
		/*foreach( InkBox inkBox in inkBoxes )
			inkBox.visible = !isPlaying;*/
			
	}
	
	void OnGUI()
	{
		GUI.skin = guiSkin;
		if( bGameOver )
		{
			gameInstructions.Skip();
			GUIStyle myStyle = "box";
			myStyle.fontSize = 16;
			GUILayout.BeginArea (new Rect((Screen.width/2)-250,200,500,100));
			if( targetHit )
			{
				GUILayout.TextArea("GREAT SHOT!!\n\nPlay Again?", myStyle);
			}
			else
			{
				GUILayout.TextArea("YOU'RE OUT!!\n\nTry again and remember to use the equations.", myStyle);
			}
			GUILayout.BeginHorizontal();
			if( GUILayout.Button(  "Main Menu" ) )
			{
				Application.LoadLevel("MenuScene");					
			}
			if( GUILayout.Button(  "Play Again" ) )
			{
				Application.LoadLevel("BaseBallScene");					
			}
			GUILayout.EndHorizontal();
	        GUILayout.EndArea ();	
		}
		else
		{
			if( GUI.Button( new Rect(5,5,100,30), "Main Menu" ) )
			{
				Application.LoadLevel("MenuScene");					
			}
			GUI.enabled = (!isPlaying && numStrikesRemaining > 0);
			if( GUI.Button( new Rect(110,5,100,30), "Play Ball" ) )
			{
				numStrikesRemaining--;
				isPlaying = !isPlaying;
				foreach( PhysicsHandlerTorque handler in handlers )
				{
					handler.Reset();
					handler.SetPlaying(isPlaying);
				}
				
				//audio.PlayOneShot(batSoundEffect);
				gameInstructions.Skip();
			}
		}
		GUI.enabled = true;
		float ballIconStart = ((Screen.width) - (50*numStrikesRemaining))*0.5f;
		for( int i = 0; i < numStrikesRemaining; i++ )
		{
			GUI.DrawTexture(new Rect(ballIconStart + ( 50 * i),10,50,50), ballTexture, ScaleMode.ScaleToFit, true, 0.0f);
		}		
	}
	
    void OnCollisionEnter(Collision collision) {
		print("Entering Collision, Torque Game Manager " + collision.gameObject.name);
		
	}
	
}
