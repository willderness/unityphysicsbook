using UnityEngine;
using System.Collections;

public class SimButtons : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	bool isSimRunning = false; 
	
	public GUISkin guiSkin;
	void OnGUI()
	{
		GUI.skin =  guiSkin;
		if( GUI.Button( new Rect(5,5,100,30), isSimRunning?"Stop":"Play Ball" ) )
		{
			isSimRunning = !isSimRunning;
			PhysicsHandler[] handlers = FindObjectsOfType(typeof(PhysicsHandler)) as PhysicsHandler[];
			foreach( PhysicsHandler handler in handlers )
			{
				handler.Reset();
			}
		}
	}
}
