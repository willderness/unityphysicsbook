using UnityEngine;
using System;
using Mono.CSharp;


public class Grapher2 : MonoBehaviour {
	
	public int zresolution = 100;
	public int xresolution = 100;
	public int gridResolution = 11;
	private int currentResolution;
	private ParticleSystem.Particle[] points;
	private string stringToEdit;
	InkBoxEquation inkbox;
	public GameObject prefabGridX;
	public GameObject prefabGridY;
	public GameObject prefabGridZ;
	public float fWorldScale = 2f;
	GameObject[] gridX,gridY,gridZ;
	private Rect xLabelRect, yLabelRect, zLabelRect;
	float halfWorldScale;
	private ZSCore zscore;
	private Vector3 originalCamPos;
	private Quaternion originalCamRot;
	private Transform centerCube;
	private float fPercentToDraw;
	void Start () 
	{			
		centerCube = GameObject.Find("CenterCube").transform;
		fPercentToDraw = 0;
		
		originalCamPos = centerCube.position;
		originalCamRot = centerCube.rotation;
		zscore = FindObjectOfType(typeof(ZSCore)) as ZSCore;
		if (zscore != null)
		{
			zscore.SetHeadTrackingEnabled(true);
			//print("Head tracking scale = " + zscore.GetHeadTrackingScale());
			zscore.SetHeadTrackingScale(1f);
			zscore.SetInterPupillaryDistance(0.01f);
			zscore.SetStereoLevel(1.0f);
			zscore.SetWorldScale(10.0f);//8
			zscore.SetFieldOfViewScale(1.0f);
			zscore.SetZeroParallaxOffset(0.0f);
			zscore.SetNearClip(0.01f);
			zscore.SetFarClip(10000.0f);
		}
		
		
		halfWorldScale = fWorldScale/2;
		CreatePoints();
		//stringToEdit = "sin(x)";
//		e = new Expression(stringToEdit);
		//Eval(EqnParser.ParseEquation(stringToEdit));
		inkbox = GetComponent<InkBoxEquation>();		
	
		gridX = new GameObject[gridResolution];
		gridY = new GameObject[gridResolution];
		gridZ = new GameObject[gridResolution];
		for( int i = 0; i< gridResolution; i++ )
		{			
			gridX[i] = GameObject.Instantiate( prefabGridX ) as GameObject;
			gridX[i].renderer.enabled = bGridEnabled;
			gridY[i] = GameObject.Instantiate( prefabGridY ) as GameObject;
			gridY[i].renderer.enabled = bGridEnabled;
			gridZ[i] = GameObject.Instantiate( prefabGridZ ) as GameObject;
			gridZ[i].renderer.enabled = bGridEnabled;
		}
		
		xLabelRect = new Rect(10, Screen.height - 200, 200, 50);
		yLabelRect = new Rect(10, Screen.height - 140, 200, 50);
		zLabelRect = new Rect(10, Screen.height - 80, 200, 50);
	}
	
	private void CreatePoints () {
		if(xresolution < 2){
			xresolution = 2;
		}
		if(zresolution < 2){
			zresolution = 2;
		}
		/*else if(resolution > 100){
			resolution = 100;
		}*/
		currentResolution = zresolution * xresolution;
		points = new ParticleSystem.Particle[currentResolution];
		SetPointInitialLocations();
		AnimateDrawing();
	}
	private void AnimateDrawing()
	{
		fPercentToDraw = 0;
	}
	
	private void SetPointInitialLocations()
	{
		float incrementX = fWorldScale / (xresolution - 1);
		float incrementZ = fWorldScale / (zresolution - 1);
		int i = 0;
		for(int x = 0; x < xresolution; x++){
			for(int z = 0; z < zresolution; z++){
				Vector3 p = new Vector3(x * incrementX - halfWorldScale, 0f, z * incrementZ - halfWorldScale);
				points[i].position = p;
				points[i].color = new Color(p.x, 0f, p.z);
				points[i++].size = 0.1f;
			}
		}
	}

	void Update () {
		if (inkbox.visible)
		{
			zscore.SetHeadTrackingEnabled(false);
		}
		else
		{
			zscore.SetHeadTrackingEnabled(true);
			
			if(currentResolution != xresolution*zresolution){
				CreatePoints();
			}
			if( fPercentToDraw < 100 )
			{
				fPercentToDraw += Time.deltaTime*(100/5); //Will make animation 5 seconds.
				
				//FunctionDelegate f = functionDelegates[(int)function];
				float t = Time.timeSinceLevelLoad;
				/*float incrementX = fSliderScaleX / (resolution - 1);
				float incrementZ = fSliderScaleZ / (resolution - 1);
				*/	
				for(int i = 0; i < points.Length; i++){
					Vector3 p = points[i].position;
					float x = p.x * fSliderScaleX;
					float z = p.z * fSliderScaleZ;
				  	/*e.Parameters["x"] = x;
			  		e.Parameters["z"] = z;
					object output =  e.Evaluate();*/
					/*if( e.HasErrors() )
					{
						print(e.Error);
					}*/
					
					//print ("Time: " + Time.timeSinceLevelLoad + " " + output);
					//float.TryParse(output.ToString(), out p.y);
					if( inkbox != null && inkbox.generatorFunc != null)
					{
						p.y = inkbox.generatorFunc(x,z) / fSliderScaleY;
					}
					
		//			p.y = f(p, t);
					points[i].position = p;
					Color c = points[i].color;
					if( i < points.Length*(fPercentToDraw/100))
					{
						c.g = p.y;
						c.a = 1;
					}
					else
					{
						c.a = 0;	
					}
					points[i].color = c;
					
				}
				
				particleSystem.SetParticles(points, points.Length);
			}
			if( bGridEnabled )
			{
				for( int i = 0; i < gridResolution; i++)
				{
					float delta = (float)(i-(0.5f*gridResolution))/10;
					gridX[i].transform.position = new Vector3((((fWorldScale * delta) - (halfWorldScale)) / fSliderScaleX ), 0, 0 );
					gridY[i].transform.position = new Vector3(0, (((fWorldScale * delta) - (halfWorldScale)) / fSliderScaleY ), 0);//( 0, (delta * fSliderScaleY)-0.5f, 0);
					gridZ[i].transform.position = new Vector3(0, 0, (((fWorldScale * delta) - (halfWorldScale)) /  fSliderScaleZ));
				}
			}
			if( inkbox.HasNewEquation() )
			{
				SetPointInitialLocations();
				AnimateDrawing();
			}
		}
	}
	
	public GUISkin guiSkin;
	float fSliderScaleX = 1f;
	float fSliderScaleY = 1f;
	float fSliderScaleZ = 1f;
	public bool bGridEnabled = false;
	public bool xGridEnabled = false;
	public bool yGridEnabled = false;
	public bool zGridEnabled = false;
	void OnGUI()
	{
		GUI.skin = guiSkin;
		if( GUI.Button( new Rect(5,5,150,75), "Main Menu" ) )
		{
			Application.LoadLevel("MainMenuScene");					
		}
		if( GUI.Button( new Rect(160,5,150,75), "Reset Camera" ) )
		{
			centerCube.position = originalCamPos;
			centerCube.rotation = originalCamRot;
		}
		if( GUI.Button( new Rect(315,5,150,75), "Sin(x)" ) )
		{
			string saveResults = inkbox.results;
			inkbox.results = "sin[x]";
			inkbox.OnEnterButtonClick();
			inkbox.results = saveResults;
		}
		if( GUI.Button( new Rect(470,5,150,75), "Sin(x)\u00B2" ) )
		{
			string saveResults = inkbox.results;
			inkbox.results = "sin[x]^2";
			inkbox.OnEnterButtonClick();
			inkbox.results = saveResults;
		}
		if( GUI.Button( new Rect(625,5,150,75), "Sin(x)\u00B2 + \n\nCos(z)\u00B2" ) )
		{
			string saveResults = inkbox.results;
			inkbox.results = "sin[x]^2 + cos[z]^2";
			inkbox.OnEnterButtonClick();
			inkbox.results = saveResults;
		}
		if( GUI.Button( new Rect(780,5,150,75), "x\u00B2" ) )
		{
			string saveResults = inkbox.results;
			inkbox.results = "x^2";
			inkbox.OnEnterButtonClick();
			inkbox.results = saveResults;
		}
		if( GUI.Button( new Rect(935,5,150,75), "x\u00B2 + z\u00B2" ) )
		{
			string saveResults = inkbox.results;
			inkbox.results = "x^2 + z^2";
			inkbox.OnEnterButtonClick();
			inkbox.results = saveResults;
		}
		if( GUI.Button( new Rect(1090,5,150,75), "x + z" ) )
		{
			string saveResults = inkbox.results;
			inkbox.results = "x + z";
			inkbox.OnEnterButtonClick();
			inkbox.results = saveResults;		
		}
		if( GUI.Button( new Rect(1245,5,150,75), "log x" ) )
		{
			string saveResults = inkbox.results;
			inkbox.results = "log[x]";
			inkbox.OnEnterButtonClick();
			inkbox.results = saveResults;		
		}
		if( GUI.Button( new Rect(1400,5,150,75), "e\u207f" ) )
		{
			string saveResults = inkbox.results;
			inkbox.results = "LetterSym(\"e\")^x";
			inkbox.OnEnterButtonClick();
			inkbox.results = saveResults;		
		}
		
		
	/*	string oldString = stringToEdit;
		stringToEdit = GUI.TextField (new Rect (10, 10, 500, 20), stringToEdit, 70);	
		if( stringToEdit != oldString )
			Eval(EqnParser.ParseEquation( stringToEdit) );
	*/			
		
		if( GUI.Button( new Rect(35, Screen.height - 280, 200, 50), "Toggle All Grids" ) )
		{
			bGridEnabled = !bGridEnabled;
			xGridEnabled = yGridEnabled = zGridEnabled = bGridEnabled;
			
			for( int i = 0; i< gridResolution; i++ )
			{
				gridX[i].renderer.enabled = bGridEnabled;
				gridY[i].renderer.enabled = bGridEnabled;
				gridZ[i].renderer.enabled = bGridEnabled;
			}		
		}
		GUI.Label( xLabelRect, "X: " );		
		GUI.Label( yLabelRect, "Y: " );
		GUI.Label( zLabelRect, "Z: " );
		float fOldSliderScaleX = fSliderScaleX;
		float fOldSliderScaleY = fSliderScaleY;
		float fOldSliderScaleZ = fSliderScaleZ;
		
		fSliderScaleX = GUI.HorizontalSlider (new Rect (35, Screen.height - 200, 200, 50), fSliderScaleX, .1f, 10f);
		/*if( fSliderScaleX > 6 )
			xresolution = 15*(int)((10-6)+fSliderScaleX);
		else
			xresolution = 150;*/
		if( GUI.Button( new Rect(240, Screen.height - 225, 150, 50), xGridEnabled ? "X Grid On" : "X Grid Off") )
		{
			xGridEnabled = !xGridEnabled;
			
			for( int i = 0; i< gridResolution; i++ )
			{
				gridX[i].renderer.enabled = xGridEnabled;
			}		
		}
		
		fSliderScaleY = GUI.HorizontalSlider (new Rect (35, Screen.height - 140, 200, 50), fSliderScaleY, .1f, 10f);
		if( GUI.Button( new Rect(240, Screen.height - 165, 150, 50), yGridEnabled ? "Y Grid On" : "Y Grid Off" ) )
		{
			yGridEnabled = !yGridEnabled;
			
			for( int i = 0; i< gridResolution; i++ )
			{
				gridY[i].renderer.enabled = yGridEnabled;
			}		
		}
		
		fSliderScaleZ = GUI.HorizontalSlider (new Rect (35, Screen.height - 80, 200, 50), fSliderScaleZ, .1f, 10f);	
		/*if( fSliderScaleZ > 6 )
			zresolution = 15*(int)((10-6)+fSliderScaleZ);
		else
			zresolution = 150;*/
		if( GUI.Button( new Rect(240, Screen.height - 105, 150, 50), zGridEnabled ? "Z Grid On" : "Z Grid Off" ) )
		{
			zGridEnabled = !zGridEnabled;
			
			for( int i = 0; i< gridResolution; i++ )
			{
				gridZ[i].renderer.enabled = zGridEnabled;
			}		
		}
		
		if( fOldSliderScaleX != fSliderScaleX || 
			fOldSliderScaleY != fSliderScaleY ||
			fOldSliderScaleZ != fSliderScaleZ )
		{
			fPercentToDraw = 99.9f;
		}
	}
	public void Eval(string sCSCode) {
		Evaluator.Init(new string [0]);
		"using System;".Run();
		"using UnityEngine;".Run();

		var s = @"new Func<System.Single,System.Single,System.Single> ( (x,z) => ((System.Single)" + sCSCode + ") );";
		print ( s);
 		var func = (Func<float, float,float>)Evaluator.Evaluate(s);
		//var func = new Func<System.Single,System.Single,System.Single> ( (s,t) => ((System.Single)System.Math.Sin(s)) );
		
		//var func = new Func<float,float> ( s => (s*0.5f) );
		//var e = (float)Evaluator.Evaluate("40+ .5f;");
		//var e = (Vector3)Evaluator.Evaluate(@"new Vector3(3,3,3);");
		//print (e);
		inkbox.generatorFunc = func;
		
		
	}
	
	
}
