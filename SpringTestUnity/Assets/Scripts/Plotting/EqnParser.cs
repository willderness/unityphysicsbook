using UnityEngine;
using System.Collections;
using NCalc;

public class EqnParser : MonoBehaviour {
	
	string expression = "";
	static string[] possibleExprs = {"System.Math.Sin", "System.Math.Cos", "System.Math.Tan", "System.Math.Pow", "System.Math.Exp"};
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	string outputExp = "";
	void OnGUI() {
		string text = GUI.TextArea(new Rect(10, 10, 200, 50), expression);		
				
		if (text != expression)
		{
			expression = text;			
		}
		
		if (GUI.Button(new Rect(220, 10, 200, 50), "Enter"))
		{
			expression = expression.Replace(" ", string.Empty);
			if (expression != string.Empty)
				outputExp = ParseEquation(expression);
		}
		
		GUI.TextArea(new Rect(10, 60, 200, 50), "Output equation:  " + outputExp);
	}
	
	
	// log[x]
	//LetterSym["e"]^x
	public static string ParseEquation(string inputExp)
	{
		// Example equation x sin[x]^2
		// Assume only x first
		inputExp = inputExp.Replace(" ", string.Empty);
		inputExp += ' ';
		inputExp = inputExp.Replace("[", "(");
		inputExp = inputExp.Replace("]", ")");
		inputExp = inputExp.Replace("sin(", "System.Math.Sin(");
		inputExp = inputExp.Replace("cos(", "System.Math.Cos(");
		inputExp = inputExp.Replace("tan(", "System.Math.Tan(");		
		inputExp = inputExp.Replace("log(", "System.Math.Log(");	
		
		// Check for x* and 
		// Check for ? * x
		for(int i = 0; i < inputExp.Length-1; i++)
		{
			if((inputExp[i].CompareTo('x') == 0 || inputExp[i].CompareTo('z') == 0) &&
				inputExp[i+1].CompareTo('*') != 0 && inputExp[i+1].CompareTo('+') != 0 && 
			   inputExp[i+1].CompareTo('-') != 0 && inputExp[i+1].CompareTo('/') != 0 &&
				inputExp[i+1].CompareTo(')') != 0 && inputExp[i+1].CompareTo('^') != 0 &&
				inputExp[i+1].CompareTo(' ') != 0)
			{
				// Is i+1 an operator +, - , / ?  Otherwise it should be a *
				inputExp = inputExp.Insert(i+1, "*");
			}
			
			// Check for ? * x
			if(i > 0 && 
				(inputExp[i].CompareTo('x') == 0 || inputExp[i].CompareTo('z') == 0) &&
				inputExp[i-1].CompareTo('*') != 0 && inputExp[i-1].CompareTo('+') != 0 && 
			   inputExp[i-1].CompareTo('-') != 0 && inputExp[i-1].CompareTo('/') != 0 &&
				inputExp[i-1].CompareTo(')') != 0 && inputExp[i-1].CompareTo('^') != 0 &&
				inputExp[i-1].CompareTo('(') != 0 )
			{
				// Is i+1 an operator +, - , / ?  Otherwise it should be a *
				inputExp = inputExp.Insert(i, "*");
			}
		}
		
		// Check for LetterSym("e")^x , length of LetterSym("e")^ is 15
		int idx = 0;
		while (idx < inputExp.Length - 16)
		{
			string searchStr = inputExp.Substring(idx, 15);
			if (searchStr == "LetterSym(\"e\")^" )
			{
				int j = idx+15;
				string exponent = "";
				while (! isOperator(inputExp[j])) j++;
				
				exponent = inputExp.Substring(idx+15,j-(idx+15));
				
				inputExp = inputExp.Replace("LetterSym(\"e\")^"+exponent, "System.Math.Exp("+exponent+")");
			}
			
			idx++;
		}
		
		// Example x*sin(x)^2 --> x*Mathf.Pow(Mathf.Sin(x),2)
		// Example x*sin(x^2) --> x*Mathf.Sin(Mathf.Pow(x,2))
		// Check for ^
		idx = inputExp.Length-1;
		
		while(idx > 0)
		{
			if(inputExp[idx].CompareTo('^') == 0)
			{
				// Find exponent, Will is really cool
				
				char exponent = inputExp[idx+1]; // This assumes 0-9 exponent, will need to find entire number
				
				inputExp = inputExp.Insert(idx+1, ",");
				inputExp = inputExp.Insert(idx+3, ")");
				inputExp = inputExp.Remove(idx, 1);
				
				char text = inputExp[idx-1];
				
				// Find start of expression
				if (inputExp[idx-1].CompareTo('x') == 0 || inputExp[idx-1].CompareTo('z') == 0)
				{
					inputExp = inputExp.Insert(idx-1, "System.Math.Pow(");
				}
				else if (inputExp[idx-1].CompareTo(')') == 0)
				{
					int idx2 = idx-2; // needs to be character before the identified ')'
					int numClosing = 1;
					
					// Find the beginning of the expression
					while (numClosing > 0)
					{
						if (inputExp[idx2].CompareTo('(') == 0)
						{
							numClosing--;	
							
							// What function is in front here?  
							
							for (int j = 0; j < possibleExprs.Length; j++)
							{
								string currExpr = inputExp.Substring(idx2 - possibleExprs[j].Length, possibleExprs[j].Length);
								if (currExpr == possibleExprs[j])
								{
									inputExp = inputExp.Insert(idx2 - possibleExprs[j].Length, "System.Math.Pow(");
									break;
								}
							}
						}
						else if(inputExp[idx2].CompareTo(')') == 0)
						{
							numClosing++;
						}
						
						idx2--;
					}
				}				
			}
			idx--;
		}
		
		return inputExp;
	}
	
	public static bool isOperator(char c)
	{
		if (c.CompareTo('*') == 0 || c.CompareTo('+') == 0 || c.CompareTo('-') == 0 || c.CompareTo('/') == 0 || c.CompareTo(' ') == 0) return true; 
		else return false;
	}
}
