using UnityEngine;
using System.Collections;


public class PhysicsHandlerTorque : MonoBehaviour {
	public enum HINT_LEVEL { NO_HINT, HINT_SOME, HINT_ALL };
	
	public bool isPlaying;
	private float velocity_x;
	private float velocity_y_0;
	private float velocity_z;
	private Vector3 gravity;
	private float simStartTime;
	private Vector3 initialPosition;
	
	//public bool bRenderTrajectory;
	public int nTrajectoryVertexCount = 15;
	public Material trajectoryMaterial;
	private LineRenderer trajectory;
	private bool bSimComplete;
	public HINT_LEVEL hintLevel = HINT_LEVEL.NO_HINT;
	void Awake() {
		isPlaying = false;
		//bRenderTrajectory = true;
	}
	// Use this for initialization
	void Start () {
		bSimComplete = false;
		velocity_x = 0;
		velocity_y_0 = 5;
		velocity_z = 0;
		gravity = new Vector3(0,-9.8f,0);
		isPlaying = false;
		initialPosition = transform.position;
//		if( bRenderTrajectory )
		{
			trajectory = gameObject.AddComponent<LineRenderer>();
			trajectory.material = trajectoryMaterial;
			trajectory.SetVertexCount( nTrajectoryVertexCount );
			trajectory.SetWidth(0.1f,0.1f);
		}
	}
	public float finalPositionX;
	public float finalPositionZ;
	void Update()
	{
		if( isPlaying )
		{
			if( trajectory != null )
			{
				/*trajectory.SetVertexCount(0);
				Destroy(trajectory);
				trajectory =null;*/
			}
			
			//float velocity_y = velocity_y_0 + 0.5f*gravity.y*Mathf.Pow(Time.time-simStartTime,2);
			float velocity_y = velocity_y_0 + gravity.y*(Time.time-simStartTime);
			rigidbody.velocity =  new Vector3( velocity_x, velocity_y, velocity_z );
			/*transform.position = new Vector3( transform.position.x + (Time.deltaTime*velocity_x),
				transform.position.y + (Time.deltaTime*velocity_y),
				transform.position.z + (Time.deltaTime*velocity_z));*/
			//rigidbody.AddForce( ib.gravity );	
		}
		//else
		{
			
			UpdateValues();
			
			float fStartAirTime = ( -1*velocity_y_0 / gravity.y );
			float s = (velocity_y_0/2.0f) * fStartAirTime;
			float fEndAirTime = Mathf.Sqrt((initialPosition.y+s)/(-0.5f*gravity.y));
			float fAirTime = fStartAirTime + fEndAirTime;
			//print( "Airtime: " + fAirTime );
			
			if( trajectory != null && hintLevel != HINT_LEVEL.NO_HINT )
				//bRenderTrajectory )
			{
				/*trajectory = gameObject.AddComponent<LineRenderer>();
			trajectory.material = trajectoryMaterial;
			trajectory.SetVertexCount( nTrajectoryVertexCount );
			trajectory.SetWidth(0.1f,0.1f);*/
				
				//trajectory.SetVertexCount( nTrajectoryVertexCount );
				float fTrajectoryMultiplier = 0f;
				if( hintLevel == HINT_LEVEL.HINT_ALL )
					fTrajectoryMultiplier = fAirTime/nTrajectoryVertexCount;
				else if( hintLevel == HINT_LEVEL.HINT_SOME )
					fTrajectoryMultiplier = (0.05f*fAirTime)/(nTrajectoryVertexCount);
					
				trajectory.SetPosition(0, initialPosition );
				trajectory.enabled = true;
				for( int i = 1; i < nTrajectoryVertexCount; i++ )
				{	
					float time_i = i * fTrajectoryMultiplier;
					float v_i = velocity_y_0 + (gravity.y * time_i);
					float v_avg = (v_i + velocity_y_0) * 0.5f;
					Vector3 displacement = new Vector3( 
						time_i* velocity_x, 
						time_i* v_avg, 
						time_i* velocity_z );
					trajectory.SetPosition(i, initialPosition + displacement );
				}
				finalPositionX = fAirTime * velocity_x;
				finalPositionZ = fAirTime * velocity_z;					
			}
		}
		
		// Check for ending simulation - this is basically the collision detection now
//		if( transform.position.y < 0.034f )
//		{
//			rigidbody.velocity = Vector3.zero;
//			transform.position = new Vector3( transform.position.x,0.034f, transform.position.z );
//			bSimComplete = true;
//			
//			
//		}
	}	
	void OnCollisionEnter(Collision collision)
	{
		print("Entering Collision, Physics Handler " + collision.gameObject.name);
		if( collision.gameObject.name == "stadium" )
		{
			
			rigidbody.velocity = Vector3.zero;
			//transform.position = new Vector3( transform.position.x,0.034f, transform.position.z );
			bSimComplete = true;
		}
	}
	public void Reset()
	{
			transform.position = initialPosition;
			rigidbody.velocity = Vector3.zero;
	}
	public void SetPlaying( bool nowPlaying )
	{
		if( isPlaying == false && nowPlaying == true )
		{
			simStartTime = Time.time;
			
			UpdateValues();			
			bSimComplete = false;
		}
		isPlaying = nowPlaying; 
		if( ! isPlaying )
		{
			rigidbody.velocity = Vector3.zero;
		}
	}
	private void UpdateValues()
	{
		/*nkBox ib = gameObject.GetComponent<InkBox>();
	
		Quaternion tempRotation = Quaternion.identity;
		tempRotation.eulerAngles = new Vector3( 180-ib.rho, (360-ib.theta)-135, 0 );
		transform.rotation = tempRotation;
	
		rigidbody.mass = ib.mass;
		float impulseForce = ib.force / ib.mass;
		float thetaRads = ((360-ib.theta)-135)* Mathf.Deg2Rad;
		float rhoRads = (180-ib.rho)* Mathf.Deg2Rad;
		velocity_x = impulseForce * Mathf.Sin(thetaRads);
		velocity_z = impulseForce * Mathf.Cos(thetaRads);
		velocity_y_0 = impulseForce * Mathf.Sin(rhoRads);
		gravity = ib.gravity;*/
	}
	public bool isSimComplete()
	{
		return bSimComplete;
	}
}
