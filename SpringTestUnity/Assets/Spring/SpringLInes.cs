using UnityEngine;
using System.Collections;

public class SpringLInes : MonoBehaviour {
	public Transform endPoint, startPoint;
	public Color springColor;
	static int count = 0;
	public int index = 0;
	// Use this for initialization
	void Start () {
   		//line.SetWidth(lineWidth, lineWidth);
        index = count++;
		//SpringScript.textAreaList[index].texture
		//springColor = new Color(Random.value,Random.value,Random.value);
		springColor = new Color(Random.value,Random.value,Random.value);
	}
	
	// Update is called once per frame
	void Update () {
//		springColor = SpringScript.colorArray[index];
		
		LineRenderer lineRenderer = gameObject.GetComponent<LineRenderer>();
		lineRenderer.SetPosition(0,startPoint.position);	
		lineRenderer.SetPosition(1, endPoint.position);
		lineRenderer.materials[0].SetColor ("_TintColor", springColor);
	}
}
