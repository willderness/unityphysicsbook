﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RecognitionEngine;

namespace RemoteRecognizer
{
    class Program
    {
        public static void Main(string[] args)
        {
            //Added to prevent a new instance of server, will kill self if another instance is running.
            int processCount = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count();
            if (processCount > 1)
                //System.Diagnostics.Process.GetCurrentProcess().Kill();
                return;
            Server s = new Server();
            if( s.Started == true )
                s.Start();
        }
    }
}
