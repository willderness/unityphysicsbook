﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starPadSDK.Inq;
using starPadSDK.MathRecognizer;
using starPadSDK.CharRecognizer;
using starPadSDK.Geom;
using starPadSDK.MathExpr;
using starPadSDK.Inq.MSInkCompat;
using System.Windows.Ink;
using System.Windows.Input;

namespace RecognitionEngine
{
    class MathRecognizer
    {
        public MathRecognition Recognizer { get; protected set; }
        public StroqCollection MathStroqs { get; protected set; }
        public Server ClientInterface { get; protected set; }


        private string RecognitionResults = "";

        public MathRecognizer(Server server)
        {
            MathStroqs = new StroqCollection();

            Recognizer = new MathRecognition(MathStroqs);
            Recognizer.EnsureLoaded();
            Recognizer.ParseUpdated += Recognizer_RecognitionUpdated;

            ClientInterface = server;
            tmr.Elapsed += new System.Timers.ElapsedEventHandler(tmr_Elapsed);
        }

        /*
         * All messages are strings and of the format
         * "[message_type],,[value1], [value2],....., [last_value]$"
         * 
         * float and int values are sent as they are
         * bool values are sent as 'T' or 'F'
        */

        System.Timers.Timer tmr = new System.Timers.Timer(100);
        volatile int timer = 0;          
        bool recognitionCompleted = false;
        public void ProcessMessage(string message)
        {
            string[] values = message.Split(new char[] { ',' });
            string msgtype = values[0];

            switch (msgtype)
            {
                case Messages.InkStroke:
                    StylusPointCollection points = new StylusPointCollection();

                    //parse x,y points from message
                    //Skip "[message_type],,"
                    for (int i = 2; i < values.Length; i += 2)
                    {
                        double x = System.Convert.ToDouble(values[i]);            //Get x-coordinate
                        double y = System.Convert.ToDouble(values[i+1]);            //Get x-coordinate
                        StylusPoint sp = new StylusPoint(x, y);
                        points.Add(sp);
                    }

                    Stroke s = new Stroke(points);
                    Stroq stroq = new Stroq(s);

                    recognitionCompleted = false;                                        

                    MathStroqs.Add(stroq);

                    //wait while recognition is not complete
                    while (!recognitionCompleted) ;

                    //send results back
                    ClientInterface.Send(RecognitionResults);

                    break;
                case Messages.ClearRecognizer:
                    MathStroqs.Clear();
                    RecognitionResults = "";
                    recognitionCompleted = false;
                    break;
                case Messages.ClearStroke:
                    recognitionCompleted = false;   

                    for (int i = 2; i < values.Length; i++)
                    {
                        int index = System.Convert.ToInt32(values[2]);
                        MathStroqs.Remove(MathStroqs.ElementAt(index));
                    }
                    //wait while recognition is not complete
                    tmr.Start();
                    while ((timer < 2) && !recognitionCompleted) ;
                    tmr.Stop();
                    //while (!recognitionCompleted) ;

                    //send results back
                    ClientInterface.Send(RecognitionResults);

                    break;
            }


        }
        void tmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer++;
        }
        private void Recognizer_RecognitionUpdated(MathRecognition source, Recognition chchanged, bool updateMath)
        {
            RecognitionResults = Messages.Results + ",,";

            foreach (Parser.Range r in Recognizer.Ranges)
                if (r.Parse.expr != null)
                    RecognitionResults += Text.InputConvert(r.Parse.expr) + '\n'; //wlhj r.parse expr is null

            recognitionCompleted = true;
        }

        

        starPadSDK.Geom.Deg fpdangle(starPadSDK.Geom.Pt a, starPadSDK.Geom.Pt b, starPadSDK.Geom.Vec v)
        {
            return (a - b).Normalized().UnsignedAngle(v.Normalized());
        }
    }
}
