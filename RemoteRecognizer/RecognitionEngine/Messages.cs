﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecognitionEngine
{
    class Messages
    {
        public const string Begin = "Hello";
        public const string End = "Bye";
        public const string ACK = "YES";


        public const string InkStroke = "Stroke";
        public const string ClearRecognizer = "Clear";
        public const string Results = "Results";
        public const string ClearStroke = "ClearStroke";
    }
}
