﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace RecognitionEngine
{
    public class Server
    {
        private Socket ServerSocket;
        private Socket ClientSocket;

        private byte[] ReceiveBuffer, SendBuffer;
        private ASCIIEncoding Encoder;

        private MathRecognizer Recognition;
        public bool Started;
        public Server()
        {
            //wInterface = WiimoteInterface.Instance;
            ClientSocket = null;
            
            ReceiveBuffer = new byte[4096];
            SendBuffer = new byte[4096];
            Encoder = new ASCIIEncoding();

            ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            IPEndPoint local_ip = new IPEndPoint(IPAddress.Any, 8881);
            Started = true;
            
            try
            {
                ServerSocket.Bind(local_ip);
            }
            catch (Exception e)
            {
                Started = false;
            }



            Recognition = new MathRecognizer(this);
        }

        public void Start()
        {
            ServerSocket.Listen(5);
            Console.WriteLine("Running Recognition Server at {0}", System.Environment.MachineName);

            while (true)
            {
                ClientSocket = ServerSocket.Accept();
                ProcessClient();

                Recognition = new MathRecognizer(this);
            }
        }

        private void ProcessClient()
        {
            string message = Receive();
            
            //Initial Connect Protocol
            //expect 'Begin' message, respond with 'ACK'
            if (message == Messages.Begin)
            {
                //hand shake
                Send(Messages.ACK);
                Console.WriteLine("Connection Received and Confirmed");

                //respond to all client requests until an 'End' message is received
                while (message != Messages.End)
                {
                    message = Receive();

                    Recognition.ProcessMessage(message);
                    //SendResponse(response);
                }

                //good bye
                Send(Messages.ACK);
                Console.WriteLine("Terminating connection");
                Recognition = null;
            }

        }

        private string Receive()
        {
            string message = "";
            try
            {
                //Get message from client
                ClientSocket.Receive(ReceiveBuffer);
                message = Encoder.GetString(ReceiveBuffer);

                //parse message: all messages must end with a '$'
                int index = message.IndexOf('$');
                message = message.Remove(index);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                message = Messages.End;
            }

            return message;
        }

        internal void Send(string response)
        {
            try
            {
                //append terminal '$' to end of message
                response += "$";
                response = response.Replace('−', '-');
                SendBuffer = Encoder.GetBytes(response);
                ClientSocket.Send(SendBuffer);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
