using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class InstructionItem {
	public string text;
	public Rect location;
	public bool isClicked = false;
	public InstructionItem( string text, Rect location )
	{
		this.text = text;
		this.location = location;
		this.isClicked = false;
		
	}
	public void Show()
	{
		if( isClicked ) 
			return;
		
		GUIStyle myStyle = "box";
		myStyle.fontSize = 16;
		myStyle.fontStyle = FontStyle.Bold;
		GUI.TextArea( location, text, myStyle);
		GUIStyle smallTextStyle;
		smallTextStyle = new GUIStyle( GUI.skin.label);
		smallTextStyle.fontSize = smallTextStyle.fontSize - 2;
		GUI.Label( new Rect( location.x+5, location.yMax-20, location.width, 20), "Click to continue..", smallTextStyle );
		if( Input.GetMouseButtonDown(0) )
		{
			Vector3 mousePos = Input.mousePosition;
	        mousePos.y = Screen.height - Input.mousePosition.y;

	        bool overGUI = location.Contains(mousePos);
			if( overGUI )
			{
				isClicked = true;
			}
		}
    }	
}

public class GameInstructions : MonoBehaviour {
	public List<InstructionItem> instructions;
	
	public GUISkin guiSkin;
	public int iIndex = 0;
	void Awake()
	{
		instructions = new  List<InstructionItem>();
	}
	void OnGUI()
	{
	GUI.skin = guiSkin;	
		if( iIndex < instructions.Count )
		{
			instructions[iIndex].Show();	
			if( instructions[iIndex].isClicked == true )
				iIndex++;
		}
	}
	void Reset()
	{
		iIndex = 0;	
	}
	public void Skip()
	{
		iIndex = instructions.Count;	
	}
};