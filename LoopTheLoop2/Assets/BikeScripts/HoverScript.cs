using UnityEngine;
using System.Collections;

public class HoverScript : MonoBehaviour {

	public string levelToLoad;
	public AudioClip soundHover;
	public AudioClip beep;
	public bool quitButton = false;
	
	void OnMouseEnter() {
		//audio.PlayOneShot(soundHover);
	}
	
	void OnMouseUp() {
		//audio.PlayOneShot(beep);
		//yield return new WaitForSeconds(0.35);
		if (quitButton) {
			Application.Quit();	
		}
		else{
			Application.LoadLevel(levelToLoad);
		}
	}
}
