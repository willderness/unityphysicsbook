using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BikeScript_Level1 : MonoBehaviour {
	
	// Spline attributes
	public GameObject splineRoot;
	Transform[] splineChildren;
	
	// Turning and Speeding Up Attributes
	bool moving = false;
	bool speedUp = false;
	float initalVelocity_z = 0.0f;
	float velocityMultiplier = 1/210;
	
	// Game play attributes
	public bool isPlaying = false;
	bool heightUpdated = false;
	
	// Position attributes
	Vector3 originalPosition = Vector3.zero;
	Vector3 originalRotation = Vector3.zero;
	Quaternion originalQuaternion = Quaternion.identity;
	float bikerHeight;
	public Transform centerOfMass;
	
	// GUI / UI
	public GameObject text3D_bikerHeight;	
	string bikerHeightTextArea;
	private GameInstructions gameInstructions;
	
	// Use this for initialization
	void Start () {
		splineChildren = GameObject.Find("SplineRoot1").GetComponentsInChildren<Transform>();
		bikerHeightTextArea = transform.position.y.ToString();
		bikerHeight = transform.position.y;
		splineChildren = splineRoot.GetComponentsInChildren<Transform>();
		System.Array.Sort(splineChildren, delegate(Transform first, Transform second) {
			return second.position.y.CompareTo(first.position.y);
		});
		
		SetupCenterOfMass();
		
		gameInstructions = GetComponent<GameInstructions>();
		
		gameInstructions.instructions.Add( new InstructionItem(
			"Welcome Evel Knievel!\nYour goal is to find out the minimum height\n" + 
			"the biker needs to start from to complete the\n" +
			"loop the loop\n" +
			"without falling off the track.", 
			new Rect(800,200,400,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"EQUATIONS:\nMove the cursor over to the right edge\n of the screen \n" +
			"to see the equations in play.", 
			new Rect(Screen.width - 400,100,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"CAMERAS:\nUse the Camera Menu\nto change Cameras views.\n\n" + 
			"Try Loop view now,\n and then change back to Side.\n", 
			new Rect(Screen.width - 400,100,300,150)));
		gameInstructions.instructions.Add( new InstructionItem(
			"GOOD LUCK!!", 
			new Rect(875,100,200,150)));
	}
	
	void SetupCenterOfMass()
	{
		if(centerOfMass != null)
			rigidbody.centerOfMass = centerOfMass.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (/*initalVelocity_x == 0.0f && transform.position.x > 124 &&*/ transform.position.z < 124.5) {
			if (initalVelocity_z < rigidbody.velocity.z) initalVelocity_z = rigidbody.velocity.z;
			//print("initalVelocity_x = " + initalVelocity_x);
		}
		
		if (transform.position.z > 126 && transform.position.z < 140) {
			moving = true;
		}
		
		if (moving && transform.position.x > -3.77)
		{
			//rigidbody.velocity.z += 1.0f;
			rigidbody.velocity = new Vector3(rigidbody.velocity.x - initalVelocity_z/500, 
											 rigidbody.velocity.y, 
											 rigidbody.velocity.z);
		}
		else
		{
			rigidbody.velocity = new Vector3(0, rigidbody.velocity.y, rigidbody.velocity.z);
			moving = false;
		}
		
		if (!speedUp && transform.position.z > 138)
		{
			rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y, initalVelocity_z);
			speedUp = true;
		}
		
		if (heightUpdated)
		{
	       	TextMesh textMesh = text3D_bikerHeight.GetComponent<TextMesh>();
			float.TryParse(bikerHeightTextArea, out bikerHeight);
			textMesh.text = bikerHeight.ToString();
	       	text3D_bikerHeight.transform.position = new Vector3(text3D_bikerHeight.transform.position.x, 
																bikerHeight + 10, 
																text3D_bikerHeight.transform.position.z);
			
			print("splineChildren.Length = " + splineChildren.Length);
			for(int i = 0; i < splineChildren.Length; i++)
			{
				print("bikerHeight = " + bikerHeight);
				print("splineChildren[" + i + "] = " + splineChildren[i].position);
				if (i==0)
				{
					if (bikerHeight > splineChildren[i].position.y)
					{
						print("splineChildren[" + i + "].y = " + splineChildren[i].position.y);
						transform.position = new Vector3(transform.position.x,
														 splineChildren[i].position.y,
														 splineChildren[0].position.z);
						break;
					}
				}
				else if (bikerHeight < splineChildren[i-1].position.y && bikerHeight > splineChildren[i].position.y)
				{
					float diff1 = Mathf.Abs(bikerHeight - splineChildren[i].position.y);
					float diff2 = Mathf.Abs(bikerHeight - splineChildren[i-1].position.y);
					if (diff1 > diff2)
					{
						print("Enter1");
						transform.position = new Vector3(transform.position.x,
														 splineChildren[i].position.y,//bikerHeight,
														 splineChildren[i].position.z);
					}
					else
					{
						print("Enter2");
						transform.position = new Vector3(transform.position.x,
														 splineChildren[i-1].position.y,//bikerHeight,
														 splineChildren[i-1].position.z);
					}
					break;
				}
			}
			
			//transform.position = new Vector3(transform.position.x, bikerHeight, transform.position.z);
		}
	}
	
	
	void OnGUI () {
		Rect textRect = new Rect(10, 10, 150, 30);
		
		if ( GUI.Button( textRect, "Menu") )
		{
			Application.LoadLevel(0);
		}
		
        GUI.TextArea(new Rect(10, 50, 150, 30), "(x, y, z) = (" + Mathf.FloorToInt(transform.position.x) + ", " + 
												 Mathf.FloorToInt(transform.position.y) + ", " + 
												 Mathf.FloorToInt(transform.position.z) + ")");
		
		GUI.TextArea(new Rect(10, 90, 150, 30), "rotation = (" + Mathf.FloorToInt(transform.rotation.x) + ", " + 
												 				 Mathf.FloorToInt(transform.rotation.y) + ", " + 
												 				 Mathf.FloorToInt(transform.rotation.z) + ")");
		
		//print ("rigidbody.velocity = " + rigidbody.velocity);
		GUI.TextArea(new Rect(10, 130, 150, 30), "velocity = (" + Mathf.FloorToInt(rigidbody.velocity.x) + ", " + 
												 				 Mathf.FloorToInt(rigidbody.velocity.y) + ", " + 
												 				 Mathf.FloorToInt(rigidbody.velocity.z) + ")");
		
		GUI.TextArea(new Rect(10, 170, 150, 30), "AV = (" + Mathf.FloorToInt(rigidbody.angularVelocity.x) + ", " + 
												 				 Mathf.FloorToInt(rigidbody.angularVelocity.y) + ", " + 
												 				 Mathf.FloorToInt(rigidbody.angularVelocity.z) + ")");
		
		
		if( GUI.Button( new Rect(175,5,100,30), isPlaying?"Stop":"Go" ) )
		{
			isPlaying = !isPlaying;
			if (isPlaying)
			{
				rigidbody.useGravity = true;
				rigidbody.constraints = RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
				originalPosition = transform.position;
				originalRotation = new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z);
				originalQuaternion = transform.rotation;
			}
			else
			{
				rigidbody.useGravity = false;
				rigidbody.velocity = Vector3.zero;
				//transform.position = originalPosition;
				//transform.Rotate(originalRotation);
				rigidbody.MoveRotation(originalQuaternion);
				rigidbody.MovePosition(originalPosition);
				rigidbody.freezeRotation = true;
			}
		}
		
		// Check height text area
		string originalVal = bikerHeightTextArea;
		GUI.TextArea(new Rect(285, 5, 75, 30), "Height = ");
		bikerHeightTextArea = GUI.TextArea(new Rect(360, 5, 100, 30), bikerHeightTextArea);
		
		if (originalVal != bikerHeightTextArea) heightUpdated = true;
		else heightUpdated = false;
    }
}
