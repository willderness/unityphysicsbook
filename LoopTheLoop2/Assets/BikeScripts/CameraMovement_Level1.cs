using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CameraMovement_Level1 : MonoBehaviour {
	
	public sealed class CameraMode {

    private readonly string name;
    public readonly int value;
	private static readonly Dictionary<string, CameraMode> instance = new Dictionary<string,CameraMode>();

    public static readonly CameraMode LOOP = new CameraMode (0, "Loop");
    public static readonly CameraMode FOLLOW = new CameraMode (1, "Follow");
    public static readonly CameraMode OVERHEAD = new CameraMode (2, "Overhead");        

    private CameraMode(int value, string name){
        this.name = name;
        this.value = value;
		instance[name] = this;
    }

    public override string ToString(){
        return name;
    }
	public static explicit operator CameraMode(string str)
	{
	    CameraMode result;
	    if (instance.TryGetValue(str, out result))
	        return result;
	    else
	        throw new InvalidCastException();
	}
}
	GUIContent[] comboBoxList;
	private ComboBox comboBoxControl;// = new ComboBox();
	private GUIStyle listStyle = new GUIStyle();
	public Transform Overhead;
	public Transform Loop;
	
	private BikeScript_Level1 bikeScript;
	
	private void Start()
	{
		comboBoxList = new GUIContent[3];
		comboBoxList[CameraMode.LOOP.value] = new GUIContent(CameraMode.LOOP.ToString());
		comboBoxList[CameraMode.FOLLOW.value] = new GUIContent(CameraMode.FOLLOW.ToString());
		comboBoxList[CameraMode.OVERHEAD.value] = new GUIContent(CameraMode.OVERHEAD.ToString());
		
		listStyle.normal.textColor = Color.white; 
		listStyle.onHover.background =
		listStyle.hover.background = new Texture2D(2, 2);
		listStyle.padding.left =
		listStyle.padding.right =
		listStyle.padding.top =
		listStyle.padding.bottom = 4;
 
		comboBoxControl = new ComboBox(new Rect(Screen.width-105, 10, 100, 20), comboBoxList[0], comboBoxList, "button", "box", listStyle);
		cameraMode = CameraMode.LOOP;

		GameObject physicsObject = GameObject.FindGameObjectWithTag("Player");
		originalScale = physicsObject.transform.localScale;
		
		bikeScript = gameObject.GetComponent<BikeScript_Level1>();
	}
	public CameraMode cameraMode;
	void OnGUI() {

		/*if (GUI.Button(new Rect(60, 10, 150, 50), trackBallText))
		{
        	trackBall = !trackBall;
			trackBallText = "Stop Tracking Ball";	
			
			if( trackBall == false )
			{
				Camera.mainCamera.transform.position = 	mainCamInitPos;
		 		Camera.mainCamera.transform.rotation = mainCamInitRot;
				trackBallText = "Track Ball";
			}
		}*/
		comboBoxControl.Show();
	}
	Vector3 originalScale;
	// Update is called once per frame
	void Update () {
		CameraMode oldCameraMode = cameraMode;				
		
		if (!bikeScript.isPlaying)
		{
			cameraMode = (CameraMode)comboBoxList[2].text;
		}
		else
		{
			if (transform.position.z > 90 && transform.position.z < 135)
			{
				cameraMode = (CameraMode)comboBoxList[0].text;
			}
			else 
			{
				cameraMode = (CameraMode)comboBoxList[1].text;
			}
		}
		//print ( cameraMode.ToString());
		if( oldCameraMode != cameraMode )
		{
			GameObject physicsObject = GameObject.FindGameObjectWithTag("Player");
			if( cameraMode == CameraMode.OVERHEAD )
			{
				Camera.mainCamera.transform.position = Overhead.position;//	OverheadPos;
		 		Camera.mainCamera.transform.rotation = Overhead.rotation;//OverheadRot;
				
				originalScale = physicsObject.transform.localScale;
				//physicsObject.transform.localScale = physicsObject.transform.localScale * 10;
			}
			else if( cameraMode == CameraMode.LOOP ) 
			{	
				Camera.mainCamera.transform.position = Loop.position;//	LoopPos;
		 		Camera.mainCamera.transform.rotation = Loop.rotation; //Rot;
				physicsObject.transform.localScale = originalScale;
			}
			if( cameraMode == CameraMode.FOLLOW )
			{
				physicsObject.transform.localScale = originalScale;
			}
		}
		
		if( cameraMode == CameraMode.FOLLOW )
		{
			GameObject physicsObject = GameObject.FindGameObjectWithTag("Player");
			Camera.mainCamera.transform.position = 	physicsObject.transform.position+ new Vector3(4,7,-5);//0.5f, 0.5f, 0.5f) ;
			Camera.mainCamera.transform.LookAt( physicsObject.transform.position );	
		}
	}
}
